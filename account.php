<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once("models/header.php");
require_once("models/db-queries.php");

echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Λογαριασμός</h2>
<div id='left-nav'>";

include("left-nav.php");

echo "
</div>
<div id='main'>";

echo "Καλώς ήλθες " . $loggedInUser->displayname . "!";
echo "<p><b>Εκκρεμότητες</b></p>";
$user = getATeacherByUsername($grmysqli, $loggedInUser->username);
if ($user == NULL)
{
	echo "<p>Φαίνεται ότι στο σύστημά μας δεν έχετε καταχωρηθεί ως διδάσκων σε κάποιο μάθημα με το όνομα $loggedInUser->username</p>";
}
else {
	$teacher_id = $user['id']; //teacher_id
	$lessons = getTeacherLessons($grmysqli, $teacher_id);
	if ($lessons == NULL)
	{
		echo "<p>Φαίνεται ότι στο σύστημά μας δεν έχετε κάποιο μάθημα σε ανάθεση (εσωτερικός κωδικός $teacher_id).</p>";
	}
	else
	{	$user_table_user = getAUserByUsername($grmysqli, $loggedInUser->username);//to get the real user id from users table
		echo "<p>";
		echo "Τα παρακάτω μαθήματα έχουν ανατεθεί σε εσάς για βαθμολόγηση.
			Μπορείτε να ξεκινήσετε την συμπλήρωση των βαθμών κάνοντας κλικ πάνω τους.
			";
		echo "</p>";
		echo "<font size=+1>";
		echo "<ul>";
		foreach($lessons as $lesson)
		{
			$scid = $lesson['schoolclassid'];
			$schclass = getAClass($grmysqli, $scid);
			if ($schclass == NULL) 
			{
				echo "<li>Εσωτερική υπόθεση για το $scid </li>";
			}
			else
			{
				$schoolid = $schclass['school_id'];
				$teacherid = $user['id'];
				$schoolcourse = $lesson['schoolcourseid'];

				echo "<li>";
				echo "   <a href=\"gradesinput.php?school=$schoolid&teacher=$teacherid&schoolcourse=$schoolcourse\">";
				echo $schclass['classlevel'] ."   ". $lesson['lessonname'] ;
				echo "</a>";
				echo "</li>";
			}
		}
		echo "</ul>";
		echo "</font>";
		if ($loggedInUser->checkPermission(array(1))){//if teacher to put gradeslogged in
			//$user_table_user = getAUserByUsername($grmysqli, $loggedInUser->username);
			$user_schools = getUserSchools($grmysqli, $user_table_user[0]['id'] );
			if (count($user_schools)>0){
				echo "<table border=1>
				<tr><td>Μηνύματα σχολείων:</td></tr>";
				foreach($user_schools as $user_school)
				{
					$school_id_for_message= $user_school['school_id'];
					$messages = getSchoolMessages($grmysqli,  $school_id_for_message);
					$school= getASchool($grmysqli, $school_id_for_message);
					foreach($messages as $message){
						echo "<tr><td><b>".$school['schoolname']."</b></td></tr>";
						echo "<tr><td>".$message['message']."</td></tr>";
					}
				}
				echo "</table>";
			}
		}
	}

	if ($loggedInUser->checkPermission(array(3))){// if school op show textarea for message to teachers
		if (isset($_POST['message_to_teachers'])) {//if post then save to db school op message
			$data[0]= $_POST['message_to_teachers'];
			$data[1]=  $user_table_user[0]['id'];
			setUserMessage($grmysqli, $data);
		}
		$user_messages = getMessageBySenderUserId($grmysqli, $user_table_user[0]['id']);
    $message='';
    if ($user_messages!=null) {
      $message = $user_messages[0]['message'];
    }
		echo "<table border=1>";
	        echo "<tr><td>
	              <form action='account.php' method='post' id='drm_message_to_from_sch_op'>
	                   Μήνυμά προς του καθηγητές που καταχωρούν βαθμολογίες:<br> 
	                   <textarea rows='10' cols='150' style='width:100%' name='message_to_teachers'>". $message."</textarea>
	                  <input type='submit' value = 'Υποβολή'>
	             </form>
	         </tr></td>";
	        echo "</table>";
	}
}
//account.php Οδηγίες εδώ.
echo "</div>
<div id='bottom'></div>
</div>
</body>
</html>";
?>
<?php
function set_school_op_message($message, $user_id) {
	echo $message.$user_id;
}
  
?>
