<?php

class parsingElement
{
	public $DEBUG;

	public $dbConnection; //Connection id used to access the db

	public $id = -1; //Data base id (primary key)

	public $data = ""; //(primary) data related to this entity

	public $dataAlreadyStored = FALSE; //true if the data were already at the db, false otherwise

	public function parsingElement( $dbConnection )
	{
    global $Settings;
    $this->DEBUG = $Settings->getDebug();
		$this->dbConnection = $dbConnection;
	}

	public function isInitialized() 
	{ 
		if ( $this->data == "" )
		{
			return FALSE;
		}

		return TRUE;
	}

	public function display()
	{
		echo ">$this->data<";
	}

	public function display_html()
	{
		echo "-$this->id - $this->data-<br>\n";
	}

  /* Επιστρέφει :
    -1 σε αποτυχία
    0 αν δεν κάνει τίποτα (υπάρχει ήδη στην βάση)
    1 αν έγινε πραγματική αποθήκευση
   */
	public function store()
	{
		echo "Store not implemented here!<br>\n";
	}
}

class School extends parsingElement
{
	public $schoolCode = -1;
	public $schoolYear = -1;
	public function setSchoolCode( $schoolCode )
	{
		$this->schoolCode = $schoolCode;
	}

	public function setSchoolYear ($schoolYear)
	{
		$this->schoolYear = $schoolYear;
	}

	public function isInitialized()
	{
		return  ($this->schoolYear!=-1 && $this->schoolCode!=-1 && $this->data != "") ;
	}

	public function display_html()
	{
		echo "<h1>$this->id $this->data</h1>";
		echo "<h2>$this->schoolCode - $this->schoolYear</h2>";
	}


	public $isStored = FALSE;
	public function store()
	{
		if ($this->isInitialized() == FALSE)
		{
			echo "Error, School cannot be stored! it's not initialized yet. Will try again later.<br>\n";
			$this->display_html();
			return -1;
		}
		if ($this->isStored) { return 0; }


		//Insert this year only if it doesn't exist
		$row = getAYear( $this->dbConnection, $this->schoolYear);
		if (!isset($row) || count($row)==0) {
			insertYear($this->dbConnection, $this->schoolYear);
		}

		//Check if the school is already stored
		$row = getASchool( $this->dbConnection, $this->schoolCode);
		if (isset($row) && count($row)>0)
		{
			$storedSchoolName = $row['schoolname'];
			if (strcmp($storedSchoolName, $this->data)!=0)
			{
				echo "Error! School is stored with different name!<br>\n";
				echo "Stored name is $storedSchoolName<br>\n";
				echo "My name is $this->data<br>\n";
				die;
			}
			else
			{
				$this->id = $row['id'];
				$this->isStored = TRUE;
				$this->dataAlreadyStored = true;
				return 0;
			}
		}

		$dbParams = array($this->schoolCode, $this->data);
		//print_r( $dbParams );
	
		$this->id = insertSchool($this->dbConnection, $dbParams);
		if ($this->id == FALSE)
		{
			echo "Failed to insert school<br>\n";
			mysqli_error($this->dbConnection);
			die;
		}
		else
		{ 
			$this->isStored = TRUE;
			$this->display_html();
      return 1;
		}
	}
}

class Tmima extends parsingElement
{
	public $classNumber = "";

	public $school;

	public function setTmima( $fullName )
	{
		$this->data = $fullName;
		$this->classNumber = substr( $fullName, 0, 2 ); //UTF : TWO BYTES!
		//echo "Found new tmima $fullName of class $classNumber<br>\n";
	}

	public function setSchool( $sch )
	{
		$this->school = $sch;
	}

	public function display()
	{
		echo "Tmima>$this->data<";
	}

	public $isStored = FALSE;
	public function store()
	{
		if ($this->isStored) { return; }

		//First store my school
		$this->school->store();
		if ($this->school->id == FALSE) 
		{
			echo "Failed to store my school. aborting";
			die;
		}

		//then check if this class is already stored
		$row = getASchoolClass( $this->dbConnection,
				$this->data,
			       	$this->classNumber, 
				$this->school->id,
				$this->school->schoolYear
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->id = $row['id'];
			$this->isStored = TRUE;
			$this->dataAlreadyStored = true;

			return 0;
		}
		else
		{
			echo "<br>\n Brand new class $this->data<br>\n";
		}

		$dbParams = array(
				$this->data, 
				$this->classNumber, 
				$this->school->id,
				$this->school->schoolYear
				);

		$this->id = insertSchoolclass( $this->dbConnection, $dbParams );
		if ($this->id == FALSE)
		{
			echo "Failed to insert school class<br>\n";
			die;
		}
		else
		{ 
			$this->isStored = TRUE;
			$this->display_html();
      return 1;
		}
	}

}

class Teacher extends parsingElement
{

  public function isInitialized()
  {
    if ($this->data == null || strcmp($this->data,"")==0)
    {
      return false;
    }

    return true;
  }

	public function display_html()
	{
		echo "Teacher: $this->data username $this->username<br>\n";
	}

	public $school;
	public function setSchool( $sch )
	{
		$this->school = $sch;
	}

	public function setTeacher( $fullName )
	{
		$this->data = $fullName;
		//echo "Found new teacher $fullName<br>\n";
	}
	public function getFullname()
	{
		return $this->data;
	}

	public function display()
	{
		echo "Teacher>$this->data<";
	}

	public $isStored = FALSE;
	public function store()
	{
		if ($this->isStored) { return 0; }

		//check if this teacher is already stored
		$row = getATeacher($this->dbConnection,
				$this->data,  //should be lastname XXX
				'', //should be firstname
				'', //fathername
				'', //mothername
				$this->getUsername() //username
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->id = $row['id'];
			$this->isStored = TRUE;
			$this->dataAlreadyStored = true;

			return 0;
		}

		//go on to store this teacher
		$dbParams = array(
				$this->data,  //should be lastname 
				'', //should be firstname
				'', //fathername
				'', //mothername
				$this->getUsername(),
				'FALSE' //isadmin
				);
		$this->id = insertTeacher( $this->dbConnection, $dbParams );
		if ($this->id == FALSE)
		{
			echo "Failed to insert teacher<br>\n";
			die;
		}
		else
		{ 
			$this->isStored = TRUE;
			$this->display_html();
      return 1;
		}
	}

	public $password=-1;
	public function getPassword() 
	{
		if ($this->password==-1)
		{
			$username = $this->getUsername();
			$this->password = substr($username,0,3) .
			       	substr(sha1( $username ), 0, 2);
		}
	       	return $this->password; 
	}
	public $username=-1;
	public $email = -1;
	public function getUsername() 
	{
		if ($this->username == -1) 
		{
			$firstPart = explode(" ", $this->data,  2);
			//print_r($firstPart);
			$this->username = substr($firstPart[1],0,2) . $firstPart[0]; //utf 2bytes->one character
			$this->username = $this->greek_to_greeklish( $this->username );
			$this->username = strtolower( $this->username );
			$this->username = $this->username . strlen($this->data);
			$this->email = "$this->username@ych.me";
		}
	       	return $this->username; 
	}
	public $userId = -1;
	public function createUser()
	{
		$this->getUsername();
		$this->getPassword();

		/*
		$counter = 1;
		do
		{
			$exists = getAUserByUsername( $this->dbConnection, $this->username);
			if ($exists != null) {
				$this->username = $this->username . "$counter";
				$counter++;
			}
		} while ($exists != null);
		*/

		$exists = getAUserByUsername( $this->dbConnection, $this->username, $this->school->schoolCode);
		if ($exists != null) {
			if ($this->DEBUG) echo "User $this->username already exists<br>\n";

			return;
		}


		$data = array (
				$this->username,
				$this->password,
				FALSE,
				$this->school->schoolCode
			      );
		$this->userId = insertUser( $this->dbConnection, $data);
		if ($this->userId == FALSE)
		{
			echo "Failed to insert user<br>\n";
			die;
		}
	}

	public function addUserToUserCake()
	{
		/* add the user to user cake */

    //first check whether already exists
    $found = fetchUserDetails($this->username);
    if ($found==null || sizeof($found)==0) {
      //go ahead and add this user
      //defined in models/class.newuser.php used in register.php
      $user = new User( 
        $this->username, 
        $this->data, //displayname
        $this->password,
        $this->email );
      $user->userCakeAddUser(); //returns nothing
      if ($user->status)
      {
        echo "Το username $this->username μπήκε στο Usercake<br>\n";
      }
      else
      {
        echo "Δυστυχώς δεν κατάφερα να φτιάξω το λογαριασμό του $this->username στο usercake.\n";
        $errors = array();
        if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($this->username));
        if($user->displayname_taken) $errors[] = lang("ACCOUNT_DISPLAYNAME_IN_USE",array($this->data));
        if($user->email_taken) 	  $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($this->email));		
        if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
        if($user->sql_failure)  $errors[] = lang("SQL_ERROR");

        print_r( $errors );
        echo "<br>\n";
      }
    } else {
      if ($this->DEBUG) {
        echo "Το username $this->username υπάρχει ήδη στο Usercake<br>\n";
      }
    }

	}



	function greek_to_greeklish($string)
	{
		//source : http://code.loon.gr/snippet/php/%CE%BC%CE%B5%CF%84%CE%B1%CF%84%CF%81%CE%BF%CF%80%CE%AE-greek-%CF%83%CE%B5-greeklish
		return strtr($string, array(
					'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
					'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'KS', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F',
					'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'O',
					'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
					'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'ks', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
					'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
					'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ώ' => 'o',
					'ϊ' => 'i', 'ϋ' => 'y',
					'ΐ' => 'i', 'ΰ' => 'y'
					));
	}

}

class Lesson extends parsingElement
{

	public $tmima;
	public $teacher = array();

	public $courseId = -1; //course connects teacher, class and lesson

	public function setLesson( $fullName )
	{
		$this->data = $fullName;
		//echo "Found new lesson $fullName<br>\n";
	}

	public function setTmima( $tmima )
	{
		$this->tmima = $tmima;
	}

	public function setTeacher( $teacher )
	{
		$this->teacher[] = $teacher;
	}
  public function addTeacher( $teacher )
  {
    $this->setTeacher( $teacher );
  }

	public function display()
	{
		if ($this->tmima != NULL) 
		{
			$this->tmima->display();
		}
		else
		{
			echo "no tmima";
		}
		if (sizeof($this->teacher)>0) 
		{
			$this->teacher[0]->display();
		}
		else
		{
			echo "no teacher";
		}
		echo "Lesson>$this->data</";
	}

	public function isInitialized()
	{

		$isIt = $this->tmima != null &&
			$this->tmima->isInitialized() &&
			sizeof($this->teacher)>0 &&
			$this->teacher[0]->isInitialized() &&
			parent::isInitialized();

		return $isIt;
	}

	public $isStored = FALSE;
	public function store()
	{
		if ($this->isStored) { return 0; }

	
		if ($this->tmima != NULL) 
		{
			$this->tmima->store();
		}
		if (sizeof($this->teacher)>0) 
		{
      foreach( $this->teacher as $teacher) {
			   $teacher->store();
      }
		}

		return $this->store_thisLessonOnly();
	}

	function store_thisLessonOnly()
	{
		//check if this lesson exists
		$row = getASchoolLesson($this->dbConnection,
				$this->data //lesson
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->id = $row['id'];
			$this->isStored = TRUE;
			$this->dataAlreadyStored = true;

			echo "Retrieved lesson<br><br>";
			$this->display_html();
			return 0;
		}

		//go on to store this lesson
		$dbParams = array(
				$this->data,  //lesson name
				);
		$this->id = insertSchoollesson( $this->dbConnection, $dbParams );
		if ($this->id == FALSE)
		{
			echo "Failed to insert lesson<br>\n";
			die;
		}
		else
		{ 
			$this->isStored = TRUE;
			$this->display_html();
      return 1;
		}
	}
}

class Course extends parsingElement
{
	public $lesson = NULL;
	public $class = NULL;
	public $teacher = array ();
	public function isInitialized()
	{
		$inited = ($this->lesson != NULL &&
      $this->class !=NULL && sizeof($this->teacher)>0 );

    return $inited;
	}

	public function setClass( $class )
	{
		$this->class = $class;
	}
	public function setTeacher( $teacher )
	{
		$this->teacher[] = $teacher;
	}
  public function addTeacher( $teacher )
  {
		$this->setTeacher( $teacher );
  }
	public function setLesson( $lesson )
	{
		$this->lesson = $lesson;
	}

  /* Store this course 
    Note that there maybe many teachers on this course. */
	public function store()
	{
		if ($this->id != -1) return -1;

    /* first insert the course */

    //check if this course is already registered
    $resultRow =
      getASchoolCourse( $this->dbConnection,
        $this->class->id,
        $this->lesson->id);
    $this->id = $resultRow['id'];
    if ($this->id==null)
    {
      //new course entry
      $data = array( $this->class->id, $this->lesson->id);
      $this->id = insertSchoolcourse( $this->dbConnection, $data );
      if ($this->id == false) {
        echo "Πρόβλημα στην καταχώριση του μαθήματος(Schoolcourse) <br>\n";
        $this->class->display_html();
        $this->lesson->display_html();
        exit(1);

      } else {
        //entry allready exists

        if ($this->DEBUG) {
          echo "Already stored school course<br>\n";
        }
      }
    }

      /* secondly insert each teacher of the course */
      foreach ($this->teacher as $teacherObject)
      {
        $teacherObject->store(); //if it's stored, will not be stored again.

        $teacher = $teacherObject->data;

        $rows = getTeachersOfCourseId($this->dbConnection, 
          $teacherObject->id,
          $this->id);
        if ($rows==null)
        {
          //This is a new entry
          $data = array (
            $teacherObject->id,
            $this->id);
          $sucOfInsert = insertTeacherOfSchoolcourse( $this->dbConnection, $data );
          if ($sucOfInsert == false) {
            echo "Πρόβλημα στην καταχώριση του μαθήματος<br>\n";
            $teacherObject->display();
            $this->class->display();
            $this->lesson->display();
            exit(1);

          } else {
            if ($this->DEBUG) {
              echo "Stored school course<br>\n";
              $teacherObject->display();
            }
          }
        } //finished storing
        else
        {
          //entry was already stored
          $this->id = $rows['schoolcourse_id']; 
          if ($this->DEBUG) echo "Course already exists with id $this->id<br>\n";
        }
      }//for each teacher of this course
    }
} // Course class

class StudentMark extends parsingElement
{
	private $DOINSERTGRADES = FALSE;
	public $aa = "";
	public $nestorid = "";
	public $lastname = "";
	public $firstname = "";
	public $fathername = "";
	public $mothername = "";
	public $mark = array(
			);

	public $period = array(
			);

	public $course = NULL;

	public function StudentMark($dbConnection, $DOINSERTGRADES)
	{
		parent::__construct($dbConnection);
		$this->DOINSERTGRADES = $DOINSERTGRADES;
	}

	public function isInitialized() 
	{ 
		if ($this->course==NULL || 
			       ($this->aa == "" ||
				$this->nestorid == "" ||
				$this->lastname == "" ||
				$this->firstname == ""
			       	// || $this->fathername == "" Υπάρχει περίπτωση να είναι πράγματι κενό! αν ο πατέρας δεν έχει αναγνωρίσει το παιδί
				)
				)
		{
			return FALSE;
		}

		return TRUE;
	}

	public function setCourse( $course )
	{
		$this->course = $course;
	}

	public function addMark( $mark, $period )
	{
		$this->mark[] = $mark;
		$this->period[] = $period;
	}

	public function display()
	{
    echo ("Student>$this->aa 
      | $this->nestorid
      | $this->lastname
      | $this->firstname
      | $this->fathername
      | $this->mothername | " );
      print_r($this->mark);


		if ($this->course!=NULL) 
		{
			$this->course->display();
		}
	}

	public function store()
	{
		if ($this->isInitialized() == FALSE)
		{
			echo "Unitiliazed!<br>\n";
			print_r($this);
			die;
		}

		if ($this->course!=NULL) 
		{
			$this->course->store();
		}

		$this->storeStudentData();
		if ($this->studentId != -1)
		{
			$this->storeStudentSchoolRegistration();
			$this->storeStudentClassRegistration();
			$this->storeStudentMark();
		}
	}

	function display_html()
	{
		if ($this->isInitialized()==FALSE) return;

		echo "<tr>";
		echo "<td>$this->nestorid</td><td>$this->lastname</td><td>$this->firstname</td><td>$this->fathername</td><td>$this->mothername</td>";
		for ($mi=0; $mi<count($this->mark); $mi++)
		{
			echo "<td>";
			$p = $this->period[$mi];
			$m = $this->mark[$mi];
			echo "$p:$m";
			echo "</td>";
		}
		echo "</tr>";
		//if $dataAlreadyStored then do something to make them show differently
	}

	public $studentId = -1;
	function storeStudentData()
	{
		if ($this->studentId != -1) 
		{
			return;
		}

		//check if this student already exists
		$row = getAStudent($this->dbConnection,
				$this->lastname,
				$this->firstname,
				$this->fathername,
				$this->mothername
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->studentId = $row['id'];
			return 0;
		}

		//go on and store the student
		$dbParams = array(
				$this->lastname,
				$this->firstname,
				$this->fathername,
				$this->mothername,
				null //birthdate 
				);
		$this->studentId = insertStudent( $this->dbConnection, $dbParams );
		if ($this->studentId == FALSE)
		{
			echo "Failed to insert student<br>\n";
			die;
		}
		else
		{ 
			$this->display_html();
      return 1;
		}
	}

	public $schoolRegistrationId = -1;
	function storeStudentSchoolRegistration()
	{
		if ($this->schoolRegistrationId!=-1)
		{
			return -1;
		}

		//check if this registration already exists
		$row = getASchoolRegistration($this->dbConnection,
				$this->course->class->school->id,
				//$this->lesson->tmima->school->schoolYear,
				$this->studentId,
				$this->nestorid
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->schoolRegistrationId = $row['id'];

			return 0;
		}

		//go on and store the registration of this student
		$dbParams = array(
				$this->course->class->school->id,
				$this->course->class->school->schoolYear, //θεωρούμε ότι εγγραφή γίνεται το τρέχων έτοσ
				$this->studentId,
				$this->nestorid
				);
		$this->schoolRegistrationId = insertSchoolregistration( 
				$this->dbConnection, $dbParams );
		if ($this->schoolRegistrationId == FALSE)
		{
			echo "Failed to register student in school<br>\n";
			$this->display_html();
			die;
		}
		else
		{ 
			//echo "student school registration stored<br>\n";
      return 1;
		}
	}


	public $classRegistrationId = -1;
	function storeStudentClassRegistration()
	{
		if ($this->classRegistrationId!=-1)
		{
			return -1;
		}

		//check if this registration already exists
		$row = getAClassRegistration($this->dbConnection,
				$this->studentId,
				$this->course->class->id
				);
		if (isset($row) && count($row)>0)
		{
			//print_r( $row );
			//one match found. For sure it's the correct one.
			$this->classRegistrationId = $row['id'];

			return 0;
		}

		//go on and store the registration of this student
		$dbParams = array(
				$this->studentId,
				$this->course->class->id
				);
		$this->classRegistrationId = insertSchoolclassregistration( 
				$this->dbConnection, $dbParams );
		if ($this->classRegistrationId == FALSE)
		{
			echo "Failed to register student<br>\n";
			die;
		}
		else
		{ 
			//echo "student registration stored<br>\n";
      return 1;
		}
	}

	function storeStudentMark()
	{
		if ($this->DOINSERTGRADES == FALSE) {
			return -1;
		}

		for ($mi = 0; $mi<count($this->mark); $mi++)
		{
			if ($mi>=9) {
				/* XXX mhn pas panw apo 9 giati einai oi vathmoi tous!  */
				echo "HIT THE ROOF!!!!<br>";
				return;
			}

			/*
			   m for mark
			   p for period
			 */

			$m = $this->mark[$mi];
			$m = rtrim( $m );

			/* Ελεγχος για το αν ο βαθμός είναι αποδεκτός. */
			if (strcmp ($m, "")==0) continue; //το κενό δεν το καταχωρούμε καθόλου
			if ($m <0 || $m>20) //εκτός ορίων βαθμός δεν καταχωρείται
			{
				echo "<br>Απαράδεκτος βαθμός στην θέση $mi ($m)<br>\n";
				$this->display();
				echo "<br><br>\n";
				continue;
			}

			$p = -1;
			if (preg_match( "/^Α .+/", $this->period[$mi])==1) {
				$p = A_TETRAMINO;
			}
			elseif (preg_match( "/^Β .+/", $this->period[$mi])==1) {
				$p = B_TETRAMINO;
			}
			elseif (preg_match( "/^Γ .+/", $this->period[$mi])==1) {
				$p = G_TETRAMINO;
			}

			if ($p == -1) 
			{
				$per = $this->period[$mi];
				echo "<br>Αγνοώ τον βαθμό $m της περιόδου $per! Προς το παρόν αυτή η περίοδος δεν υποστηρίζεται.</br>";
				continue;
			}



			//try to update an existing value first
			$dbParams = array(
					$m,
					0, //XXX locked
					$this->studentId,
					$this->course->id, 
					$p
					);
			$result = updateGrade( $this->dbConnection,
					$dbParams );
			/*---
			echo "Storing mark $m for period $mi for student $this->studentId<br>\n";
			*/
			if ($result == -1)
			{
				echo "Πρόβλημα στην ενημέρωση βαθμού για τον <br>\n";
				$this->display();
				echo "<br><br>\n";
			}

			if ($result == 0)
			{
				/* no update -> we need to insert this grade */
				$dbParams = array(
						$this->studentId,
						$this->course->id, 
						$p, 
						$m, //grade
						0 //XXX locked
						);
				$result = insertGrade( $this->dbConnection,
						$dbParams );

				if ($result==FALSE || $result<=0)
				{
					echo "Πρόβλημα στην προσθήκη βαθμού για τον <br>\n";
					$this->display();
					echo "<br><br>\n";
				}
			}
		}
	}


}

?>
