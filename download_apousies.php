<?php 
/*error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);*/
$CURRENT_TERM=$GLOBAL['CURRENT_TERM'];
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
require_once("models/config.php");
require_once("models/db-queries.php");
ob_clean();
if(isset($_GET['school'])){
        $school_id = $_GET['school'];
        if(isset($_GET['classnumber'])){  //download specific lesson 
// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

// Set document properties
	$objPHPExcel->getProperties()->setCreator("School")
	->setLastModifiedBy("User")
	->setTitle("Office 2003 XLS Document for inserting absences to myschool")
	->setSubject("Office 2003 XLS Document for inserting absences to myschool")
	->setDescription("Office 2003 XLS Document for inserting absencees to myschool")
	 ->setKeywords("Office 2003 XLS Document for inserting absences to myschool")
	 ->setCategory("Absencees");

//get some data
         $classnumber = $_GET['classnumber'];
         $school = getASchool($grmysqli, $school_id );
         $schoolname = $school['schoolname'];

// Redirect output to a client�~@~Ys web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$schoolname.' - '.$classnumber.' τάξη-απουσίες - '.date("d-m-Y").'.xls"');
	header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

  $sql =  " select cls.id as class_id, cls.classlevel, cls.startyear, crs.id as course_id, crs.teacher_id, " . //TODO :maybe not necessary
    " crs.lesson_id, crs.schoolclass_id, lsn.lessonname " .
    " from " .
    " SCHOOLCLASSES as cls, SCHOOLCOURSES as crs, SCHOOLLESSONS as lsn " .
    " WHERE crs.schoolclass_id=cls.id and crs.lesson_id = lsn.id and " .
    " cls.SCHOOL_ID=? and cls.classnumber=? and lsn.lessonname LIKE $SYNOLO_APOYSIWN;"; 
	$data =array( $school_id, $classnumber);
        $sql = preparedQuery($grmysqli, $sql, $data);
        $result = mysqli_query($grmysqli, $sql);
	

        if (!isset($result) || $result==FALSE)  {
                echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
                return null     ;
        }
	$course_no =0;
        while($row = mysqli_fetch_array($result)) {
		$course_no+=1;
                $course_id = $row['course_id'];
                $lesson_id = $row['lesson_id'];
                $teacher_id = $row['teacher_id'];
		$teacher = getTeacher($grmysqli , $teacher_id);
		$lessonname = $row['lessonname'];//getLessonName($grmysqli, $row['lesson_id']); 
		$coursename = $row['classlevel'].$classnumber.'  -  '.$lessonname;//getLessonName($grmysqli, $row['lesson_id']);
		/*
			Due to greeks being UTF they consume TWO bytes each.
			Therefore we need to cut sheetname to 31 bytes divided by two.
			However this is not correct due to spaces and other special characters using only one byte.
			Therefore what we do is convert the string to greeklish and 
			remove spaces so as to gain more useful characters in the sheetname.
		*/
		$sheetname = greek_to_greeklish($row['classlevel'].'-'.$lessonname); //will also remove the spaces
		$sheetname = substr($sheetname, 0, 31); //max 31 chars for sheetname
		if ($course_no>1){
			$newWorkSheet = new PHPExcel_Worksheet($objPHPExcel, $sheetname);
			$objPHPExcel->addSheet($newWorkSheet, 0); //put new sheet first
		}else{
			$objPHPExcel->getActiveSheet()->setTitle($sheetname);
		}
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueByColumnAndRow(0, 1, $schoolname)
		->setCellValueByColumnAndRow(0, 2, $teacher['lastname'] . ' ' .  $teacher['firstname'])
		->setCellValueByColumnAndRow(0, 3, $coursename)
		->setCellValueByColumnAndRow(0, 4, $row['startyear'])
		->setCellValueByColumnAndRow(0, 5, 'ΑΜ')
                ->setCellValueByColumnAndRow(1, 5, 'Συνολικές απουσίες')
		->setCellValueByColumnAndRow(2, 5, 'Δικαιολ. απουσίες')
		->setCellValueByColumnAndRow(3, 5, 'Α/A')
                ->setCellValueByColumnAndRow(4, 5, 'Επώνυμο')
                ->setCellValueByColumnAndRow(5, 5, 'Όνομα')
                ->setCellValueByColumnAndRow(6, 5, 'Πατρώνυμο')
                ->setCellValueByColumnAndRow(7, 5, 'Μητρώνυμο');
		$header_lines_no = 5;
		$students = getClassStudents($grmysqli, $school_id , $row['schoolclass_id']);
		$student_no = 0;
		foreach($students as $class_student){
			$student_no+=1;
			$grade_result = getAStudentLessonGrade( $grmysqli, $class_student['id'], $course_id, $CURRENT_TERM);
			$grade = $grade_result['grade']; // here we have the syn-apousies, not the grade!!!!next get the dik-apousies!!!!!!!!!
			////////////////////////////////////////////just get student am
			$sql = "select * from SCHOOLREGISTRATIONS where school_id=? and student_id=? limit 1;";
			$data =array( $school_id, $class_student['id'] );
			$sql = preparedQuery($grmysqli, $sql, $data);
			$result_reg = mysqli_query($grmysqli, $sql);

			if (!isset($result_reg) || $result_reg==FALSE)  {
				echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
				return null;
			}
			$student_school_registration = mysqli_fetch_array($result_reg);	
			$student_am = $student_school_registration['student_am']; 
			/////////////////////////////////////////find the dik apousies
			$dik_apousies_lesson=getASchoolLesson($grmysqli, str_replace("ΣΥΝΟΛΟ ΑΠΟΥΣΙΩΝ", "ΔΙΚΑΙΟΛΟΓΗΜΕΝΕΣ ΑΠΟΥΣΙΕΣ",  $lessonname));
			if (sizeof($dik_apousies_lesson>0)){
				$dik_apousies_course = getASchoolCourseNoTeacher($grmysqli,  $row['class_id'], $dik_apousies_lesson['id']);  
				$grade_dik_apousies = getAStudentLessonGrade( $grmysqli, $class_student['id'],  $dik_apousies_course['id'], $CURRENT_TERM);
                        	$dik_apousies_no = $grade_dik_apousies['grade']; 
			}else{
				$dik_apousies_no = "not found-????";
			}
			// now fill the excel with values
			$objPHPExcel->setActiveSheetIndex(0)
	                ->setCellValueByColumnAndRow(0, $student_no+$header_lines_no, $student_am)
                	->setCellValueByColumnAndRow(1, $student_no+$header_lines_no, $grade) // syn-apousies here!!!!!!!!!!!!!!!
			->setCellValueByColumnAndRow(2, $student_no+$header_lines_no, $dik_apousies_no)
			->setCellValueByColumnAndRow(3, $student_no+$header_lines_no, $student_no)
			->setCellValueByColumnAndRow(4, $student_no+$header_lines_no, $class_student['lastname'])
			->setCellValueByColumnAndRow(5, $student_no+$header_lines_no, $class_student['firstname'])
			->setCellValueByColumnAndRow(6, $student_no+$header_lines_no, $class_student['fathername'])
			->setCellValueByColumnAndRow(7, $student_no+$header_lines_no, $class_student['mothername']);
		}
	}      
}
else{//find alllessons for teacher

}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
exit; 
}

//source : http://code.loon.gr/snippet/php/%CE%BC%CE%B5%CF%84%CE%B1%CF%84%CF%81%CE%BF%CF%80%CE%AE-greek-%CF%83%CE%B5-greeklish
function greek_to_greeklish($string)
{
                return strtr($string, array(
                                'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
                                'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'KS', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F',
                                'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'O',
                                'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
                                'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'ks', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
                                'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
                                'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ώ' => 'o',
                                'ϊ' => 'i', 'ϋ' => 'y',
                                'ΐ' => 'i', 'ΰ' => 'y',

				/* added by i2g because they are invalid characters */
				'[' => '', ']' => '',
				'*'=>'', '?'=>'',
				':'=>'', '/'=>'',
				'\\'=>'',
				' ' => '' /* ADDED BY i2g TO REMOVE SPACES */
                ));
}

?>
