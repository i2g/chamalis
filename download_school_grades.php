<?php 

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
require_once("models/config.php");
require_once("models/db-queries.php");

$Settings->loadGlobalSettings();
$CURRENT_TERM=$Settings->currentTerm;
$DEBUG=$Settings->getDebug();


if ($DEBUG) {
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);
}

date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
require_once("models/config.php");
require_once("models/db-queries.php");
if (!$DEBUG) { ob_clean(); }

if(isset($_GET['school'])){
  $school_id = $_GET['school'];
  if(isset($_GET['classnumber'])){

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("School")
      ->setLastModifiedBy("User")
      ->setTitle("Office 2003 XLS Document for inserting grades to myschool")
      ->setSubject("Office 2003 XLS Document for inserting grades to myschool")
      ->setDescription("Office 2003 XLS Document for inserting grades to myschool")
      ->setKeywords("Office 2003 XLS Document for inserting grades to myschool")
      ->setCategory("Grades");

    //get some data
    $classnumber = $_GET['classnumber'];
    $school = getASchool($grmysqli, $school_id );
    $schoolname = $school['schoolname'];

    if (!$DEBUG) {
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$schoolname.' - '.$classnumber.' τάξη - '.date("d-m-Y").'.xls"');
      header('Cache-Control: max-age=0');
      header('Cache-Control: max-age=1');
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0
    }
    else
    {
      header ('Content-Type: text/html; charset=utf-8'); 
    }

    $sql =  "select cls.id as class_id, cls.classlevel, cls.startyear, " .
      " crs.id as course_id, tsc.teacher_id, crs.lesson_id, crs.schoolclass_id " .
      " from " .
      " SCHOOLCLASSES as cls INNER JOIN SCHOOLCOURSES as crs INNER JOIN TEACHERSOFSCHOOLCOURSES as tsc  " .
      " WHERE crs.schoolclass_id= cls.id ". 
      " and crs.id= tsc.schoolcourse_id ". 
      " and cls.SCHOOL_ID=? and cls.classnumber=? " .
      ";";

    if ($DEBUG) {
      echo "<p>$sql</p>";
      echo "<p> school_id=$school_id and classnumber=$classnumber </p>";
    }

    $data =array( $school_id, $classnumber);
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null     ;
    }

    $course_no = 0;
    while($row = mysqli_fetch_array($result)) {
      if ($DEBUG) {
        echo "Data:";
        print_r ($row);
        echo "<br>";
      }

      $course_no+=1;
      $course_id = $row['course_id'];
      $lesson_id = $row['lesson_id'];
      $teacher_id = $row['teacher_id'];
      $teacher = getTeacher($grmysqli , $teacher_id);
      if ($DEBUG) {
        echo "Teacher dump:";
        print_r($teacher);
        echo "<br>";
      }
      $lessonname = $row['classlevel'].$classnumber.'  -  '.getLessonName($grmysqli, $row['lesson_id']);
      if ($DEBUG) {
        echo "Lesson dump:";
        echo "<p>$lessonname</p>";
        echo "<br>";
      }
      $sheetname = substr(getLessonName($grmysqli, $row['lesson_id']), 0, 61); //max 31 chars for sheetname
      $sheetname = substr(greek_to_greeklish( $sheetname ), 0, 28);
      if ($DEBUG) {
        echo "<p>$sheetname</p>";
      }
      if ($course_no>1){
        $newWorkSheet = new PHPExcel_Worksheet($objPHPExcel, $sheetname);
        $objPHPExcel->addSheet($newWorkSheet, 0); //put new sheet first
      }else{
        $objPHPExcel->getActiveSheet()->setTitle($sheetname);
      }
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueByColumnAndRow(0, 1, $schoolname)
        ->setCellValueByColumnAndRow(0, 2, $teacher['lastname'] . ' ' .  $teacher['firstname'])
        ->setCellValueByColumnAndRow(0, 3, $lessonname)
        ->setCellValueByColumnAndRow(0, 4, $row['startyear'])
        ->setCellValueByColumnAndRow(0, 5, 'ΑΜ')
        ->setCellValueByColumnAndRow(1, 5, 'Βαθμός')
        ->setCellValueByColumnAndRow(2, 5, 'Α/A')
        ->setCellValueByColumnAndRow(3, 5, 'Επώνυμο')
        ->setCellValueByColumnAndRow(4, 5, 'Όνομα')
        ->setCellValueByColumnAndRow(5, 5, 'Πατρώνυμο')
        ->setCellValueByColumnAndRow(6, 5, 'Μητρώνυμο');

      /*
        Δεν νομίζω να δουλεύει τώρα.. (διαχωρισμός Θεωρίας, εργαστηρίου
        για υπολογισμό μέσου όρου).

      $pos_theoria = strpos($lessonname, '(Θ)');
      if ($pos_theoria !== false) { ////////////////////////////// it is theoria so search ergastirio for m/o
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, 5, 'Μ/Ο (Θ+Ε)');
        $ergast_lessonname = str_replace("(Θ)", "(Ε)", $lessonname); // lessonname of ergastirio
        $lesson_set= getLessonSet($grmysqli, $lessonname);
        if (count($lesson_set) > 1){
          foreach($lesson_set as $lesson_set_lesson){
            $pos_ergastirio = strpos($lesson_set_lesson['lessonname'], '(Ε)');
            if ($pos_ergastirio !== false) {
              $ergast_lesson_id = $lesson_set_lesson['id'];
              $ergast_course = getASchoolCourseNoTeacher($grmysqli,  $row['schoolclass_id'], $ergast_lesson_id);
              $ergast_course_id =  $ergast_course['id'];
              $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, 5, $lesson_set_lesson['lessonname']);
              break;
            }
          }
        }
        else{
          $pos_theoria = false;
        }
      }
      ///////////////////////////////////////////end get ergastirio
       */
      $header_lines_no = 5;
      $students = getClassStudents($grmysqli, $school_id , $row['schoolclass_id']);
      $student_no = 0;
      foreach($students as $class_student){
        if ($DEBUG) {
          print_r ($class_student);
          echo "<br>";
        }
        $student_no+=1;
        $student_am = $class_student['student_am']; 
        $grade_result = getAStudentLessonGrade( $grmysqli, $class_student['id'], $course_id, $CURRENT_TERM);
        $grade = $grade_result['grade'];
        if ($DEBUG) {
          if ($grade==0 || !isset($grade))
          { 
            echo "no grade.<br>";
          } else {
            echo "$grade<br>";
          }
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValueByColumnAndRow(0, $student_no+$header_lines_no, $student_am)
          ->setCellValueByColumnAndRow(1, $student_no+$header_lines_no, $grade)
          ->setCellValueByColumnAndRow(2, $student_no+$header_lines_no, $student_no)
          ->setCellValueByColumnAndRow(3, $student_no+$header_lines_no, $class_student['lastname'])
          ->setCellValueByColumnAndRow(4, $student_no+$header_lines_no, $class_student['firstname'])
          ->setCellValueByColumnAndRow(5, $student_no+$header_lines_no, $class_student['fathername'])
          ->setCellValueByColumnAndRow(6, $student_no+$header_lines_no, $class_student['mothername']);
        /*
        if ($pos_theoria !== false) { // there is also ergastirio so get erg. grade for mo
          $ergast_grade_result = getAStudentLessonGrade( $grmysqli, $class_student['id'], $ergast_course_id, $CURRENT_TERM);
          $ergast_grade = $ergast_grade_result['grade'];
          $mo_grade = round(($grade+$ergast_grade)/2);
          $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $student_no+$header_lines_no, $mo_grade);
        }
         */

      }
    }

    if (!$DEBUG) {
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      //$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
      $objWriter->save('php://output');
      exit; 
    }
  }
  else{
    echo "No school or class selected.";
  }
}

//source : http://code.loon.gr/snippet/php/%CE%BC%CE%B5%CF%84%CE%B1%CF%84%CF%81%CE%BF%CF%80%CE%AE-greek-%CF%83%CE%B5-greeklish
function greek_to_greeklish($string)
{
                return strtr($string, array(
                                'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
                                'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'KS', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F',
                                'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'O',
                                'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
                                'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'ks', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
                                'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
                                'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ώ' => 'o',
                                'ϊ' => 'i', 'ϋ' => 'y',
                                'ΐ' => 'i', 'ΰ' => 'y',
				/* added by i2g because they are invalid characters */
				'[' => '', ']' => '',
				'*'=>'', '?'=>'',
				':'=>'', '/'=>'',
				'\\'=>'',
				'-'=>'_',
				' ' => '' /* ADDED BY i2g TO REMOVE SPACES */
                ));
}
?>
