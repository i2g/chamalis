<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
/*
 */
   error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once("models/config.php");
require_once("models/db-queries.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$captcha = md5($_POST["captcha"]);
	
	
	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
	//End data validation
	if(count($errors) == 0)
	{	
		require_once("models/header.php");
		echo "
			<body>
			<div id='wrapper'>
			<div id='top'><div id='logo'></div></div>
			<div id='content'>
			<h1>Συλλογή Βαθμών</h1>
			<h2>Αδειασμα όλης της βάσης του σχολείου</h2>

			<div id='left-nav'>";
		include("left-nav.php");
		echo "
			</div>

			<div id='main'>";
		$schoolid = $_POST['school'];


		/* Τσέκαρε οπωσδήποτε τον κωδικό!
		   Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
		if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1)
		{
			echo "Κωδικός σχολείου ($schoolid) μη αποδεκτός. Τίποτα δεν έγινε...";
			die;
		}


		if (deleteGradesForSchool( $grmysqli, $schoolid ))
		{
			echo "Ολα καλά, κατάφερα και έσβησα τους βαθμούς χωρίς κανένα λάθος.";
		}
		else
		{
			echo "Δυστυχώς απέτυχα. Και το χειρότερο... Δεν ξέρω τι γίνεται με τη βάση! Καλή τύχη!";
		}


		die;
	}
}

require_once("models/header.php");
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Αδειασμα όλης της βάσης του σχολείου</h2>

<div id='left-nav'>";
include("left-nav.php");
echo "
</div>

<div id='main'>";

echo resultBlock($errors,$successes);
$loggedInUsername = $loggedInUser->username;
$schools = mysqli_query($grmysqli,
		"select school_id as id, schoolname " .
		" from USERS inner join SCHOOLS " .
	        " on USERS.school_id = SCHOOLS.id " .
		" where username='$loggedInUsername';" );

echo "
<div id='regbox'>
<form name='newUser' action='".$_SERVER['PHP_SELF']."' method='post'>

<p>
Προσοχή! Αν προχωρήσετε θα σβηστούν οι βαθμοί όλων των μαθημάτων.
</p>
<p>
Θα σβηστούν:
<ul>
<li>μόνο οι βαθμοί (<b>κάθε είδους</b> βαθμός που είναι περασμένος) </li>
</ul>
Θα παραμείνουν οι καθηγητές και οι λογαριασμοί τους, οι μαθητές και τα τμήματα που ανήκουν όπως και τα μαθήματα που κάνουν.
</p>
<p>
		<P>Σχολείο:<br>
		<select name=\"school\" id ='school')\">
			<option>Επιλέξτε σχολείο</option>";

			while($row = mysqli_fetch_array($schools)){
				echo '<option value="'.$row['id'].'">'.$row['schoolname'].'</option>';  
			}
echo "
		</select></P>
<label>Κωδικός Ασφαλείας</label>
<img src='models/captcha.php'>
</p>
<label>Παρακαλώ εισάγετε τον κωδικό που βλέπετε:</label>
<input name='captcha' type='text'>
<label>&nbsp;<br>
<input type='submit' value='Delete all'/>

</form>
</div>

</div>
</div>
</body>
</html>";
?>
