<?php 

/*
*/
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
require_once("models/config.php");
require_once("models/db-queries.php");

//Note: If debuging is enabled, then the XLS is NOT generated
$Settings->loadGlobalSettings();
$DEBUG_1 = $Settings->getDebug();
$DEBUG_2 = FALSE;
$DO_CROSS_CHECK = TRUE;

ob_clean();

$school_id = $_GET['school'];
$schoolname= $school_id;
$classnumber = $_GET['classnumber'];


$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("i2g")
->setLastModifiedBy("i2g")
->setTitle("Office 2003 XLS Document for inserting grades to myschool")
->setSubject("Office 2003 XLS Document for inserting grades to myschool")
->setDescription("Office 2003 XLS Document for inserting grades to myschool")
->setKeywords("Office 2003 XLS Document for inserting grades to myschool")
->setCategory("Grades");
$activeSheet = $objPHPExcel->setActiveSheetIndex(0);
$activeSheet->setTitle($classnumber);

if ($DEBUG_2 || $DEBUG_1) header ('Content-Type: text/html; charset=utf-8'); 
if (!$DEBUG_1) 
{
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$schoolname.' - '.$classnumber.' τάξη Εννιαίο - '.date("d-m-Y").'.xls"');
	header('Cache-Control: max-age=0');
	header('Cache-Control: max-age=1');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
}


$sql = " select " .
" SCHOOLREGISTRATIONS.student_am as nestor_id, STUDENTS.firstname, STUDENTS.lastname, STUDENTS.fathername, STUDENTS.mothername, " .
" SCHOOLLESSONS.lessonname, " .
" GRADES.grade " .
" from  " .
" STUDENTS " .
" inner join SCHOOLREGISTRATIONS on STUDENTS.id=SCHOOLREGISTRATIONS.student_id " .
" inner join SCHOOLCLASSREGISTRATIONS on STUDENTS.id=SCHOOLCLASSREGISTRATIONS.student_id " .
" inner join SCHOOLCOURSES on SCHOOLCLASSREGISTRATIONS.schoolclass_id = SCHOOLCOURSES.schoolclass_id " .
" inner join GRADES on GRADES.schoolcourse_id = SCHOOLCOURSES.id and GRADES.student_id=STUDENTS.id " .
" inner join SCHOOLLESSONS on SCHOOLCOURSES.lesson_id = SCHOOLLESSONS.id " .
" inner join SCHOOLCLASSES on SCHOOLCOURSES.schoolclass_id = SCHOOLCLASSES.id and SCHOOLCLASSES.school_id=SCHOOLREGISTRATIONS.school_id " .
" where " .
" SCHOOLREGISTRATIONS.school_id=? and SCHOOLCLASSES.classnumber=? and GRADES.term=" . $Settings->currentTerm . 
" order by " .
" STUDENTS.id;";

$data =array($school_id, $classnumber);
$sql = preparedQuery($grmysqli, $sql, $data);
$result = mysqli_query($grmysqli, $sql);

if (!isset($result) || $result==FALSE)  {
	echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
	return null     ;
}

if ($DEBUG_1) echo "<br>---- start of result ----<br>";
if ($DEBUG_1) echo "<table>";

$headerLinesNo=1;	/* Γραμμή που περιέχει τους τίτλους των μαθημάτων */
$activeSheet->setCellValueByColumnAndRow(0,$headerLinesNo++, "Σχολείο: $school_id"); 
$activeSheet->setCellValueByColumnAndRow(0,$headerLinesNo++, "Τάξη: $classnumber Τάξη"); 
$activeSheet->setCellValueByColumnAndRow(0,$headerLinesNo++, "Βαθμοί: ");

$allLessonColumns[]=""; /* Το νούμερο της στήλης που περιέχει κάθε μάθημα */

$nextFreeColumn = 0;    /* 0: νέστωρ αμ, 1: επώνυμο, 2: όνομα, 3: πατρώνυμο, 4: μητρώνυμο */
$activeSheet->setCellValueByColumnAndRow($nextFreeColumn++,$headerLinesNo, "Μητρώο");
$activeSheet->setCellValueByColumnAndRow($nextFreeColumn++,$headerLinesNo, "Επώνυμο");
$activeSheet->setCellValueByColumnAndRow($nextFreeColumn++,$headerLinesNo, "Ονομα");
$activeSheet->setCellValueByColumnAndRow($nextFreeColumn++,$headerLinesNo, "Πατρώνυμο");
$activeSheet->setCellValueByColumnAndRow($nextFreeColumn++,$headerLinesNo, "Μητρώνυμο");

$curStudentId = -1;
$curStudentLastname = "";
$curStudentFirstname = "";
$curStudentFathername = "";
$curStudentMothername = "";

$studentCount = 0;
while ( $row = mysqli_fetch_array( $result ) )
{
	$lessonName = $row['lessonname'];
	$lessonColumn = -1;

	/* Check whether we have a dedicated column for the lesson */
	if (isset( $allLessonColumns[ $lessonName ] ) )
	{
		$lessonColumn = $allLessonColumns[ $lessonName ];
	}
	else
	{
		$lessonColumn = $nextFreeColumn;
		$nextFreeColumn = $nextFreeColumn + 1;
		$allLessonColumns[ $lessonName ] = $lessonColumn;
		$allLessonColumns[ $lessonColumn ] = $lessonName;
		$activeSheet->setCellValueByColumnAndRow($lessonColumn, $headerLinesNo, $lessonName );
	}

	$curStudentId = $row['nestor_id'];
	$curStudentLastname = $row ['lastname'];
	$curStudentFirstname = $row ['firstname'];
	$curStudentFathername = $row ['fathername'];
	$curStudentMothername = $row ['mothername'];
	$curStudentGrade = $row[ 'grade' ];

	/* Check whether current student is a new one. Remember rows are ordered by student_id */
	$pendingStudentId = $activeSheet->getCellByColumnAndRow(0, $studentCount+$headerLinesNo)->getValue();
	if ($pendingStudentId==NULL || isset($pendingStudentId)==FALSE || $pendingStudentId != $curStudentId)
	{
		$studentCount ++; /* move to next line */

		/* set static data for current student */
		$activeSheet->setCellValueByColumnAndRow(0, $studentCount+$headerLinesNo, $curStudentId);
		$activeSheet->setCellValueByColumnAndRow(1, $studentCount+$headerLinesNo, $curStudentLastname);
		$activeSheet->setCellValueByColumnAndRow(2, $studentCount+$headerLinesNo, $curStudentFirstname);
		$activeSheet->setCellValueByColumnAndRow(3, $studentCount+$headerLinesNo, $curStudentFathername);
		$activeSheet->setCellValueByColumnAndRow(4, $studentCount+$headerLinesNo, $curStudentMothername);
	}

	if ($DO_CROSS_CHECK)
	{
		/* Εδώ γίνεται ένας έλεγχος ότι ο μαθητής για τον οποίο θα συμπληρώσουμε τον βαθμό
		   είναι ο ίδιος με αυτόν που βρίσκεται στο result-set απο την βαση */
		$pId = $activeSheet->getCellByColumnAndRow(0, $studentCount+$headerLinesNo)->getValue();
		$pLa = $activeSheet->getCellByColumnAndRow(1, $studentCount+$headerLinesNo)->getValue();
		$pFi = $activeSheet->getCellByColumnAndRow(2, $studentCount+$headerLinesNo)->getValue();
		$pFa = $activeSheet->getCellByColumnAndRow(3, $studentCount+$headerLinesNo)->getValue();
		$pMo = $activeSheet->getCellByColumnAndRow(4, $studentCount+$headerLinesNo)->getValue();
		$pGr = $activeSheet->getCellByColumnAndRow($lessonColumn, $studentCount+$headerLinesNo)->getValue();

		if ( $pId != $curStudentId  ||
				strcmp ($pLa, $curStudentLastname)!=0 ||
				strcmp ($pFi, $curStudentFirstname) != 0 ||
				strcmp ($pFa, $curStudentFathername) != 0 ||
				strcmp ($pMo, $curStudentMothername) !=0 )
		{
			echo "MISMATCH!";
			echo "Line ($studentCount+$headerLinesNo) is for $pId $pLa $pFi but I am filling for ";
			echo "$curStudentId $curStudentLastname $curStudentFirstname!"; 
			die;
		}

		if (isset( $pGr ) && $pGr!=$curStudentGrade)
		{
			echo "MISMATCH on grades!";
			echo "Previous grade for $lessonName was $pGr.";
			echo "New grade for $lessonName  is $curStudentGrade.";
			echo "(Student $curStudentLastname $curStudentFirstname)";
			echo "<br>\n";
			die; //ych 9/May/2015 - Should never occur.
		}
	}

	/* set the grade */
	$activeSheet->setCellValueByColumnAndRow($lessonColumn, $studentCount+$headerLinesNo, $curStudentGrade);

	if ($DEBUG_1) {
		echo "<tr>";
		$colData = $row['nestor_id'];
		echo "<td>$colData</td>";
		$colData = $row['lastname'];
		echo "<td>$colData</td>";
		$colData = $row['firstname'];
		echo "<td>$colData</td>";
		$colData = $row['fathername'];
		echo "<td>$colData</td>";
		$colData = $row['mothername'];
		echo "<td>$colData</td>";
		$colData = $lessonName . "($lessonColumn)";
		echo "<td>$colData</td>";
		$colData = $row['grade'];
		echo "<td>$colData</td>";
		echo "</tr>";
	}
}


if ($DEBUG_1)
{
	echo "Counted $studentCount students<br>";

	echo "</table>";
	echo "<br>---- end of result ----<br>";
}

if (!$DEBUG_1 && !$DEBUG_2)
{
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
}
?>
