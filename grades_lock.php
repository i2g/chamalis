<?php
require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}
if(!isUserLoggedIn()) { header("Location: login.php"); die(); }
require_once("models/header.php");
require_once("models/db-queries.php");

error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

?>
<body>

<div id='wrapper'>
<div id='top'>
	<div id='logo'></div>
</div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Διαχείριση Βαθμολογίας Τάξεων</h2>
<div id='left-nav'>

<?php 
include("left-nav.php"); 


?>

</div>
<div id='main'>
<?php

function updateLockStatus($grmysqli, $schoolid, $status){
global $Settings;
	$allteacher = getSchoolTeachers( $grmysqli, $schoolid);
	
	//print_r($allteacher);	
		
	foreach($allteacher as $tempteach){
		$lessons = getTeacherLessons( $grmysqli,$tempteach['id']);
		
	//	echo "<br>".$tempteach['lastname'];
	//	print_r($lessons);
		
		foreach($lessons as $les){
			$res = updateSchoolcourseLockStatus($grmysqli, $status ,$les['schoolcourseid'],$Settings->currentTerm);
			//echo "<br>". $status ." -- ".$les['schoolcourseid'] ." -- ".$les['lessonname'];
			
			if(($res>0) && ($status == 1)){
				echo "<br><span id='infook'>Κλείδωσα το μάθημα ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']."</span>";
			}else if(($res>0) && ($status == 0)){
				echo "<br><span id='infook'>Ξεκλείδωσα το μάθημα ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']."</span>";
			}else if($res == 0){
				echo "<br><span>Δεν έγιναν αλλαγές στο μάθημα ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']."</span>";
			}else{
				echo "<br><span id='infoerror'>Σφάλμα κατά το κλείδωμα του μαθήματος ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']."</span>";
				//triger_error(mysqli_error($grmysqli));
			}
		}			
	}
	
}


//ΚΛΕΙΔΩΜΑ/ΞΕΚΛΕΙΔΩΜΑ ΜΑΘΗΜΑΤΩΝ ΣΥΓΚΕΚΡΙΜΕΝΟΥ ΚΑΘΗΓΗΤΗ
if(isset($_GET['savestatus'])){
	$nextstatus = $_GET['nextstatus'];
	$prevstatus = $_GET['currstatus'];
	$currcourseids = $_GET['currcourseid'];
	$currclass = $_GET['currclasses'];

	$size = sizeof($nextstatus);
	$msg = array();
					
	for($i=0; $i<$size; $i++){
		$schcrs = getSchoolCourse($grmysqli, $currcourseids[$i]);
		$name = getLessonName($grmysqli,$schcrs['lesson_id'] );
		
		if( $prevstatus[$i]==1){
			if(strcmp($nextstatus[$i], "false")==0){			 			
				updateSchoolcourseLockStatus($grmysqli, 0 ,$currcourseids[$i],$Settings->currentTerm);			
				$e="<br><span id='infook'>Ξεκλείδωσα το μάθημα ". $name ." του ".$currclass[$i] ."</span>";
				array_push($msg, $e);
			}else{
				$e="<br>Δεν χρειάστηκαν αλλαγές για το μάθημα ". $name ." του ".$currclass[$i];
				array_push($msg, $e);
			}			
		}else if($prevstatus[$i]==0){		
			if(strcmp($nextstatus[$i], "true")==0){
				updateSchoolcourseLockStatus($grmysqli, 1 ,$currcourseids[$i],$Settings->currentTerm);			
				$e="<br><span id='infook'>Κλείδωσα το μάθημα ". $name ." του ".$currclass[$i] ."</span>";
				array_push($msg, $e);
			}else{
				$e="<br>Δεν χρειάστηκαν αλλαγές για το μάθημα ". $name ." του ".$currclass[$i];
				array_push($msg, $e);
			}
		}else{
			$e="<br>Δεν χρειάστηκαν αλλαγές για το μάθημα ". $name ." του ".$currclass[$i];
			array_push($msg, $e);
		}
	
	}
	
	foreach($msg as $e){
		echo $e;
	}
?>	
	<br><input type="button" onclick="window.history.back();" name="back" value="Πίσω"/>
<?php
	die();
}
	
	
?>
<form action='grades_lock.php' name = 'selectclass' method='get'>
<?php
	$schoolid = $_GET['schoolid'];
	$teachid = $_GET['teacherid'];
	
	
	//ΔΩΣΕ ΑΝΑΦΟΡΑ ΚΛΕΙΔΩΜΑΤΟΣ ΚΑΙ ΚΑΤΑΧΩΡΙΣΗΣ ΒΑΘΜΩΝ
	 if($teachid == -50){
			//echo "----------------- Start reporting...<br>";
			echo "<input type='button' onclick='window.history.back();' name='back' value='Πίσω'/><br>";

			getLessonGradesStatus($grmysqli, $schoolid, $Settings->currentTerm);

			echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";
			//echo "----------------- End reporting...<br>";

	//ΚΛΕΙΔΩΣΕ ΟΛΑ ΤΑ ΜΑΘΗΜΑΤΑ
	}else if($teachid == -100){
			//echo "----------------- Start locking...<br>";
			echo "<input type='button' onclick='window.history.back();' name='back' value='Πίσω'/><br>";

			updateLockStatus($grmysqli, $schoolid, 1);

			echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";
			//echo "----------------- End locking...<br>";

	//ΞΕΚΛΕΙΔΩΣΕ ΟΛΑ ΤΑ ΜΑΘΗΜΑΤΑ
	}else if($teachid == -200){
                //echo "----------------- Start unlocking...<br>";

                echo "<input type='button' onclick='window.history.back();' name='back' value='Πίσω'/><br>";

                updateLockStatus($grmysqli, $schoolid, 0);

                echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";
        //      echo "----------------- End unlocking...<br>";

	//ΕΜΦΑΝΙΣΕ ΜΑΘΗΜΑΤΑ ΣΥΓΚΕΚΡΙΜΕΝΟΥ ΚΑΘΗΓΗΤΗ
	}else{

                $tempteach = getTeacher($grmysqli , $teachid);
                $counter = 1;

                echo "Καθηγητής: ". $tempteach['lastname'] ." ". $tempteach['firstname'] ."<br>";
                echo "
				<TABLE id='datatable'>
				<tr> 
						<td colspan=4 align='right'><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/></td>
						<td><INPUT TYPE='submit' NAME='savestatus' VALUE='Αποθήκευση'/></td>
				</tr>
				<TR  align='center'>
						<TD>Α/Α</TD>
						<TD>ΤΜΗΜΑ</TD>
						<TD colspan=2>ΜΑΘΗΜΑ</TD>
						<tD>ΚΛΕΙΔΩΜΕΝΟ</TD>
				</TR>";
				
				$lessons = getTeacherLessons( $grmysqli,$teachid);
				if($lessons <> null){
					foreach($lessons as $les){
						$courseid       = $les['schoolcourseid'];
						$classid        = $les['schoolclassid'];
						$tempclass = getAClass($grmysqli, $classid );
						//$st = getSchoolcourseLockStatus($grmysqli, $les['id'], $classid);
						$st = getSchoolcourseLockStatus2($grmysqli, $courseid, $Settings->currentTerm);

				//      if($st <> -1){
						echo "<TR";
						if(($counter%2)==1){
								echo(" class=\"alt\"");
						}
						$index = $counter-1;
						echo "><td align='center'>". $counter."</td><td>". $tempclass['classlevel'] ."</td><td colspan=2>" .$les['lessonname']. "</td>";


						echo "<td align='center'><input type='hidden' name='nextstatus[".$index."]' value='false'/>";
						echo "<input type='checkbox' name='nextstatus[".$index."]' value='true' ";
						if($st==1){
								echo " checked";
						}

						echo "/>";
						echo "<INPUT TYPE='hidden' name='currclasses[".$index."]' value='". $tempclass['classlevel'] ."'/>";
						if($st == -1){
								$st = 0;
								
						}
						echo "<INPUT TYPE='hidden' name='currstatus[".$index."]' value='". $st ."'/>";
						echo "<INPUT TYPE='hidden' name='currcourseid[".$index."]' value='". $courseid ."'/></td></tr>";
						$counter++;
						//      }
					}
				}else{
						//echo "<br>Σφάλμα κατά την εμφάνιση των μαθημάτων του καθηγητή!";
				}		
?>
		<tr> 
		<td colspan=4 align="right"><input type="button" onclick="window.history.back();" name="back" value="Πίσω"/></td>
		<td><INPUT TYPE='submit' NAME='savestatus' VALUE='Αποθήκευση'/></td></tr>
<?php 
		echo "</table>";
	}
	

?>
</form>
</div>
<div id='bottom'></div>
</div>
</body>
</html>
