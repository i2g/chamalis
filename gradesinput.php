<?php
require_once("models/config.php");

if (!securePage($_SERVER['PHP_SELF'])){die();}
if(!isUserLoggedIn()) { header("Location: login.php"); die(); }

require_once("models/header.php");

require_once("models/db-queries.php");
error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

?>

<body>

<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Καταχώρηση Βαθμών</h2>
<div id='left-nav'>
	
<?php
include("left-nav.php");
?>

</div>
<script src='js/jquery.min.js'></script>
<script src='models/funcs.js' type='text/javascript'></script>
<script type="text/javascript">
	
	
	
	
	
</script>
<div id='main'>

<?php

if(isset($_GET['savegrades'])){
	$allgrades 		= $_GET['allgrades'];			//checked						  	OK 		new grades for lesson 1
	$allstudents 	= $_GET['allstudents'];			//checked
	$allupdates 	= $_GET['allupdates'];			//checked -- split values using _  	OK 		previous grades...
	$schoolcourseid = $_GET['schoolcourse'];		//checked -- split values using _	OK
	$teacherid 		= $_GET['teacher'];				//checked -- split values using _	not used
	$lname_tmp		= $_GET['lname'];				//checked -- split values using _	OK
	$plithos 		= $_GET['count'];				//checked
	$csem		= $_GET['semester'];

	$i=0;
	$msg = array();
	
	//ΤΟ ΜΑΘΗΜΑ ΕΧΕΙ ΘΕΩΡΙΑ ΚΑΙ ΕΡΓΑΣΤΗΡΙΟ ΟΠΟΤΕ ΠΑΡΕ ΤΟΥΣ ΒΑΘΜΟΥΣ ΤΟΥ ΔΕΥΤΕΡΟΥ ΜΑΘΗΜΑΤΟΣ 
	//ΚΑΙ ΧΩΡΙΣΕ ΚΩΔΙΚΟΥΣ ΔΙΔΑΣΚΑΛΙΑΣ, ΟΝΟΜΑΤΑ ΜΑΘΗΜΑΤΟΣ ΚΑΙ ΦΤΙΑΞΕ ΠΙΝΑΚΑ ΜΕ ΤΟΥΣ ΜΕΣΟΥΣ ΟΡΟΥΣ


	if($plithos > 1){
		$allgrades2 = $_GET['allgrades2'];		//checked 	OK 		new grades for lesson 2		
		$tcourses 	= explode("_", $schoolcourseid, 2);
		$lnames_tmp = explode("_", $lname_tmp, 2);
		$lnames[] = getLessonName($grmysqli, $lnames_tmp[0]);
		$lnames[] = getLessonName($grmysqli, $lnames_tmp[1]);
		$mo			= array();		
	}
	else
	{
		$lname = getLessonName($grmysqli, $lname_tmp);
	}

		
	//ΓΙΑ ΚΑΘΕ ΜΑΘΗΤΗ ΚΑΝΕ ΤΑ ΠΑΡΑΚΑΤΩ
	foreach($allstudents as $onestudent){
		
		//ΤΟ ΜΑΘΗΜΑ ΕΧΕΙ ΘΕΩΡΙΑ ΚΑΙ ΕΡΓΑΣΤΗΡΙΟ ΟΠΟΤΕ ΔΙΑΧΩΡΙΣΕ ΤΟΥΣ ΗΔΗ ΚΑΤΑΧΩΡΗΜΕΝΟΥΣ ΣΤΗ ΒΑΣΗ ΒΑΘΜΟΥΣ
		if($plithos > 1){
			$tupdates 	= explode("_", $allupdates[$i], 2);
			
		//ΜΑΘΗΜΑ ΠΡΩΤΟ
				//ΑΝ ΔΕΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ ΤΩΡΑ ΕΙΣΑΓΕΤΑΙ ΝΕΟΣ, ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΙΣΑΓΩΓΗ
				if(($tupdates[0] == NULL)  && ($allgrades[$i] <> NULL)){									
					$t = checkNumeric($grmysqli, $allgrades[$i], $onestudent, $tcourses[0], $lnames[0], true);			
					
					//ΑΝ ΕΓΙΝΕ Η ΕΙΣΑΓΩΓΗ ΤΟΤΕ ΒΑΛΕ ΤΗΝ ΤΙΜΗ ΤΟΥ ΠΡΩΤΟΥ ΣΤΟ ΑΘΡΟΙΣΜΑ 
					if($t == 1){
						$mo[$i] = $allgrades[$i];
					
					//ΑΛΛΙΩΣ ΚΑΝΕ ΤΟ ΑΘΡΟΙΣΜΑ 0
					}else{
						$mo[$i] = 0;
					}					
				
				//ΑΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ Ο ΝΕΟΣ ΕΙΝΑΙ ΔΙΑΦΟΡΕΤΙΚΟΣ ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΝΗΜΕΡΩΣΗ
				}else if($tupdates[0] <> $allgrades[$i]){
					$t = checkNumeric($grmysqli, $allgrades[$i], $onestudent, $tcourses[0], $lnames[0], false);				
					
					//ΑΝ ΕΓΙΝΕ Η ΕΝΗΜΕΡΩΣΗ ΤΟΤΕ ΒΑΛΕ ΤΗΝ ΤΙΜΗ ΤΟΥ ΠΡΩΤΟΥ ΣΤΟ ΑΘΡΟΙΣΜΑ
					if($t == 1){
						$mo[$i] = $allgrades[$i];
						
					//ΑΛΛΙΩΣ ΒΑΛΕ ΤΟΝ ΠΡΟΗΓΟΥΜΕΝΟ ΒΑΘΜΟ
					}else{
						$mo[$i] = $tupdates[0];
					}
					
				//ΑΝ Ο ΒΑΘΜΟΣ ΔΕΝ ΑΛΛΑΞΕ ΒΑΛΕ ΤΟΝ ΣΤΟ ΑΘΡΟΙΣΜΑ
				}else if($tupdates[0] == $allgrades[$i]){
					$mo[$i] = $tupdates[0];
				}
		//ΜΑΘΗΜΑ ΔΕΥΤΕΡΟ		
				//ΑΝ ΔΕΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ ΤΩΡΑ ΕΙΣΑΓΕΤΑΙ ΝΕΟΣ, ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΙΣΑΓΩΓΗ
				if(($tupdates[1] == NULL) && ($allgrades2[$i] <> NULL)){									
					$t = checkNumeric($grmysqli, $allgrades2[$i], $onestudent, $tcourses[1], $lnames[1], true);			
					
					//ΑΝ ΕΓΙΝΕ Η ΕΙΣΑΓΩΓΗ ΤΟΤΕ ΠΡΟΣΘΕΣΕ ΤΟ ΔΕΥΤΕΡΟ ΒΑΘΜΟ ΣΤΟΝ ΠΡΩΤΟ ΚΑΙ ΒΡΕΣ ΜΕΣΟ ΟΡΟ
					if($t == 1){
						if($csem >= IOYNIOS){
							$mo[$i] = round(($mo[$i] + $allgrades2[$i])/2, 1);
						}else{
							$mo[$i] = round(($mo[$i] + $allgrades2[$i])/2, 0);
						}
					
					//ΑΛΛΙΩΣ ΔΙΑΙΡΕΣΕ ΜΟΝΟ ΤΟΝ ΠΡΩΤΟ ΚΑΙ ΒΡΕΣ ΜΕΣΟ ΟΡΟ
					}else{
						if($csem >= IOYNIOS){
							$mo[$i] = round(($mo[$i])/2, 1);
						}else{	
							$mo[$i] = round(($mo[$i])/2, 0);
						}
					}
					
				//ΑΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ Ο ΝΕΟΣ ΕΙΝΑΙ ΔΙΑΦΟΡΕΤΙΚΟΣ ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΝΗΜΕΡΩΣΗ
				}else if($tupdates[1] <> $allgrades2[$i]){
					$t = checkNumeric($grmysqli, $allgrades2[$i], $onestudent, $tcourses[1], $lnames[1], false);				
					
					//ΑΝ ΕΓΙΝΕ Η ΕΝΗΜΕΡΩΣΗ ΤΟΤΕ ΠΡΟΣΘΕΣΕ ΤΟ ΔΕΥΤΕΡΟ ΒΑΘΜΟ ΣΤΟΝ ΠΡΩΤΟ ΚΑΙ ΒΡΕΣ ΜΕΣΟ ΟΡΟ
					if($t == 1){
						if($csem >=IOYNIOS){
							 $mo[$i] = round(($mo[$i] + $allgrades2[$i])/2, 1);
						}else{
							$mo[$i] = round(($mo[$i] + $allgrades2[$i])/2, 0);
						}
						
					//ΑΛΛΙΩΣ ΠΡΟΣΘΕΣΕ ΤΟΝ ΠΡΟΗΓΟΥΜΕΝΟ  ΚΑΙ ΒΡΕΣ ΜΕΣΟ ΟΡΟ
					}else{
						if($csem>=IOYNIOS){
							$mo[$i] = round(($mo[$i] + $tupdates[1])/2, 1);
						}else{
							$mo[$i] = round(($mo[$i] + $tupdates[1])/2, 0);
						}
					}
					
				//ΑΝ Ο ΒΑΘΜΟΣ ΔΕΝ ΑΛΛΑΞΕ ΠΡΟΣΘΕΣΕ ΤΟΝ ΣΤΟΝ ΠΡΩΤΟ ΚΑΙ ΒΡΕΣ ΜΕΣΟ ΟΡΟ
				}else if($tupdates[1] == $allgrades2[$i]){
					if($csem >=IOYNIOS){
						$mo[$i] = round(($mo[$i] + $tupdates[1])/2, 1);
					}else{
						$mo[$i] = round(($mo[$i] + $tupdates[1])/2, 0);

					}
				}
			
		//ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ ΜΟΝΟ
		}else{
			//ΑΝ ΔΕΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ ΤΩΡΑ ΕΙΣΑΓΕΤΑΙ ΝΕΟΣ, ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΙΣΑΓΩΓΗ
			if(($allupdates[$i] == NULL) && ($allgrades[$i] <> NULL)){									
				checkNumeric($grmysqli, $allgrades[$i], $onestudent, $schoolcourseid, $lname, true);			
				
				
			//ΑΝ ΕΙΧΕ ΗΔΗ ΒΑΘΜΟ ΚΑΙ Ο ΝΕΟΣ ΕΙΝΑΙ ΔΙΑΦΟΡΕΤΙΚΟΣ ΤΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ ΚΑΙ ΚΑΝΕ ΕΝΗΜΕΡΩΣΗ
			}else if($allupdates[$i] <> $allgrades[$i]){
				checkNumeric($grmysqli, $allgrades[$i], $onestudent, $schoolcourseid, $lname, false);				
			}
			
		}
		$i++;
	}		
		
	if($plithos > 1){
		/// NA EMFSANISO PINAKA ME MA*ITES, VATHMOUS ΚΑΙ ΜΕΣΟ ΟΡΟ
		printTable($grmysqli,"lock","gradesinput.php", true, $mo, true);
	
	}else{
		/// NA EMFSANISO PINAKA ME MA*ITES KAI VATHMOUS ΧΩΡΙΣ ΜΕΣΟ ΟΡΟ
		printTable($grmysqli,"lock","gradesinput.php", true, null, false);
	}
	die();
}	

printTable($grmysqli, "gradeform","gradesinput.php", false, null, false);




function printTable($grmysqli, $formname, $formaction, $isreport, $mesosoros, $isMixedLesson){
  global $Settings;
  $semester=$Settings->getTerm();

	$schoolcourseid = $_GET['schoolcourse'];
	$teacherid = $_GET['teacher'];
	$schoolid = $_GET['school'];
	
	if(!isset($teacherid) || !isset($schoolcourseid) || !isset($schoolid)){ 
		echo "<span id='infoerror'>Δεν ξέρω τον κωδικό του σχολείου, του καθηγητή ή του μαθήματος!</span><br>";
		echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";
		die();
	}
	
	if($isMixedLesson){
		$tcourses 	= explode("_", $schoolcourseid, 2);
		$tteachers 	= explode("_", $teacherid, 2);
		
		$schoolcourseid = $tcourses[0];
		$teacherid	= $tteachers[0];
	}

		$teacher 	= getTeacher($grmysqli, $teacherid);
		$course 	= getSchoolCourse($grmysqli, $schoolcourseid);
		$lessonname     = getLessonName($grmysqli, $course['lesson_id']);
		$classname 	= getAClass($grmysqli, $course['schoolclass_id']);
		$students 	= getClassStudents($grmysqli, $schoolid,  $course['schoolclass_id']);
		
		$tempcourse	= array( 
					$schoolcourseid, 
					$teacherid,
					$teacher['lastname'] ." ".$teacher['firstname'],
					$classname['id'],
					$classname['classlevel'],
					$lessonname,
					$course['lesson_id']
					);
					
			$allcourse[] = $tempcourse;
		
    /* XXX ych 9/May/2015 - suspecting error
		$lessonset 	= getLessonSet($grmysqli, $lessonname);				
		foreach($lessonset as $ls){
			$temp = getASchoolCourseNoTeacher($grmysqli,  $course['schoolclass_id'], $ls['id']);
			if($temp['id'] <> $schoolcourseid){
				$teacher 	= getTeacher($grmysqli, $temp['teacher_id']);
				$course 	= getSchoolCourse($grmysqli, $temp['id']);
				$classname	= getAClass($grmysqli, $course['schoolclass_id']);
				
				$tempcourse = array( 
					$temp['id'], 
					$temp['teacher_id'],
					$teacher['lastname'] ." ".$teacher['firstname'],					
					$classname['id'],
					$classname['classlevel'],					
					$ls['lessonname'],
					$ls['id']
					);
				
				//ΕΛΕΓΞΕ ΑΝ ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ Η ΘΕΩΡΙΑ ΚΑΙ ΒΑΛΕ ΤΟ ΠΡΩΤΟ
				Bug confirmed:
				Μερικές φορές, όταν η ενιαία φόρμα είναι ενεργοποιημένη,
				οι βαθμοί του εργαστηρίου παιρνιούνται στην θεωρία και το ανάποδα.
				Βγάζοντας τις παρακάτω γραμμές, εμφανίζεται μόνο το Ε ή μόνο η Θ και φαίνεται να μην έχει πρόβλημα.

				if(strpos($ls['lessonname'], "-(Ε)") === false){
					array_unshift($allcourse, $tempcourse);
				}else{
					$allcourse[] = $tempcourse;
				}
			}			
		}			
   */
		
		echo "<FORM autocomplete=\"off\" method='GET' id='".$formname."' action='".$formaction."'>";
		echo "<span id='infoclass'>Τμήμα: (" . $course['schoolclass_id']. ") " .$allcourse[0][4] ."</span>";
		if(count($allcourse) >1){
			if(strcmp($allcourse[0][2], $allcourse[1][2])==0){
				echo "<br><span id='infoteacher'>Καθηγητής: ". $allcourse[0][2] ." </span>";
			}else{
				echo "<br><span id='infoteacher'>Καθηγητής 1: ". $allcourse[0][2] ." </span>";
				echo "<br><span id='infoteacher'>Καθηγητής 2: ". $allcourse[1][2] ." </span>";
			}
		
			$temp = explode("-(", $allcourse[0][5], -1);
			echo "<br><span id='infolesson'>Μάθημα: ". $temp[0] ."</span>";	
		}else{
			echo "<br><span id='infoteacher'>Καθηγητής: ". $allcourse[0][2] ." </span>";
			echo "<br><span id='infolesson'>Μάθημα: ". $allcourse[0][5] ."</span>";
		}
		
    global $Settings;
    echo "<br>Τετράμηνο :" . $Settings->getCurrentTermText();
		echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";
			
		if(!$isreport){			
			echo "<INPUT TYPE='submit' NAME='savegrades' VALUE='Αποθήκευση'/>";
		}else{
			echo "<input type='button' onclick='window.print();' name='print' value='Εκτύπωση'/><br>";
		}
		
				
		echo "<TABLE id='datatable'>";
		
		echo 	"<TR align='center'>
				<TD>Α/Α</TD>
				<TD>ΜΗΤΡΩΟ</TD>
				<TD>ΕΠΩΝΥΜΟ</TD>
				<TD>ΟΝΟΜΑ</TD>
				<TD>ΠΑΤΡΩΝΥΜΟ</TD>
				<TD>ΜΗΤΡΩΝΥΜΟ</td>";
				
				if(count($allcourse) >1){
					echo "<TD><b>ΘΕΩΡΙΑ</b> ".$allcourse[0][2]."</TD>";
					echo "<TD><b>ΕΡΓΑΣΤΗΡΙΟ</b> ".$allcourse[1][2]."</TD>";
					if($isreport){
						echo "<TD>ΜΕΣΟΣ ΟΡΟΣ</TD>";
					}
				}else{
					echo "<TD>ΒΑΘΜΟΣ</TD>";
					printOtherSemesterTitles($semester);
				}		
				echo "</tr>";

		if(count($allcourse)>1){
			$status = array(getSchoolcourseLockStatus2($grmysqli, $allcourse[0][0] ),
							getSchoolcourseLockStatus2($grmysqli, $allcourse[1][0] ));
		}else{				
			$status = array(getSchoolcourseLockStatus2($grmysqli, $allcourse[0][0]));
		}
				
		$i = 1;
		/*
		   Για κάθε μαθητή τυπώνουμε μία γραμμή 
		   */
		foreach($students as $onestudent){
			if(count($allcourse)>1){
				$exist = array(	gradeExists($grmysqli, $onestudent['id'], $allcourse[0][0]),
								gradeExists($grmysqli, $onestudent['id'], $allcourse[1][0]));
								
				//ΒΡΕΣ ΒΑΘΜΟΥΣ ΑΛΛΩΝ ΠΕΡΙΟΔΩΝ
			//	$othergrades = array(findRestPeriodsGrades($grmysqli, $onestudent['id'], $allcourse[0][0], $semester),
			//						 findRestPeriodsGrades($grmysqli, $onestudent['id'], $allcourse[1][0], $semester));
			}else{
				$exist = array(	gradeExists($grmysqli, $onestudent['id'], $allcourse[0][0]));
				//ΒΡΕΣ ΒΑΘΜΟΥΣ ΑΛΛΩΝ ΠΕΡΙΟΔΩΝ
				$othergrades = findRestPeriodsGrades($grmysqli, $onestudent['id'], $allcourse[0][0], $semester);
			}			
			 
			echo "<TR";
			if(($i%2)==1){ echo(" class=\"alt\""); } 					
			echo "><TD align='center'>". $i ."</TD>";
			
			echo "<TD align='center'>";
			echo $onestudent['student_am'];
			echo "<INPUT TYPE='hidden' name='count' 				value='". count($allcourse)."'>";
			echo "<INPUT TYPE='hidden' name='school' 				value='". $schoolid."'>";
			echo "<INPUT TYPE='hidden' name='allstudents[]' 		value='". $onestudent['id'] ."'>";
				if(count($allcourse)>1){					
					echo "<INPUT TYPE='hidden' name='schoolcourse' 		value='". $allcourse[0][0]		."_".$allcourse[1][0]."'/>";
					echo "<INPUT TYPE='hidden' name='teacher' 			value='". $allcourse[0][1]		."_".$allcourse[1][1]."'/>";
					echo "<INPUT TYPE='hidden' name='lname' 			value='". $allcourse[0][6]		."_".$allcourse[1][6]."'>";
					echo "<INPUT TYPE='hidden' name='allupdates[]' 		value='". $exist[0]['grade'] 	."_".$exist[1]['grade'] ."'>";				
					
					//check if logged in teacher, teaches both lessons --- ΠΡΟΑΙΡΕΤΙΚΟ
					//$temp = getSchoolCourse($grmysqli, $$allcourse[0][0]);
					
				}else{
					echo "<INPUT TYPE='hidden' name='schoolcourse' 		value='". $allcourse[0][0] ."'/>";
					echo "<INPUT TYPE='hidden' name='teacher' 			value='". $allcourse[0][1]  ."'/>";
					echo "<INPUT TYPE='hidden' name='lname' 			value='". $allcourse[0][6]."'>";
					echo "<INPUT TYPE='hidden' name='allupdates[]' 		value='". $exist[0]['grade'] ."'>";					
				}
			echo "</TD>";
			
			echo "<TD>" . $onestudent['lastname'] 	."</TD>";
			echo "<TD>" . $onestudent['firstname'] 	."</TD>";
			echo "<TD>" . $onestudent['fathername'] ."</TD>";
			echo "<td>" . $onestudent['mothername'] ."</td>";

			// ΑΝ ΓΙΝΕΤΑΙ ΑΝΑΦΟΡΑ ΒΑΘΜΩΝ ΚΑΙ ΟΧΙ ΚΑΤΑΧΩΡΗΣΗ
			if($isreport){
				if(count($allcourse)>1){
					//show both values theory and lab
					echo 	"<td align='center'>". $exist[0]['grade']."</td>";
					echo 	"<td align='center'>". $exist[1]['grade']."</td>";
					if($mesosoros[$i-1] <> null){
						echo 	"<td align='center'>".$mesosoros[$i-1]	."</td>";
					}else{
						echo 	"<td align='center'></td>";					
					}
					echo	"</tr>";
				}else{
					echo 	"<td align='center'>". $exist[0]['grade']."</td>";
					
					printOtherSemesterGrades( $semester, $othergrades);
					printOtherSemesterAvg ($semester, $othergrades, $exist[0]['grade']);
					
					echo	"</tr>";
				}
				
			//ΑΝ ΓΙΝΕΤΑΙ ΕΙΣΑΓΩΓΗ ΒΑΘΜΩΝ
			}else{
				//ΑΝ ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ ΘΕΩΡΙΑ-ΕΡΓΑΣΤΗΡΙΟ
				if(count($allcourse)>1){
					
					echo 	"<TD>
							<INPUT TYPE='text' maxlength='5' size='5' name='allgrades[]' class='allgrades' id='allgrades'";
					
							if($exist[0]['grade'] >= 0){
								echo " VALUE='". $exist[0]['grade'] ."'";
							}
							if($status[0]==1){
								echo " disabled";
							}
					echo 	"></TD>";
					echo 	"<TD>
							<INPUT TYPE='text' maxlength='5' size='5' name='allgrades2[]' class='allgrades' id='allgrades'";
					
							if($exist[1]['grade'] >= 0){
								echo " VALUE='".  $exist[1]['grade'] ."'";				
							}
							if($status[1]==1){
								echo " disabled";
							}
					echo 	"></TD>
							</TR>";						
				
				//ΑΝ ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ ΜΟΝΟ	
				}else{
					echo 	"<TD>
							<INPUT TYPE='text' maxlength='5' size='5' name='allgrades[]' class='allgrades' id='allgrades'";
					
							if($exist[0]['grade'] >= 0){
								echo " VALUE='". $exist[0]['grade'] ."'";				
							}
							if($status[0]==1){
								echo " disabled";
							}
					echo 	"></TD>";

					printOtherSemesterGrades( $semester, $othergrades);
					printOtherSemesterAvg ($semester, $othergrades, $exist[0]['grade']);
					
					echo	"</tr>";			
				}
			}
			$i++;
		}
			
		echo "</TABLE>";
		echo "<INPUT TYPE='hidden' name='semester'  value='". $semester."'>";

		echo "<br><input type='button' onclick='window.history.back();' name='back' value='Πίσω'/>";				
		if(!$isreport){			
			echo "<INPUT TYPE='submit' NAME='savegrades' VALUE='Αποθήκευση'/>";
		}else{
			echo "<input type='button' onclick='window.print();' name='print' value='Εκτύπωση'/>";
		}
	echo "</FORM>";
}
	echo "</div>
	<div id='bottom'></div>
	</div>";
?>
</body>
</html>

<?php 
function semesterName($semester){
	
	if($semester == A_TETRAMINO){
		return "Α' Τ.μηνο";
	}else if($semester == B_TETRAMINO){
		return "Β' Τ.μηνο";
	}else if($semester == G_TETRAMINO){
		return "Γ' Τ.μηνο";
	}else if($semester == IOYNIOS){
		return "Εξ. Ιουνίου";
	}else if($semester == SEPTEMVRIOS){
		return "Εξ. Σεπτεμβρίου";
	}else{
		return null;
	}
	
}

function printOtherSemesterTitles($currsemester){
	$all = array(A_TETRAMINO,B_TETRAMINO,IOYNIOS,SEPTEMVRIOS);
	$curr = array($currsemester);
	
	$othersemesters = array_diff($all , $curr);
	foreach($othersemesters as $sem){
		echo "<td  align='center'>". semesterName($sem) ."</td>";
	}	
	echo "<td  align='center'>Μ.Ο. Προφορικών</td>";
	echo "<td  align='center'>Γενικός Μ.Ο.</td>";
}

function printOtherSemesterGrades($currsemester, $othergrades){
	$all = array(A_TETRAMINO,B_TETRAMINO,IOYNIOS,SEPTEMVRIOS);
	$curr = array($currsemester);
	
	$othersemesters = array_diff($all , $curr);
	foreach($othersemesters as $sem){
		echo "<td  align='center'>". $othergrades[$sem] ."</td>";
	}	
}
function printOtherSemesterAvg($currsemester, $othergrades, $newgrade){
	if ($currsemester==1) {
		echo "<td  align='center'> - </td>";
		echo "<td  align='center'> - </td>";

	} elseif ($currsemester==2) {

		$moProforikwn = ($newgrade + $othergrades[1])/2;
		echo "<td  align='center'>". $moProforikwn ."</td>";
		echo "<td  align='center'> - </td>";

	} elseif ($currsemester==IOYNIOS || $currsemester==SEPTEMVRIOS) {

		$moProforikwn = ($othergrades[2] + $othergrades[1])/2;
		$telikosMo = round ( ($moProforikwn + $newgrade)/2, 1);

		echo "<td  align='center'>". $moProforikwn ."</td>";
		echo "<td  align='center'>". $telikosMo ."</td>";
	}
}

function findRestPeriodsGrades($grmysqli, $studentid, $schcourseid, $currsemester){
	$all = array(A_TETRAMINO,B_TETRAMINO,IOYNIOS,SEPTEMVRIOS);
	$curr = array($currsemester);	
	$othergrades = null;
	
	$othersemesters = array_diff($all , $curr);
	
	foreach($othersemesters as $sem){		
		$grade = getAStudentLessonGrade( $grmysqli,$studentid, $schcourseid, $sem);
		$othergrades[$sem] = $grade['grade'];
	}
	

	/*
	echo "<br>";
	echo "For student $studentid course $schcourseid I got :";
	print_r ($othergrades);
	echo "<br>";
	*/
	return $othergrades;
}

//ΕΛΕΓΧΕΙ ΑΝ ΥΠΑΡΧΕΙ ΚΟΜΜΑ Ή ΤΕΛΕΙΑ ΓΙΑ ΤΑ ΔΕΚΑΔΙΚΑ ΝΟΥΜΕΡΑ
//ΔΕΚΤΕΣ ΜΟΡΦΕΣ 1.2 ΚΑΙ 1,2
function isGrade($g){
	$regex = "~^[0-9]+([\.,][0-9]+)?$~xD";

	if(preg_match($regex, $g)){
		return true;
	}else{
		return false;
	}
}


//ΔΕΝ ΕΙΝΑΙ ΟΛΟΚΛΗΡΩΜΕΝΗ
function formatFloat($num){
	$t = preg_replace('/./', ',', $num ,1);

	return $t;
}

function gradeExists($mysql, $stid, $schcourseid){
  global $Settings;
  $semester=$Settings->getTerm();

	$gr = getAStudentLessonGrade( $mysql, $stid, $schcourseid,$semester);
	
	if(sizeof($gr) > 0) {				
		return $gr;
	}else{
		return NULL;
	}
}


function checkNumeric($grmysqli, $grade, $studentid, $courseid, $lessonname, $doInsert=true){
  global $Settings;
  $semester=$Settings->getTerm();

	$t = 0;
	

	$student = getStudentById ($grmysqli, $studentid);
	//ΕΙΝΑΙ ΑΡΙΘΜΟΣ?????
	if(isGrade( $grade )){
		
		$grade = preg_replace('/,/','.', $grade); 					


		///ΑΝ ΕΙΣΑΓΕΙΣ ΑΠΟΥΣΙΕΣ ΜΗΝ ΕΛΕΓΞΕΙΣ ΟΡΙΑ 0-20
		if(strcmp($lessonname, 'ΔΙΚΑΙΟΛΟΓΗΜΕΝΕΣ ΑΠΟΥΣΙΕΣ')==0 || strcmp($lessonname, 'ΣΥΝΟΛΟ ΑΠΟΥΣΙΩΝ')==0){										
			$t = storeGrade($grmysqli, $courseid, $studentid, $grade, $semester, 0, $doInsert);									
		}else{
		
			//ΕΙΣΑΓΩΓΗ ΒΑΘΜΩΝ ΟΠΟΤΕ ΕΛΕΓΞΕ ΑΝ ΕΙΝΑΙ ΣΤΑ ΟΡΙΑ 0-20
			if( $grade >=0 && $grade <=20 ){
				if($semester==A_TETRAMINO || $semester==B_TETRAMINO){
					if(preg_match('/^\d+$/',$grade)){
//					if(is_numeric($grade)){
						$t = storeGrade($grmysqli, $courseid, $studentid, $grade, $semester, 0, $doInsert);
					}else{
						echo "<br><span id='infoerror'>" . $student['lastname'] ." ". $student['firstname']. ": Ο βαθμός " . $grade ." είναι πραγματικός και όχι ακέραιος</span>";
					}
        } elseif ($semester==IOYNIOS || $semester==SEPTEMVRIOS) {
					$t = storeGrade($grmysqli, $courseid, $studentid, $grade, $semester, 0, $doInsert);
				}
			//ΔΕΝ ΕΙΝΑΙ ΕΝΤΟΣ ΟΡΙΩΝ --- ΣΦΑΛΜΑ
			}else{
				echo "<br><span id='infoerror'>" . $student['lastname'] ." ". $student['firstname']. ": Ο βαθμός ".$grade." είναι εκτός ορίων</span>";						
			}
		}	
	//ΔΕΝ ΕΙΝΑΙ ΑΡΙΘΜΟΣ --- ΣΦΑΛΜΑ
	}else{
		echo "<br><span id='infoerror'>" .$student['lastname'] ." ". $student['firstname']. ": Ο " . $grade ." δεν είναι αριθμός</span>";		
		
	}	
		
	return $t;
}


//ΑΠΟΘΗΚΕΥΕΙ 'Η ΕΝΗΜΕΡΩΝΕΙ ΕΝΑ ΒΑΘΜΟ ΑΝΑ ΜΑΘΗΤΗ ΑΝΑ ΜΑΘΗΜΑ ΕΝΟΣ ΤΕΤΡΑΜΗΝΟΥ
function storeGrade($grmysqli, $courseid, $studentid, $grade, $semester, $doLock, $doInsert){
	$t = 0;

	$student = getStudentById ($grmysqli, $studentid);	

	/* Ξεκίνα με update και αν δεν ενημερώσεις κανένα τότε προχώρα σε insert */
	$data = array($grade , $doLock, $studentid, $courseid, $semester);

	if(updateGrade($grmysqli, $data) > 0)
	{
		echo  "<br><span id='infook'>" . $student['lastname'] ." ". $student['firstname']. " άλλαξε ο βαθμός σε " . $grade ."</span>";
		$t = 1;		
	}
	else
	{
		/* η update είτε απέτυχε είτε δεν βρήκε κανένα να κάνει update. 
		   Κάνε τώρα insert και αν πετύχει καλώς, αλλιώς δήλωσε αποτυχία. */
		$data = array( $studentid, $courseid , $semester, $grade , $doLock);
						
		$result = insertGrade($grmysqli, $data);
		if($result > 0)
		{
			echo  "<br><span id='infook'>" . $student['lastname'] ." ". $student['firstname']. " καταχωρήθηκε ο βαθμός " . $grade . "</span>";
			$t = 1;
		}else{
			/* Δες το λάθος αν είναι το 23000 οπότε πρόκειτε για Duplicate entry και αγνόησέ το */
			$mysqlErrorCode = mysqli_errno( $grmysqli );
			//echo "mysqlerror=$mysqlErrorCode";
			if ($mysqlErrorCode != 1062) /* duplicate entry : δηλαδή πήγαμε να εισάγουμε κάτι που υπήρχε το ίδιο */
							/* http://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html */
			{
				echo  "<br><span id='infoerror'>" . $student['lastname'] ." ". $student['firstname']. " σφάλμα κατά την καταχώρηση του βαθμού του</span>";
			}
		}		
	}

	return $t;
}

?>
