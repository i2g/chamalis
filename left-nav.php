<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once("models/standard_roles.php");
require_once("models/db-queries.php");

if (!securePage($_SERVER['PHP_SELF'])){die();}

//Links for logged in user
if(isUserLoggedIn()) {
	echo " <ul>
	<li><a href='account.php'>Λογαριασμός</a></li>
  <li><a href='user_settings.php'>Ρυθμίσεις χρήστη</a></li>
	<li><a href='logout.php'>Αποσύνδεση</a></li>
  ";
	
  if (userCake_checkStringPermissionForUserId($loggedInUser->user_id, MYSCHOOLOP)) 
  {
		echo "<br>
    <li><b>MySchool</b></li>
    <style>settings {color: #0000FF}</style>
    <style>inputs {color: #00FF00}</style>
    <style>dangers {color: #FF0000}</style>
    <li><a href='settings.php'>Διάφορες Ρυθμίσεις</a></li>
    <li><i>Εισαγωγή</i> </li>
	  <li><a href='uploadexcel.php'>Ανέβασμα Excel</a></li>
		<li><a href='select_course.php'>Εισ. Βαθ./Απ.</a></li>	
    <li><i>Λήψη</i> </li>
		<li><a href='select_grades_lock.php'>Κλείδωμα</a></li>
		<li><a href='select_school_grades_dd.php'>Λήψη βαθμών</a></li>
		<li><a href='select_school_grades_all.php'>Λήψη βαθμών (Εν.)</a></li>	
		<li><a href='select_apousies_dd.php'>Λήψη απουσιών</a></li>
		<li></li>
    <li><i>Αλλα</i> </li>
    <li><a href='kartelakia.php'>Καρτελάκια</a></li>
		<li>-------</li>
    <li><i>Επικίνδυνα</i> </li>
		<li><a href='empty_grades.php'>Καθάρισμα βαθμών</a></li>
		<li><a href='empty_classes.php'>Καθάρισμα τάξεων</a></li>
		<li><a href='empty_entire_db.php'>Καθάρισμα σχολείου</a></li>
	";
  }

  if (userCake_checkStringPermissionForUserId($loggedInUser->user_id, ADMIN)) 
  {
    echo "<br>
    <li><b>Χαμάλης</b> </li>
  <li><a href='admin_configuration.php'>Ρυθμίσεις Διαχειριστή</a></li>
  <li><a href='admin_users.php'>Διαχειριστές</a></li>
  <li><a href='admin_permissions.php'>Δικαιώματα Διαχειριστών</a></li>
  <li><a href='admin_pages.php'>Σελίδες διαχειριστών</a></li>
    <li><a href='su_kartelakia.php'>Καρτελάκια</a></li>
		<li>-------</li>
    <li><i>Επικίνδυνα</i> </li>
		<li><a href='su_empty_entire_db.php'>Καθάρισμα σχολείου</a></li>
  ";
  }
  echo "</ul>";
} 
//Links for users not logged in
else {
	//Removed: Registration is being done only by admins
	// <li><a href='register.php'>Εγγραφή</a></li>
	echo "
	<ul>
	<li><a href='index.php'>Σπίτι</a></li>
	<li><a href='login.php'>Είσοδος</a></li>
	<li><a href='forgot-password.php'>Ξέχασα τον κωδικό μου!</a></li>
	";
	/*
	if ($emailActivation)
	{
	echo "<li><a href='resend-activation.php'>Ξαναστείλε μου, σε παρακαλώ, το η-μήνυμα για την ενεργοποίηση του λογαριασμού μου</a></li>";
	}
	*/
	echo "</ul>";
}

?>
