<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

require_once("db-settings.php"); //Require DB connection
require_once("db-queries.php");

/*
  chamalis globals
 */
define ('A_TETRAMINO', 1);
define ('B_TETRAMINO', 2);
define ('G_TETRAMINO', 3);
define ('IOYNIOS', 10);
define ('SEPTEMVRIOS', 11);

class GlobalSettings 
{
  private $isInitialized = false;
  public $currentTerm=A_TETRAMINO; 
  private $debug=TRUE;    
  private $synolo_apousiwn="'%ΣΥΝΟΛΟ ΑΠΟΥΣΙΩΝ%'";
  private $comment="you should not see this";

	public $title_AA = 'Α/Α';
	public $title_AM = 'Αρ. μητρώου';
	public $title_EPONIMO = 'Επώνυμο μαθητή';
	public $title_ONOMA = 'Όνομα μαθητή';
	public $title_FATHER = 'Όνομα πατέρα';
	public $title_MOTHER = 'Όνομα μητέρας';
	public $title_MARK = 'Αριθμητικώς';
	public $title_OLOGRAFOS = 'Ολογράφως';

  /* Getters/Setters */
  public function setDebug( $newDebug ) {
    if (strcmp($newDebug, '0')==0 || !$newDebug) $this->debug = false;
    elseif (strcmp($newDebug, '1')==0 || $newDebug==1) $this->debug = true;
    else echo "invalid debug value at settings ($newDebug)";

  }
  public function getDebug( ) {
    return $this->debug;
  }
  public function setTerm( $newTerm ) { 
    if (($newTerm!=A_TETRAMINO) && ($newTerm!=B_TETRAMINO)
      && ($newTerm!=IOYNIOS) && ($newTerm!=SEPTEMVRIOS)) {
      echo "Trying to set invalid term";
      exit(1);
    }
    $this->currentTerm = $newTerm; 
  }
  public function getTerm( ) {
    return $this->currentTerm; 
  }
  public function getCurrentTermText() {
    if ($this->currentTerm==A_TETRAMINO) return "Α Τετράμηνο";
    if ($this->currentTerm==B_TETRAMINO) return "Β Τετράμηνο";
    if ($this->currentTerm==IOYNIOS) return "Εξετάσεις Ιουνίου";
    if ($this->currentTerm==SEPTEMVRIOS) return "Εξετάσεις Σεπτ";

    echo "invalid term detected";
  }
  public function setComment($comment) {
    $this->comment = trim( $comment );
    if (strcmp( $this->comment, "")==0) $this->comment = "default comment";
  }
  public function getComment() {
    return $this->comment;
  }
  public function setSynoloApousiwn( $synolo_apousiwn ) {
    $this->synolo_apousiwn = trim( $synolo_apousiwn );
    if (strcmp( $this->synolo_apousiwn, "")==0) $this->synolo_apousiwn = "'%ΣΥΝΟΛΟ ΑΠΟΥΣΙΩΝ%'";
  }
  public function getSynoloApousiwn() {
    return $this->synolo_apousiwn;
  }

  /* Store and Loaf */
  public function loadGlobalSettings()
  {
    global $grmysqli;
    $xml_string = loadChamalisSettings($grmysqli, "basic");
    if (!isset($xml_string) || $xml_string==null) {
      echo "Failed to load settings from database, loading defaults.";
      echo "-- >". htmlspecialchars($xml_string) . "< ---";
      $this->insertDefaultSettings();
    }
    else {
      //go ahead and parse the xml
      $xml = new SimpleXMLElement($xml_string);

      foreach ($xml->children() as $kid)
      {
        if (strcmp($kid->getName(), "debug")==0) {
          $this->setDebug( $kid );
          continue;
        }
        if (strcmp($kid->getName(), "current_term")==0) {
          $this->setTerm( $kid );
          continue;
        }
        if (strcmp($kid->getName(), "synolo_apousiwn")==0) {
          $this->setSynoloApousiwn( $kid );
          continue;
        }
        if (strcmp($kid->getName(), "comment")==0) {
          $this->setComment( $kid );
          continue;
        }

        if (strcmp($kid->getName(), "title_AA")==0) {
          $this->title_AA = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_AM")==0) {
          $this->title_AM = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_EPONIMO")==0) {
          $this->title_EPONIMO = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_ONOMA")==0) {
          $this->title_ONOMA = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_FATHER")==0) {
          $this->title_FATHER = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_MOTHER")==0) {
          $this->title_MOTHER = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_MARK")==0) {
          $this->title_MARK = $kid;
          continue;
        }
        if (strcmp($kid->getName(), "title_OLOGRAFOS")==0) {
          $this->title_OLOGRAFOS = $kid;
          continue;
        }
        echo "Unhandled attribude at a settings : $kid";
      }
    }
  }
  public function storeGlobalSettings( $doInsert=FALSE )
  {
    global $grmysqli;

    $d = $this->getDebug();
    $ct = $this->getTerm();
    $sa = $this->getSynoloApousiwn();
    $c = $this->getComment();
    $taa = $this->title_AA;
    $tam = $this->title_AM;
    $tep = $this->title_EPONIMO;
    $ton = $this->title_ONOMA;
    $tfa = $this->title_FATHER;
    $tmo = $this->title_MOTHER;
    $tma = $this->title_MARK;
    $tol = $this->title_OLOGRAFOS;

    $xml_string="<settings>
          <debug>$d</debug>
          <current_term>$ct</current_term>
          <synolo_apousiwn>$sa</synolo_apousiwn>
          <comment>$c</comment>
          <title_AA>$taa</title_AA>
          <title_AM>$tam</title_AM>
          <title_EPONIMO>$tep</title_EPONIMO>
          <title_ONOMA>$ton</title_ONOMA>
          <title_FATHER>$tfa</title_FATHER>
          <title_MOTHER>$tmo</title_MOTHER>
          <title_MARK>$tma</title_MARK>
          <title_OLOGRAFOS>$tol</title_OLOGRAFOS>
      </settings>";
    echo "<code>";
    echo "XML to store starts here:<br>";
    echo htmlspecialchars($xml_string);
    echo "</code>";
    if ($doInsert) {
      insertChamalisSettings($grmysqli, "basic", $xml_string);
      $this->loadGlobalSettings();//reload
    } else {
      storeChamalisSettings($grmysqli, "basic", $xml_string);
    }

  }

  /*
    private functions 
   */
  private function loadDefaultGlobalSettings()
  {
    $this->isInitialized = false;
    $this->currentTerm=A_TETRAMINO; 
    $this->debug=TRUE;    
    $this->synolo_apousiwn="%ΣΥΝΟΛΟ ΑΠΟΥΣΙΩΝ%";
    $this->comment="default comment";
    $this->title_AA = 'Α/Α';
    $this->title_AM = 'Αρ. μητρώου';
    $this->title_EPONIMO = 'Επώνυμο μαθητή';
    $this->title_ONOMA = 'Όνομα μαθητή';
    $this->title_FATHER = 'Όνομα πατέρα';
    $this->title_MOTHER = 'Όνομα μητέρας';
    $this->title_MARK = 'Αριθμητικώς';
    $this->title_OLOGRAFOS = 'Ολογράφως';
  }
  private function insertDefaultSettings()
  {
    global $grmysqli;
    $this->loadDefaultGlobalSettings();
    $this->storeGlobalSettings( TRUE );
  }
}

global $Settings;
$Settings = new GlobalSettings();
$Settings->loadGlobalSettings();

//Retrieve settings
$stmt = $mysqli->prepare("SELECT id, name, value
	FROM ".$db_table_prefix."configuration");	
$stmt->execute();
$stmt->bind_result($id, $name, $value);

while ($stmt->fetch()){
	$settings[$name] = array('id' => $id, 'name' => $name, 'value' => $value);
}
$stmt->close();

//Set Settings
$emailActivation = $settings['activation']['value'];
$mail_templates_dir = "models/mail-templates/";
$websiteName = $settings['website_name']['value'];
$websiteUrl = $settings['website_url']['value'];
$emailAddress = $settings['email']['value'];
$resend_activation_threshold = $settings['resend_activation_threshold']['value'];
date_default_timezone_set('UTC'); //added by ych, because php issues a warning otherwise. (Sep/2015)
$emailDate = date('dmy');
$language = $settings['language']['value'];
$template = $settings['template']['value'];

$master_account = -1;

$default_hooks = array("#WEBSITENAME#","#WEBSITEURL#","#DATE#");
$default_replace = array($websiteName,$websiteUrl,$emailDate);

if (!file_exists($language)) {
	$language = "models/languages/en.php";
}

if(!isset($language)) $language = "models/languages/en.php";

require_once($language);
require_once("class.mail.php");
require_once("class.user.php");
require_once("class.newuser.php");
require_once("funcs.php");

session_start();

//Global User Object Var
//loggedInUser can be used globally if constructed
if(isset($_SESSION["userCakeUser"]) && is_object($_SESSION["userCakeUser"]))
{
	$loggedInUser = $_SESSION["userCakeUser"];
}

?>
