<?php
//$DEBUG_db_queries=FALSE;

if (!function_exists('loadChamalisSettings')) {
  //ych 9/2016 : if it exists it causes "Cannot redeclare function" problems



  function loadChamalisSettings($grmysqli)
  {
    $sql = "select settings from SETTINGS where label='basic';";
    $result = mysqli_query($grmysqli, $sql);
    if (!isset($result) || $result==null) {
      return null;
    }

    $row = mysqli_fetch_array($result);
    if ($row==null || sizeof($row)==0) return null;

    if (!isset($row['settings'])) return null;

    return $row['settings'];
  }
  function insertChamalisSettings($grmysqli, $label, $settings)
  {
    $sql = "insert into SETTINGS values (NULL,?,?);";
    $data = array( $label, $settings );
    $sql = preparedQuery($grmysqli, $sql, $data);
    if (!mysqli_query($grmysqli, $sql))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo $sql;
      exit(1);
    }
  }
  function storeChamalisSettings($grmysqli, $label, $settings)
  {
    $sql = "update SETTINGS set settings=? where label=?;";
    $data = array( $settings, $label );
    $sql = preparedQuery($grmysqli, $sql, $data);
    if (!mysqli_query($grmysqli, $sql))  {
      echo "Could not update, storing new<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli) . "<br>";
      echo htmlspecialchars($sql);
      echo "<br>";
      echo "<hr>";
      exit(1);
    }

    $arose = mysqli_affected_rows($grmysqli);
    if (sizeof($arose)==0) {
      echo "Failed to update settings";
      exit(1);
    }


  }


  function userCake_checkStringPermissionForUserId($userId, $stringPermission)
  {
    global $mysqli,$db_table_prefix;
    $sql = "select * from uc_user_permission_matches join uc_permissions  
      on uc_user_permission_matches.permission_id=uc_permissions.id  
      where user_id=$userId and name=\"$stringPermission\";";
    //echo "<p>$sql</p>";
    $result = mysqli_query($mysqli, $sql);
    if (!isset($result) || $result==FALSE)  {
      return false;
    }

    //echo $result;

    $row = mysqli_fetch_array($result);
    //echo sizeof($row);
    if (!isset($row) || $row==null) {
      return false;
    }

    return true;
  }

  function updateSchoolcourseLockStatus($grmysqli, $status, $schoolcourseid, $semester=null){
    global $Settings;
    if ($semester==null) $semester = $Settings->currentTerm;
    $data = array($status, $schoolcourseid, $semester);
    $sql = "UPDATE GRADES SET locked=? WHERE schoolcourse_id=? AND term=?;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    //echo $sql;

    if (!mysqli_query($grmysqli, $sql))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_affected_rows($grmysqli);

  }


  function getSchoolcourseLockStatus($grmysqli, $lessonid, $schoolclassid, $semester=10){
    $grades = getClassGrades($grmysqli, $schoolclassid);

    $found = 0;
    $i = 0;
    $status = -1;
    while($found<>2 && $i<sizeof($grades)){		
      $grade = $grades[$i];

      if(($grade['lessonid']==$lessonid) && ($grade['term']==$semester)){
        $found = 1;

        if($status == -1){
          $status = 0;
        }

        $status = $status + $grade['locked'];
      }
    /*else if(($grade['lessonid']<>$lessonid) && ($grade['term']==$semester) && ($found==1)){
      $found = 2;
    }*/

      $i++;
    }

    if($status > 0){
      return 1;
    }else{
      return $status;
    }
  }


  function getSchoolcourseLockStatus2($grmysqli, $schoolcourseid, $semester=null){
    global $Settings;
    if ($semester==null) $semester = $Settings->getTerm();
    $data = array($schoolcourseid, $semester);
    $sql = "SELECT * FROM GRADES WHERE schoolcourse_id=? AND term=?";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    $count = 0;
    $i = 0;
    while($row = mysqli_fetch_array($result)) {		
      $i = $i + $row['locked'];
      $count++;		
    }

    if($i >0){
      if($count == $i){
        return 1;
      }else{
        return 0;
      }
    }else{
      if($count <> $i){
        return 0;
      }else{
        return -1;
      }
    }		
  }


  function getClassbyClassnumber($grmysqli, $classnumber){	
    $sql = "SELECT SCL.*, SCR.*, TOF.* " .
      " FROM SCHOOLCLASSES AS SCL, SCHOOLCOURSES AS SCR, TEACHERSOFSCHOOLCOURSES AS TOF " .
      " WHERE SCL.id=? AND SCL.id=SCR.schoolclass_id AND SCR.id=TOF.schoolcourse_id;";
    echo $sql;

    $sql = preparedQuery($grmysqli, $sql, $classnumber);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {		
      $lesson['sclid'] = $row['SCL.id'];
      $lesson['scrid'] = $row['SCR.id'];
      $lesson['scrteacher'] = $row['TOF.teacher_id'];
      $lesson['scrlesson'] = $row['SCR.lesson_id'];

      $array[] = $lesson;
    }

    return (array) $array;
  }

  function insertUser($grmysqli,$data)
  {
    $sql = "INSERT INTO USERS (username, password, isadmin, school_id) VALUES (?, ?, ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }


  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchoolclass($grmysqli,$data)
  {
    $sql = "INSERT INTO SCHOOLCLASSES (classlevel, classnumber, school_id, startyear) VALUES (?, ?, ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchoolclassregistration($grmysqli, $data)
  {
    $sql = "INSERT INTO SCHOOLCLASSREGISTRATIONS (student_id, schoolclass_id) VALUES ( ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchoolcourse($grmysqli,$data )
  {
    $sql = "INSERT INTO SCHOOLCOURSES (schoolclass_id, lesson_id) VALUES (?,?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<hr>";
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<hr>";
      echo "$sql";
      echo "<hr>";

      return false;
    }	

    return mysqli_insert_id($grmysqli);
  }

  function insertTeacherOfSchoolcourse($grmysqli, $data)
  {
    $sql = "INSERT INTO TEACHERSOFSCHOOLCOURSES (teacher_id, schoolcourse_id) " .
      " VALUES (?,?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<hr>";
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<hr>";
      echo "$sql";
      echo "<hr>";

      return false;
    }	

    return mysqli_insert_id($grmysqli);
  }

  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchoolregistration($grmysqli, $data )
  {
    $sql = "INSERT INTO SCHOOLREGISTRATIONS (school_id, startyear, student_id, student_am) VALUES (?,?,?,?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo '<br>';
      echo '<br>';
      foreach ( $data as $value ) {
        echo "$value";
        echo '<br>';
      }
      echo '<br>';

      return false;
    }	

    return mysqli_insert_id($grmysqli);
  }

  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchoollesson($grmysqli,$lessonname )
  {
    $sql = "INSERT INTO SCHOOLLESSONS (lessonname) VALUES (?);";
    $sql = preparedQuery($grmysqli, $sql, $lessonname);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertSchool($grmysqli, $data )
  {
    $sql = "INSERT INTO SCHOOLS (id, schoolname) VALUES (? , ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return $data[0]; //We do not use AUTO_INCREMENT so mysqli_insert_id returns 0
    //return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertStudent($grmysqli, $data )
  {
    $sql = "INSERT INTO STUDENTS (lastname, firstname, fathername, mothername, birthdate) VALUES (?, ?, ?, ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli) . ' - ' . mysqli_errno($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertTeacher($grmysqli,$data )
  {
    $sql = "INSERT INTO TEACHERS (lastname, firstname, fathername, mothername, username, isadmin) VALUES (?, ?, ?, ?, ? ,?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertYear($grmysqli, $year )
  {
    $sql = "INSERT INTO SCHOOLYEARS (startyear) VALUES (?);";
    $data =array( $year );
    $sql = preparedQuery($grmysqli, $sql, $year);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }
  /*Επιστρέφει το id του μαθητή της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertGrade($grmysqli, $data)
  {
    $sql = "INSERT INTO GRADES (student_id,schoolcourse_id,term,grade,locked) VALUES (?, ?, ?, ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);

    //echo $sql;
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo '<br>Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<hr>";
      echo $sql;
      echo "<hr>";
      return false;
    }	

    return mysqli_affected_rows($grmysqli); //We do not use AUTO_INCREMENT so mysqli_insert_id returns 0

    //return mysqli_insert_id($grmysqli);

  }

  /*Επιστρέφει το id της νέας εγγραφής αλλιώς επιστρέφει false*/
  function insertExcelfile($grmysqli, $data )
  {
    $sql = "INSERT INTO EXCELFILES (filename,teacher_id,uploaddate) VALUES ( ?, ?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);


    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }

  /*Επιστρέφει το πλήθος των εγγραφών που επηρεάστηκαν αλλιώς επιστρέφει false*/
  function updateGrade($grmysqli, $data)
  {
    $sql = "UPDATE GRADES SET grade=?, locked=? WHERE student_id=? AND schoolcourse_id=? AND term=?;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    //echo $sql;

    if (!mysqli_query($grmysqli, $sql))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_affected_rows($grmysqli);

  }


  //Καλείται κάθε φορά που πρόκειται να εκτελέσουμε ένα sql query
  //Στο $code υπάρχει το query και στο $data περνάμε τα δεδομένα με τη μορφή array
  //Επιστρέφει το query σε πλήρη μορφή αποφεύγοντας τα sql injections 
  //που δημιουργούνται από τα  NUL (ASCII 0), \n, \r, \, ', ", και Control-Z. 
  function preparedQuery($grmysqli,  $sqlcode, $data )
  {
    //if ($DEBUG_db_queries) { echo "<br>in prepared query"; }
    $parts = explode( '?', $sqlcode );

    $sql = '';
    if(is_array($data)){
      //if ($DEBUG_db_queries) {echo "<br>is ARRAY";}
      foreach ( $data as $value ) {
        $sql .= array_shift( $parts );
        if (is_null( $value ) )
	{
		$sql .= "NULL";
	} 
	elseif (is_bool( $value ) ) 
	{
		if ($value) {
			$sql .= 1;
		} else {
			$sql .= 0;
		}
	}
	elseif (is_numeric( $value ) ) 
	{
          $sql .= mysqli_real_escape_string($grmysqli, $value);
	} 
	elseif ($value == "FALSE") {
		$sql .= 0;
	}
        else
        {
          $sql .= "'" . mysqli_real_escape_string($grmysqli, $value) . "'";
        }

        //'"'.addslashes($value). '"';
      }
      $sql .= array_shift( $parts );
    }else{		
      $sql .= array_shift( $parts );
      $sql .= mysqli_real_escape_string($grmysqli, $data);					
      $sql .= array_shift( $parts );
    }
    //if ($DEBUG_db_queries) { echo("<br>Query to execute:". $sql); }

    return $sql;
    //	return mysqli_query( $sql )
    //			or die( mysqli_error($grmysqli) );
  }

  function getTeacher($grmysqli , $teacherid) 
  {
    $sql = "SELECT * FROM TEACHERS WHERE id=?;";
    $sql = preparedQuery($grmysqli, $sql, $teacherid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getTeachers($grmysqli ) 
  {
    $sql = "SELECT * FROM TEACHERS;";
    //	$sql = preparedQuery($sql, $lessonid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $teacher['id'] = $row['id'];
      $teacher['lastname'] = $row['lastname'];
      $teacher['firstname'] = $row['firstname'];
      $teacher['fathername'] = $row['fathername'];
      $teacher['mothername'] = $row['mothername'];
      $teacher['username'] = $row['username'];
      $teacher['isadmin'] = $row['isadmin'];

      $array[] = $teacher;
    }

    return (array) $array;
  }

  function getTeachersOfCourseId($grmysqli, $teacher_id, $course_id)
  {
    $data =array( $teacher_id, $course_id );
    $sql = "SELECT id,schoolcourse_id FROM TEACHERSOFSCHOOLCOURSES WHERE teacher_id=? AND schoolcourse_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' \n ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getAYear($grmysqli, $year)
  {
    $data =array( $year );
    $sql = "SELECT *  FROM SCHOOLYEARS WHERE startyear=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' \n ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getASchoolLesson($grmysqli, $lesson)
  {
    // echo "In getASchoolLesson";
    $data =array( $lesson );
    $sql = "SELECT *  FROM SCHOOLLESSONS WHERE LESSONNAME=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    echo "executing $sql:";

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }


    $row = mysqli_fetch_array($result);

    echo "$row <br>";

    return $row;
  }

  function getLessonSet($grmysqli, $lessonname)
  {
    $array = null;

    $pieces = explode("-(Ε", $lessonname, -1);
    if(sizeof($pieces) == 0){
      $pieces = explode("-(Θ", $lessonname, -1);		
    }

    if(sizeof($pieces) > 0){
      $pieces[0] = $pieces[0] ."%";
      $data = array($pieces[0]);

      $sql = "SELECT * FROM SCHOOLLESSONS WHERE LESSONNAME LIKE ? ORDER BY lessonname;";
      $sql = preparedQuery($grmysqli, $sql, $data);		
      $result = mysqli_query($grmysqli, $sql);

      if (!isset($result) || $result==FALSE)  {
        echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
        return null	;
      }

      while($row = mysqli_fetch_array($result)) {
        $lesson['id'] = $row['id'];
        $lesson['lessonname'] = $row['lessonname'];

        $array[] = $lesson;
      }		
    }

    return (array) $array;
  }


  function getSchoolCourse($grmysqli, $schoolcourse_id)
  {
    $sql = "SELECT * FROM SCHOOLCOURSES WHERE id=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $schoolcourse_id);

    $result = mysqli_query($grmysqli, $sql);
    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

/*
  Φαίνεται να μην χρησιμοποιείται 
function getASchoolCourseNoTeacher($grmysqli,  $schoolclass_id, $lesson_id)
{

  $data = array( $schoolclass_id, $lesson_id);
  $sql = "SELECT * FROM SCHOOLCOURSES WHERE schoolclass_id=? and lesson_id=? LIMIT 1;";
  $sql = preparedQuery($grmysqli, $sql, $data);

  $result = mysqli_query($grmysqli, $sql);
  if (!isset($result) || $result==FALSE)  {
    echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
    return null	;
  }

  $row = mysqli_fetch_array($result);

  return $row;
}
 */

  function getASchoolCourse($grmysqli, $schoolclass_id, $lesson_id)
  {
    $data = array( $schoolclass_id, $lesson_id);
    $sql = "SELECT * FROM SCHOOLCOURSES WHERE schoolclass_id=? and lesson_id=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);

    $result = mysqli_query($grmysqli, $sql);
    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getASchoolCourseTeacher($grmysqli, $schoolcourse_id)
  {
    $sql = "SELECT DISTINCT TEACHERS.* FROM " .
      " TEACHERS INNER JOIN TEACHERSOFSCHOOLCOURSES " .
      " ON TEACHERSOFSCHOOLCOURSES.TEACHER_ID = TEACHERS.ID " .
      " WHERE SCHOOLCOURSES.ID = ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolcourse_id);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getATeacher($grmysqli, $lastname, $firstname, $fathername, $mothername, $username)
  {
    // echo "In getATeacher";
    $data =array( $lastname, $firstname, $fathername, $mothername, $username);
    $sql = "SELECT *  FROM TEACHERS WHERE LASTNAME=? AND FIRSTNAME=? AND FATHERNAME=? AND MOTHERNAME=? AND USERNAME=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }


  /*Επιστρέφει τους καθηγητές  που ανήκουν σε συγκεκριμένο σχολείο */
  function getSchoolTeachers( $grmysqli, $schoolid)
  {
    $allteachers = array();

    $sql = "SELECT * FROM SCHOOLCLASSES WHERE school_id=?";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {

      $temp = getClassTeachers($grmysqli, $row['id']);
      foreach($temp as $t){
        if(!in_array($t, $allteachers, true)){
          array_push($allteachers, $t);
        }
      }
    }

    return (array) $allteachers;
  }

  /*Επιστρέφει τους καθηγητές αναλογα με τα δικαιώματά τους*/
  function getTeachersPriviledge($grmysqli, $isadmin ) 
  {
    $sql = "SELECT *  FROM TEACHERS WHERE ISADMIN=?;";
    $sql = preparedQuery($grmysqli, $sql, $isadmin);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $teacher['id'] = $row['id'];
      $teacher['lastname'] = $row['lastname'];
      $teacher['firstname'] = $row['firstname'];
      $teacher['fathername'] = $row['fathername'];
      $teacher['mothername'] = $row['mothername'];
      $teacher['username'] = $row['username'];

      $array[] = $teacher;
    }

    return (array) $array;
  }

  /*Επιστρέφει τους καθηγητές που διδάσκουν συγκεκριμένο μάθημα*/
  function getLessonTeachers( $grmysqli, $lessonid ) 
  {
    $sql = "SELECT DISTINCT TEACHERS.*, SCHOOLCOURSES.schoolclass_id " .
      " FROM TEACHERS, SCHOOLCOURSES, TEACHERSOFSCHOOLCOURSES " .
      " WHERE TEACHERSOFSCHOOLCOURSES.TEACHER_ID = TEACHERS.ID " .
      " and SCHOOLCOURSES.id = TEACHERSOFSCHOOLCOURSES.schoolcourse_id " .
      " AND SCHOOLCOURSES.LESSON_ID = ?;";
    $sql = preparedQuery($grmysqli, $sql, $lessonid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $teacher['id'] = $row['id'];
      $teacher['lastname'] = $row['lastname'];
      $teacher['firstname'] = $row['firstname'];
      $teacher['fathername'] = $row['fathername'];
      $teacher['mothername'] = $row['mothername'];
      $teacher['username'] = $row['username'];
      $teacher['isadmin'] = $row['isadmin'];
      $teacher['schoolclassid'] = $row['schoolclass_id'];

      $array[] = $teacher;
    }

    return (array) $array;
  }

  /*Επιστρέφει τους διδάσκοντες καθηγητές συγκεκριμένης τάξης*/
  function getClassTeachers( $grmysqli, $classid ) 
  {
    $sql = 
      "SELECT DISTINCT TEACHERS.* " .
      " FROM " .
      " TEACHERS INNER JOIN TEACHERSOFSCHOOLCOURSES  " .
      " ON TEACHERSOFSCHOOLCOURSES.TEACHER_ID = TEACHERS.ID " .
      " INNER JOIN SCHOOLCOURSES " .
      " ON SCHOOLCOURSES.ID = TEACHERSOFSCHOOLCOURSES.schoolcourse_id " .
      " WHERE SCHOOLCOURSES.SCHOOLCLASS_ID = ?;";
    $sql = preparedQuery($grmysqli, $sql, $classid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $teacher['id'] = $row['id'];
      $teacher['lastname'] = $row['lastname'];
      $teacher['firstname'] = $row['firstname'];
      $teacher['fathername'] = $row['fathername'];
      $teacher['mothername'] = $row['mothername'];
      $teacher['username'] = $row['username'];
      $teacher['isadmin'] = $row['isadmin'];

      $array[] = $teacher;
    }

    return (array) $array;
  }




  function getSchools( $grmysqli )
  {
    $sql = "SELECT *  FROM SCHOOLS;";
    //	$sql = preparedQuery($sql, $classid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $school['id'] = $row['id'];
      $school['schoolname'] = $row['schoolname'];


      $array[] = $school;
    }

    return (array) $array;
  }

  function getASchool($grmysqli, $schoolid )
  {
    $sql = "SELECT *  FROM SCHOOLS WHERE ID=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getAClass($grmysqli, $classid )
  {
    $sql = "SELECT *  FROM SCHOOLCLASSES WHERE ID=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $classid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  function getClasses($grmysqli  )
  {
    //echo "in getClasses";
    $sql = "SELECT *  FROM SCHOOLCLASSES;";
    //	$sql = preparedQuery($sql, $classid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $schoolclass['id'] = $row['id'];
      $schoolclass['classlevel'] = $row['classlevel'];
      $schoolclass['classnumber'] = $row['classnumber'];
      $schoolclass['school_id'] = $row['school_id'];
      $schoolclass['startyear'] = $row['startyear'];


      $array[] = $schoolclass;
    }

    return (array) $array;
  }

  /*Επιστρέφει όλα τα τμήματα που ανήκουν σε συγκεκριμένο σχολείο και χρονιά*/
  function getSchoolClasses($grmysqli, $schoolid, $year )
  {
    // echo "In getSchoolClasses";
    $data =array( $schoolid , $year);
    $sql = "SELECT *  FROM SCHOOLCLASSES WHERE SCHOOL_ID=? AND STARTYEAR=? ORDER BY CLASSLEVEL ASC;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $schoolclass['id'] = $row['id'];
      $schoolclass['classlevel'] = $row['classlevel'];
      $schoolclass['classnumber'] = $row['classnumber'];
      $schoolclass['school_id'] = $row['school_id'];
      $schoolclass['startyear'] = $row['startyear'];


      $array[] = $schoolclass;
    }

    return (array) $array;
  }

  function getSchoolClasses2($grmysqli, $schoolid )
  {
    // echo "In getSchoolClasses";
    //$data =array( $schoolid , $year);
    $sql = "SELECT *  FROM SCHOOLCLASSES WHERE SCHOOL_ID=? ORDER BY classlevel ASC;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $schoolclass['id'] = $row['id'];
      $schoolclass['classlevel'] = $row['classlevel'];
      $schoolclass['classnumber'] = $row['classnumber'];
      $schoolclass['school_id'] = $row['school_id'];
      $schoolclass['startyear'] = $row['startyear'];


      $array[] = $schoolclass;
    }

    return (array) $array;
  }


  function getASchoolClass($grmysqli, $classlevel, $classnumber, $schoolid, $startyear)
  {
    //echo "In getASchoolClass";
  /*
     classnumber είναι το Α, Β, Γ
     classlevel είναι το A1-Proj, BΠ, ΓΓεωπ.
   */
    $data =array( $classlevel, $classnumber, $schoolid , $startyear);
    $sql = "SELECT *  FROM SCHOOLCLASSES WHERE CLASSLEVEL=? AND CLASSNUMBER=? AND SCHOOL_ID=? AND STARTYEAR=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }


  function getLessons($grmysqli )
  {
    $sql = "SELECT *  FROM SCHOOLLESSONS;";
    //	$sql = preparedQuery($sql, $classid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $lessons['id'] = $row['id'];
      $lessons['lessonname'] = $row['lessonname'];

      $array[] = $lessons;
    }

    return (array) $array;
  }


  function getLessonName($grmysqli,$id )
  {

    $sql = "SELECT LESSONNAME FROM SCHOOLLESSONS WHERE ID=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $id);

    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row['LESSONNAME'];
  }


  /*Επιστρέφει τα μαθήματα συγκεκριμένου καθηγητή*/
  function getTeacherLessons( $grmysqli,$teacherid ) 
  {
    $sql = "SELECT DISTINCT " .
      " SCHOOLLESSONS.*,SCHOOLCOURSES.id AS scid, " .
      " SCHOOLCOURSES.schoolclass_id AS cid  " .
      " FROM " .
      " SCHOOLLESSONS inner join SCHOOLCOURSES " .
      " on SCHOOLLESSONS.id = SCHOOLCOURSES.lesson_id " .
      " inner join TEACHERSOFSCHOOLCOURSES " .
      " on TEACHERSOFSCHOOLCOURSES.schoolcourse_id=SCHOOLCOURSES.id " .
      " where TEACHERSOFSCHOOLCOURSES.TEACHER_ID =?;";
    $sql = preparedQuery($grmysqli, $sql, $teacherid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }
    $array = null;
    while($row = mysqli_fetch_array($result)) {
      $lesson['id'] = $row['id'];
      $lesson['lessonname'] = $row['lessonname'];
      $lesson['schoolcourseid'] = $row['scid'];
      $lesson['schoolclassid'] = $row['cid'];

      $array[] = $lesson;
    }

    return (array) $array;
  }

  /*Επιστρέφει τα μαθήματα συγκεκριμένου τμήματος*/
  function getClassLessons($grmysqli,$classid ) 
  {

    $sql = "SELECT DISTINCT SCHOOLLESSONS.*, SCHOOLCOURSES.ID AS cid " .
      " FROM SCHOOLLESSONS, SCHOOLCOURSES " .
      " WHERE " .
      " SCHOOLCOURSES.LESSON_ID = SCHOOLLESSONS.ID " .
      " AND SCHOOLCOURSES.SCHOOLCLASS_ID =?;";
    $sql = preparedQuery($grmysqli, $sql, $classid);
    //echo $sql;
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $lesson['id'] = $row['id'];
      $lesson['lessonname'] = $row['lessonname'];
      $lesson['schoolcourseid'] = $row['cid'];

      $array[] = $lesson;
    }

    return (array) $array;
  }

  function getStudents($grmysqli  )
  {
    $sql = "SELECT *  FROM STUDENTS;";
    //	$sql = preparedQuery($sql, $classid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $students['id'] = $row['id'];
      $students['lastname'] = $row['lastname'];
      $students['firstname'] = $row['firstname'];
      $students['fathername'] = $row['fathername'];
      $students['mothername'] = $row['mothername'];
      $students['birthdate'] = $row['birthdate'];


      $array[] = $students;
    }

    return (array) $array;
  }

  /*Επιστρέφει τους μαθητές και το ΑΜ τους που ανήκουν σε συγκεκριμένο σχολείο και χρονιά*/
  function getSchoolStudents( $grmysqli,$schoolid, $year )
  {
    $data[0] = $schoolid;
    $data[1] = $year;
    $sql = "SELECT STUDENTS.*, SCHOOLREGISTRATIONS.STUDENT_AM AS AM FROM STUDENTS INNER JOIN SCHOOLREGISTRATIONS ON SCHOOLREGISTRATIONS.STUDENT_ID=STUDENTS.ID WHERE SCHOOLREGISTRATIONS.SCHOOL_ID=? AND SCHOOLREGISTRATIONS.STARTYEAR=?;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    while($row = mysqli_fetch_array($result)) {
      $students['id'] = $row['id'];
      $students['lastname'] = $row['lastname'];
      $students['firstname'] = $row['firstname'];
      $students['fathername'] = $row['fathername'];
      $students['mothername'] = $row['mothername'];
      $students['birthdate'] = $row['birthdate'];
      $students['am'] = $row['am'];

      $array[] = $students;
    }

    return (array) $array;
  }



  /*Επιστρέφει το μαθητή που έχει συγκεκριμένο ΑΜ και ανήκει σε συγκεκριμένο σχολείο και χρονιά*/
  function getSpecificStudent( $grmysqli,$schoolid, $year, $studentam )
  {
    $data[0] = $schoolid;
    $data[1] = $year;
    $data[2] = $studentam;
    $sql = "SELECT STUDENTS.* FROM STUDENTS INNER JOIN SCHOOLREGISTRATIONS ON SCHOOLREGISTRATIONS.STUDENT_ID=STUDENTS.ID WHERE SCHOOLREGISTRATIONS.SCHOOL_ID=? AND SCHOOLREGISTRATIONS.STARTYEAR=? AND SCHOOLREGISTRATIONS.STUDENT_AM=?;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

  /*while($row = mysqli_fetch_array($result)) {
                $students['id'] = $row['id'];
                $students['lastname'] = $row['lastname'];
                $students['firstname'] = $row['firstname'];
        $students['fathername'] = $row['fathername'];
        $students['mothername'] = $row['mothername'];
        $students['birthdate'] = $row['birthdate'];

                $array[] = $students;
  }*/

    $row = mysqli_fetch_array($result);
    return $row;
    //return (array) $array[];
  }

  function getAStudent ($grmysqli, $lastname, $firstname, $fathername, $mothername)
  {
    $data =array( $lastname, $firstname, $fathername, $mothername);
    $sql = "SELECT STUDENTS.* FROM STUDENTS WHERE lastname=? and firstname=? and fathername=? and mothername=? limit 1 ;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }


  function getStudentById ($grmysqli, $id)
  {
    $sql = "SELECT * FROM STUDENTS WHERE id=? limit 1 ;";
    $sql = preparedQuery($grmysqli, $sql, $id);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }




  /*Επιστρέφει τους μαθητές που έχουν εγγραφεί σε συγκεκριμένο τμήμα*/
  function getClassStudents( $grmysqli, $schoolid, $schoolclassregistrationid )
  {
    $data = array( $schoolclassregistrationid, $schoolid);
    $sql = "SELECT STUDENTS.*,SCHOOLREGISTRATIONS.student_am 
      FROM STUDENTS 
      INNER JOIN SCHOOLCLASSREGISTRATIONS 
      ON SCHOOLCLASSREGISTRATIONS.STUDENT_ID=STUDENTS.ID 
      INNER JOIN SCHOOLREGISTRATIONS 
      ON SCHOOLREGISTRATIONS.STUDENT_ID=STUDENTS.ID 
      WHERE 
      SCHOOLCLASSREGISTRATIONS.SCHOOLCLASS_ID=? 
      AND SCHOOLREGISTRATIONS.school_id=?;";
$sql = preparedQuery($grmysqli, $sql, $data);

$result = mysqli_query($grmysqli, $sql);

if (!isset($result) || $result==FALSE)  {
  echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
  return null	;
}

$array = null;    
while($row = mysqli_fetch_array($result)) {
  $students['id'] = $row['id'];
  $students['lastname'] = $row['lastname'];
  $students['firstname'] = $row['firstname'];
  $students['fathername'] = $row['fathername'];
  $students['mothername'] = $row['mothername'];
  $students['birthdate'] = $row['birthdate'];
  $students['student_am'] = $row['student_am'];

  $array[] = $students;
}

return (array) $array;
  }
  function getAClassRegistration($grmysqli, $student_id, $schoolclass_id)
  {
    $sql = "select * from SCHOOLCLASSREGISTRATIONS where student_id=? and schoolclass_id=? limit 1;";
    $data =array( $student_id, $schoolclass_id); 
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  //Χωρίς του schoolyear μιας και η εγγραφή στο στο σχολείο γίνεται ένα συγκεκριμένο 
  //έτος. Η επανεγγραφή σε τάξη έχει δικό της school year.
  function getASchoolRegistration($grmysqli, $school_id, $student_id, $student_am)
  {
    $sql = "select * from SCHOOLREGISTRATIONS where school_id=? and student_id=? and student_am=? limit 1;";
    $data =array( $school_id, $student_id, $student_am );
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }

    $row = mysqli_fetch_array($result);

    return $row;
  }

  /*Επιστρέφει το βαθμό ενός μαθητή σε συγκεκριμένο μάθημα συγκεκριμένο τετράμηνο*/
  function getAStudentLessonGrade( $grmysqli,$studentid, $courseid, $semester)
  {
    $data = array($studentid, $courseid, $semester);
    $sql = "SELECT * FROM GRADES WHERE student_id=? AND schoolcourse_id=? AND term=? LIMIT 1;";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);


    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null	;
    }


    $row = mysqli_fetch_array($result);
  /* debuging
  echo "For student $studentid course:$courseid and semester $semester<br>";
  print_r( $row );
  echo "<br>";
   */
    return $row;
  }


  /*Επιστρέφει τους βαθμούς των μαθητών συγκεκριμένου τμήματος σε όλα τα μαθήματα, ανά μάθημα*/
  function getClassGrades( $grmysqli,$schoolclassid )
  {
    $sql = "SELECT STUDENTS.*, GRADES.*, SCHOOLCOURSES.LESSON_ID AS lessonid 
      FROM STUDENTS, GRADES 
      INNER JOIN SCHOOLCOURSES 
      ON SCHOOLCOURSES.ID= GRADES.SCHOOLCOURSE_ID 
      WHERE STUDENTS.ID=GRADES.STUDENT_ID 
      AND GRADES.SCHOOLCOURSE_ID IN 
      (
        SELECT ID FROM SCHOOLCOURSES WHERE SCHOOLCLASS_ID=?
      ) 
      GROUP BY SCHOOLCOURSES.LESSON_ID;";
$sql = preparedQuery($grmysqli, $sql, $schoolclassid);
$result = mysqli_query($grmysqli, $sql);

if (!isset($result))  {
  echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
  return null	;
}

$array = null;
while($row = mysqli_fetch_array($result)) {
  $grades['id'] = $row['id'];
  $grades['lastname'] = $row['lastname'];
  $grades['firstname'] = $row['firstname'];
  $grades['fathername'] = $row['fathername'];
  $grades['mothername'] = $row['mothername'];
  $grades['birthdate'] = $row['birthdate'];
  $grades['term'] = $row['term'];
  $grades['grade'] = $row['grade'];
  $grades['locked'] = $row['locked'];
  $grades['lessonid'] = $row['lessonid'];

  $array[] = $grades;
}

return (array) $array;
  }

  function getATeacherByUsername($grmysqli, $username)
  {
    $sql = "SELECT * FROM TEACHERS where username=\"$username\";";
    //$sql = preparedQuery($grmysqli, $sql, $username);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $user = null;
    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];
      $user['isadmin'] = $row['isadmin'];
    }

    return  $user;
  }


  function getAUserByUsername($grmysqli, $username, $schoolcode=NULL)
  {
    $sql = "SELECT * FROM USERS where username=\"$username\" and school_id=$schoolcode;";
    if ($schoolcode==NULL) {
      $sql = "SELECT * FROM USERS where username=\"$username\"; ";
    }
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $array = null;
    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];

      $user['isadmin'] = $row['isadmin'];
      $user['school_id'] = $row['school_id'];

      $array[] = $user;
    }

    return (array) $array;
  }

  function getUsers($grmysqli) 
  {
    $sql = "SELECT * FROM USERS;";
    //	$sql = preparedQuery($sql, $lessonid, $grmysqli);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];

      $user['isadmin'] = $row['isadmin'];
      $user['school_id'] = $row['school_id'];

      $array[] = $user;
    }

    return (array) $array;
  }

  /*Επιστρέφει τα σχολεία που ανήκει ο χρήστης και όλα τα άλλα πεδία*/
  function getUserSchools($grmysqli,$userid )
  {
    $sql = "SELECT *  FROM USERS WHERE id=?;";
    $sql = preparedQuery($grmysqli, $sql, $userid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];
      $user['isadmin'] = $row['isadmin'];
      $user['school_id'] = $row['school_id'];

      $array[] = $user;
    }

    return (array) $array;
  }
  //get messages for school teachers by school ops via school id
  function getSchoolMessages($grmysqli,$schoolid )
  {
    $sql = "SELECT m.message, m.from_user_id, u.school_id  FROM USERS as u, MESSAGES as m WHERE u.id=m.from_user_id and u.school_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==false)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $array = null;
    while($row = mysqli_fetch_array($result)) {
      $message['message'] = $row['message'];
      $message['from_user_id'] = $row['from_user_id'];
      $array[] = $message;
    }

    return (array) $array;
  }

  function getSchoolUsers($grmysqli,$schoolid ) 
  {
    $sql = "SELECT *  FROM USERS WHERE SCHOOL_ID=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];
      $user['isadmin'] = $row['isadmin'];

      $array[] = $user;
    }

    return (array) $array;
  }



  /*Επιστρέφει τους χρήστες αναλογα με τα δικαιώματά τους*/
  function getUsersByPriviledge($grmysqli,$isadmin ) 
  {
    $sql = "SELECT *  FROM USERS WHERE ISADMIN=?;";
    $sql = preparedQuery($grmysqli, $sql, $isadmin);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return null;
    }

    while($row = mysqli_fetch_array($result)) {
      $user['id'] = $row['id'];
      $user['school_id'] = $row['school_id'];

      $array[] = $user;
    }

    return (array) $array;
  }

  /*Επιστρέφει τους χρήστες ενός σχολείου μαζί με username και κωδικούς για τα καρτελάκια */
  function getUsersBySchoolWithPasswords($grmysqli, $schoolid)
  {
    $sql = "select USERS.username as username, password, lastname, firstname " .
      " from USERS inner join TEACHERS " .
      " on USERS.username=TEACHERS.username " .
      " where USERS.school_id=? " .
      " order by convert(lastname using utf8) collate utf8_bin; "; //sorting does not work well
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result))  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>$sql<br>";
      return null;
    }

    $array=null;
    while($row = mysqli_fetch_array($result)) {
      $array[] = $row;
    }

    return (array) $array;
  }

  function getMessageBySenderUserId($grmysqli, $user_id)
  {
    $sql = "SELECT * FROM MESSAGES where from_user_id=\"$user_id\";";
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' . mysqli_sqlstate($grmysqli) . ' - ' . mysqli_error($grmysqli);
      return null;
    }

    $array = null;
    while($row = mysqli_fetch_array($result)) {
      $message['id'] = $row['id'];
      $message['from_user_id'] = $row['from_user_id'];
      $message['message'] = $row['message'];

      $array[] = $message;
    }

    return (array) $array;
  }

  function setUserMessage($grmysqli, $data)//1 user_id,0 message
  {	
/*	$messages = getMessageBySenderUserId($grmysqli, $data[1]);
  if (($messages[0]>"")) {
    $sql="UPDATE MESSAGES SET message =?  WHERE from_user_id = ?";
  }
  else{
    $sql="INSERT INTO MESSAGES (message, from_user_id) VALUES (?, ?)";
  }
 */

    $messages = getMessageBySenderUserId($grmysqli, $data[1]);
    if($messages == null){
      $sql="INSERT INTO MESSAGES (message, from_user_id) VALUES (?, ?)";		
    }else if (($messages[0]>"")) {
      $sql="UPDATE MESSAGES SET message =?  WHERE from_user_id = ?";
    }

/*
  $messages = getMessageBySenderUserId($grmysqli, $data[0]);
  if (($messages[0]>"")) {
                $sql="UPDATE MESSAGES SET message = \"$data[1]\" WHERE from_user_id = \"$data[0]\"";
        }
        else{
                $sql="INSERT INTO MESSAGES (from_user_id, message) VALUES (\"$data[0]\", \"$data[1]\")";
        }
 */
    //$sql= mysql_escape_string($sql);
    //$sql = "INSERT INTO MESSAGES (from_user_id, message) VALUES (?, ?);";
    $sql = preparedQuery($grmysqli, $sql, $data);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      return false;
    }	

    return mysqli_insert_id($grmysqli);

  }


  function getLessonGradesStatus($grmysqli, $schoolid, $semester){
    $allteacher = getSchoolTeachers( $grmysqli, $schoolid);

    //change array data from rows to columns
    foreach ($allteacher as $key => $row) {
      $lastname[$key]  = $row['lastname'];
    }

    // Sort the data with lastname  ascending
    array_multisort($lastname, SORT_ASC, $allteacher);

    foreach($allteacher as $tempteach){
      $lessons = getTeacherLessons( $grmysqli,$tempteach['id']);

      foreach($lessons as $les){
        $res = getSchoolcourseLockStatus2($grmysqli, $les['schoolcourseid'],$semester);

        $cl = getAClass($grmysqli, $les['schoolclassid'] );

        if($res == 0){
          echo "<br><span>Το μάθημα ". $cl['classlevel']. " - ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']." είναι ξεκλείδωτο</span>";
        }else if($res == 1){
          //echo "<br><span>Tο μάθημα ". $cl['classlevel']. " - ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']." είναι κλειδωμένο</span>";
        }else if($res == -1){
          echo "<br><span id='infoerror'>Tο μάθημα ". $cl['classlevel']. " - ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']." δεν έχει βαθμούς</span>";
        }else{
          //echo mysqli_error($grmysqli);
          echo "<br><span id='infoerror'>Σφάλμα κατά τη διαπίστωση της κατάστασης του μαθήματος ". $les['lessonname'] ." του ".$tempteach['lastname'] ." ".$tempteach['firstname']."</span>";
        }
      }			
    }

  }


/*
   Empties the GRADES for a specific SCHOOL
   Returns TRUE on success, FALSE on error
 */
  function deleteGradesForSchool($grmysqli, $schoolid)
  {
  /* Τσέκαρε οπωσδήποτε τον κωδικό σχολείου!
  Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
    if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1) {
      return FALSE;
    }

    $sql = "delete GRADES " .//, SCHOOLCLASSES, SCHOOLCLASSREGISTRATIONS, SCHOOLCOURSES, SCHOOLREGISTRATIONS " .
      " from " .
      " GRADES inner join " .
      " SCHOOLCOURSES on GRADES.schoolcourse_id=SCHOOLCOURSES.id " .
      " inner join " .
      " SCHOOLCLASSES on SCHOOLCOURSES.schoolclass_id=SCHOOLCLASSES.id " .
      " inner join " .
      " SCHOOLCLASSREGISTRATIONS on SCHOOLCLASSREGISTRATIONS.schoolclass_id=SCHOOLCLASSES.id ".
      " inner join " .
      " SCHOOLREGISTRATIONS on SCHOOLREGISTRATIONS.school_id=SCHOOLCLASSES.school_id " .
      " where " .
      " SCHOOLCLASSES.school_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    return TRUE;
  }

/*
   Empties the COURSEREGISTRATIONS for a specific SCHOOL
   Requires that the grades have been empty
   Returns TRUE on success, FALSE on error
 */
  function deleteCoursesForSchool($grmysqli, $schoolid)
  {
  /* Τσέκαρε οπωσδήποτε τον κωδικό σχολείου!
  Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
    if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1) {
      return FALSE;
    }

    /* 1. Σβήσιμο τον εγγραφών σε τάξεις */
    $sql = "delete SCHOOLCLASSREGISTRATIONS " .
      " from " .
      " SCHOOLCLASSREGISTRATIONS " .
      " inner join " .
      " SCHOOLCLASSES on SCHOOLCLASSREGISTRATIONS.schoolclass_id=SCHOOLCLASSES.id " .
      " where " .
      " SCHOOLCLASSES.school_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    /* Σβήσιμο των διδασκαλιών απο τον πίνακα TEACHERSOFSCHOOLCOURSES */
    $sql = "delete TEACHERSOFSCHOOLCOURSES ".
      " from " .
      " TEACHERSOFSCHOOLCOURSES " .
      " inner join " .
      " SCHOOLCOURSES " .
      " on TEACHERSOFSCHOOLCOURSES.schoolcourse_id = SCHOOLCOURSES.id " .
      " inner join " .
      " SCHOOLCLASSES " .
      " on SCHOOLCOURSES.schoolclass_id " .
      " where " .
      " SCHOOLCLASSES.school_id= ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);
    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }



    /*2. Σβήσιμο των διδασκαλιών, απαιτείται να μην υπάρχουν εγγραφές στις τάξεις. */
    $sql = "delete SCHOOLCOURSES " .
      " from " .
      " SCHOOLCOURSES " .
      " inner join " .
      " SCHOOLCLASSES " .
      " on SCHOOLCOURSES.schoolclass_id=SCHOOLCLASSES.id " .
      " where " .
      " SCHOOLCLASSES.school_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    /*3. Σβήσιμο των τμημάτων */
    $sql = "delete SCHOOLCLASSES " .
      " from " .
      " SCHOOLCLASSES " .
      " where " .
      " SCHOOLCLASSES.school_id=?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    /*4. Σβήσιμο των μαθημάτων στα οποία δεν αναφέρεται καμία διδασκαλία (καθάρισμα) */
    $sql = "delete from SCHOOLLESSONS " .
      " where " .
      " SCHOOLLESSONS.id " .
      " not in " .
      " (select SCHOOLCOURSES.lesson_id from SCHOOLCOURSES);";
    $result = mysqli_query($grmysqli, $sql);
    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    return TRUE;
  }

  function deleteStudentsForSchool($grmysqli, $schoolid)
  {
  /* Τσέκαρε οπωσδήποτε τον κωδικό σχολείου!
  Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
    if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1) {
      return FALSE;
    }

  /* Πρώτα μαρκάρουμε τους μαθητές που είναι για σβήσιμο.
    Αυτό γίνεται γιατί η πληροφορία του ποιός πρέπει να διαγραφεί,
    βρίσκεται στον πίνακα SCHOOLREGISTRATIONS, ο οποίος έχει 
    foreign key στους μαθητές, άρα δεν μπορώ να σβήσω πρώτα τους
  μαθητές και μετά τις σειρές στο SCHOOLREGISTRATIONS. */
    $sql = "update STUDENTS " .
      " SET mothername=concat('2delete',rand()) " .
      " WHERE STUDENTS.id IN ( " .
      "   select student_id FROM " .
      "   SCHOOLREGISTRATIONS where " .
      "   SCHOOLREGISTRATIONS.school_id = ? " .
      " ) " .
";";
$sql = preparedQuery($grmysqli, $sql, $schoolid);
$result = mysqli_query($grmysqli, $sql);

if (!isset($result) || $result==FALSE)  {
  echo "<br>";
  echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
  echo "<br>";
  echo "$sql";
  echo "<br>";
  echo "<br>";
  return FALSE;
}

  /* Κατόπιν σβήνουμε τις εγγραφές στο σχολείο.
    Μετά απο αυτό το delete, θα έχουμε ξεκρέμαστους μαθητές στον πίνακα
    STUDENTS, αλλά δεν μας νοιάζει γιατί τους έχουμε αλλάξει το όνομα σε 2delete,
  και επομένως θα τους σβήσουμε και αυτους αμέσως μετά. */

  /* Μαζί με το 2delete κολάμε και έναν τυχαίο αριθμό για να αποφύγουμε
  τις συννονημίες */
$sql = "delete SCHOOLREGISTRATIONS " .
  " from " .
  " STUDENTS inner join SCHOOLREGISTRATIONS " .
  " where " .
  " STUDENTS.id = SCHOOLREGISTRATIONS.student_id  AND ".
  " SCHOOLREGISTRATIONS.school_id = ?;";
$sql = preparedQuery($grmysqli, $sql, $schoolid);
$result = mysqli_query($grmysqli, $sql);

if (!isset($result) || $result==FALSE)  {
  echo "<br>";
  echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
  echo "<br>";
  echo "$sql";
  echo "<br>";
  echo "<br>";
  return FALSE;
}

/* Σβήσιμο μαθητών που είχαμε μαρκάρει πριν */
$sql = "delete " .
  " from STUDENTS ".
  " where " .
  " mothername like '2delete%' " .
";";
$result = mysqli_query($grmysqli, $sql);

if (!isset($result) || $result==FALSE)  {
  echo "<br>";
  echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
  echo "<br>";
  echo "$sql";
  echo "<br>";
  echo "<br>";
  return FALSE;
}
return TRUE;
  }

  function deleteTeachersForSchool($grmysqli, $schoolid)
  {
  /* Τσέκαρε οπωσδήποτε τον κωδικό σχολείου!
  Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
    if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1) {
      return FALSE;
    }

    $sql = "delete SCHOOLREGISTRATIONS " .
      " from " .
      " TEACHERS inner join SCHOOLREGISTRATIONS " .
      " where " .
      " TEACHERS.id = SCHOOLREGISTRATIONS.student_id AND ".
      " SCHOOLREGISTRATIONS.school_id = ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    $sql = "delete TEACHERS " .//, SCHOOLCLASSES, SCHOOLCLASSREGISTRATIONS, SCHOOLCOURSES, SCHOOLREGISTRATIONS " .
      " from " .
      " TEACHERS inner join SCHOOLREGISTRATIONS " .
      " where " .
      " TEACHERS.id = SCHOOLREGISTRATIONS.student_id AND ".
      " SCHOOLREGISTRATIONS.school_id = ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    $sql = "delete from USERS " .//, SCHOOLCLASSES, SCHOOLCLASSREGISTRATIONS, SCHOOLCOURSES, SCHOOLREGISTRATIONS " .
      " where " .
      " USERS.school_id = ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }
    return TRUE;
  }

  function deleteSchool($grmysqli, $schoolid)
  {
  /* Τσέκαρε οπωσδήποτε τον κωδικό σχολείου!
  Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
    if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1) {
      return FALSE;
    }

    $sql = "delete from SCHOOLS " .
      " where " .
      " SCHOOLS.id = ?;";
    $sql = preparedQuery($grmysqli, $sql, $schoolid);
    $result = mysqli_query($grmysqli, $sql);

    if (!isset($result) || $result==FALSE)  {
      echo "<br>";
      echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
      echo "<br>";
      echo "$sql";
      echo "<br>";
      echo "<br>";
      return FALSE;
    }

    return TRUE;
  }

  function getSemester(){
    $s=-1;

  /*if($today > mktime( ) ){


  }else if(){

  }*/

    return $s;
  }

}
?>

