<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
echo "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>".$websiteName."</title>
<link href='".$template."' rel='stylesheet' type='text/css' />
<link rel='stylesheet' type='text/css' href='css/vathmoi.css' />
<script src='js/jquery.min.js'></script>
<script src='js/validations.js' type='text/javascript'></script>
<script src='js/populate_lists.js' type='text/javascript'></script>
<script src='models/funcs.js' type='text/javascript'></script>
</head>";

?>
