	<?php

  global $Settings;


	require_once  ("PHPExcel.php");
	require_once  ("models/db-settings.php");
	require_once  ("models/db-queries.php");
	require_once	("basic_classes.php");
	require_once  ("models/config.php");

	if (!securePage($_SERVER['PHP_SELF'])){die();}

	/* Οι οδηγίες μεταφέρθηκαν στο upload excel */

	/* Τα ονόματα των στηλών που περιέχουν την αντίστοιχη πληροφοριά */
	define('AA',        $Settings->title_AA);
	define('AM',        $Settings->title_AM);
	define('EPONIMO',   $Settings->title_EPONIMO);
	define('ONOMA',     $Settings->title_ONOMA);
	define('FATHER',    $Settings->title_FATHER);
	define('MOTHER',    $Settings->title_MOTHER);
	define('MARK',      $Settings->title_MARK);
	define('OLOGRAFOS', $Settings->title_OLOGRAFOS);

	$expectedColumnsInOrderConfigured = FALSE; /* Θα γίνει configured στην πρώτη γραμμή που θα εντοπίσει */
	$expectedColumnsInOrder = array ( 
			0 => AA,

			AA      =>  0, //Απαραίτητο το Α/Α να είναι στην στήλη 0.
			AM      =>  1,
			EPONIMO =>  2,
			ONOMA   =>  3,
			FATHER  =>  4,
			MOTHER  =>  5,
			MARK    =>  6,
			OLOGRAFOS  =>  7);

	$gradingPeriodColumnsOrder = array (
			);



	if (isset( $argv[1] ) == FALSE)
	{
		echo "Please provide a filename.\n";
		echo "Usage php parser.php FILENAME CREATEUSERS? IMPORTGRADES?\n";
		return;
	}
	if (isset( $argv[2] ) == FALSE) $argv[2] = FALSE;
	//{
		//echo "Please after filename TRUE or FALSE for CreateUsers?\n";
		//echo "Usage php parser.php FILENAME CREATEUSERS? IMPORTGRADES?\n";
		//return;
	//}
	if (isset( $argv[3] ) == FALSE) $argv[3] = FALSE;
	//{
		//echo "Please after filename TRUE or FALSE for ImportGrades?\n";
		//echo "Usage php parser.php FILENAME CREATEUSERS? IMPORTGRADES?\n";
		//return;
	//}

	$CREATEUSERS = $argv[2]; /* Do create the users now */
	$DOINSERTGRADES = $argv[3]; /* If you find grades inside the XLS file, do insert them */
  $IGNORENOTEACHER = $argv[4]; /* Assign missing teachers to KANENAS */
	if ($Settings->getDebug())
	{
		if ($CREATEUSERS) 
			echo "<br>Με δημιουργία χρηστών<br>";
		else
			echo "<br>Χωρίς δημιουργία χρηστών<br>";

		if ($DOINSERTGRADES)
			echo "<br>Με καταχώρηση βαθμών</br>";
		else
			echo "<br>Χωρίς καταχώρηση βαθμών</br>";
	}
	$createdUsers = array (
		'username' => 'password'
		);
	$createdUsernames = array (
		'username' => 'Πλήρες όνομα'
		);


	$sourceXls = $argv[1];

	$myXls = null;
	try {
		$myXls = PHPExcel_IOFactory::load( $sourceXls );
	} catch (Exception $e) {
		echo "Exception $e";
		exit();
	}

	if ($myXls == null) {
		echo "<b>Δεν μπόρεσα να φορτώσω το αρχείο XLS.<b>\n";
		exit(1);
	}

	if ($Settings->getDebug()) echo "Loaded file $sourceXls<br>\n";

	//Detect school code id from filename (there is no such info inside the XLS as of 3/6/2014)
	$schoolCode = "";
	if ( (preg_match( "/(\d+)-*(\d*).*\.(xls|XLS)/", $sourceXls, $matches)!=1) )
	{
		echo "Το όνομα του αρχείου θα πρέπει να είναι ο κωδικός του σχολείου<br>\n";
		return;
	}
	else
	{
		$schoolCode = $matches[1];
		if ($Settings->getDebug()) echo "<h1>$schoolCode</h1>";
	}

	$myXls->setActiveSheetIndex( 0 );
	$as = $myXls->getActiveSheet();

	$highestRow = $as->getHighestRow(); // e.g. 10
	$highestColumn = $as->getHighestColumn(); // e.g 'F'
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	if ($Settings->getDebug()) echo "This file is <b>$highestRow</b> by <b>$highestColumnIndex</b> cells<br>\n";

	$mySchool = new School($grmysqli);
	$mySchool->setSchoolCode( $schoolCode );

	$curClass = new Tmima($grmysqli);
	$curLesson = new Lesson($grmysqli);
	$curTeacher = new Teacher($grmysqli);
	$curCourse = new Course($grmysqli);
	$parsingStudents = FALSE;

	/*
	   We process each ROW using two states.
	   STATE 1 = getting data about a course
	   STATE 2 = getting students grades of the course that was created in STATE 1

	   if (parsingStudents) then we are on STATE 2
	   else we are on STATE 1
	*/
	for ($row = 0; $row <= $highestRow; $row++) 
	{
		$col = 0;

		if (!$parsingStudents)
		{
			/*
			   Main loop for parsing data _about_ a course.
			   A separate loop is being used to parse the degrees of students for a certain course.
			   */

			while ($col <= $highestColumnIndex)
			{
				$cell = $as->getCellByColumnAndRow($col, $row);
				$cellValue = $cell->getValue();

				//
				// Get the school year
				//
				if ( (preg_match( "/.*Σχολικό έτος.*/", $cellValue, $matches)==1)
				   )
				{
					if ($Settings->getDebug()) echo "<br>εντοπισμός σχολικού έτους στο $col, $row<br>\n";
					$ncol = $col+1;
					do {
						$next_cell = $as->getCellByColumnAndRow(
								$ncol,
								$row);
						$next_cellValue = $next_cell->getValue();
						$v = trim($next_cellValue);
						$ncol = $ncol+1;
					} while (strcmp($v,'')==0 && $ncol<$highestColumnIndex);

					if (preg_match( "/(\d+)-(\d+)/", $v, $submatches ) != 1) 
					{
						echo "Λάθος στο σχολικό έτος ($row, $col) : $v <br>\n";
						exit(1);
					}
					$schoolYear = $submatches[1];
					if ($schoolYear < 2012 || $schoolYear>3000)
					{
						echo "Λάθος στο σχολικό έτος ($row, $col)<br>\n";
						exit(1);
					}
					$mySchool->setSchoolYear( $schoolYear );
					if ($mySchool->isInitialized()) $mySchool->store();

					if ($Settings->getDebug()) echo "<br>Βρήκα το σχολικό έτος $schoolYear<br>\n";
				}

				//
				// Get the school name
				//
				if ( (preg_match( "/.* ΛΥΚΕΙΟ .*/", $cellValue, $matches)==1)
						|| (preg_match( "/.* ΓΥΜΝΑΣΙΟ .*/", $cellValue, $matches)==1)
						|| (preg_match( "/.* ΕΠΑΛ .*/", $cellValue, $matches)==1)
						|| (preg_match( "/.* ΔΗΜΟΤΙΚΟ .*/", $cellValue, $matches)==1)
            || (preg_match( "/.* ΜΠΑΡ .*/", $cellValue, $matches)==1) /* Χρησιμοποιείται για debugging */
				   )
				{
					if ($Settings->getDebug()) echo "<br>εντοπισμός σχολείου στο $col, $row<br>\n";
					//print_r ($matches[0]);
					if ($mySchool->isInitialized() == FALSE)
					{
						$mySchool->data = $matches[0];
						if ($mySchool->isInitialized()) $mySchool->store();
						if ($Settings->getDebug()) echo "<br>Βρήκα το σχολείο! $matches[0]<br>\n";
					}
					else 
					{
						echo "<b>Warning!\n";
						echo "Reinitialized fullName of School\n";
						echo "at $col, $row</b></br>\n";
					}
				}

				//
				// get Tmima
				//
				if ((preg_match( "/.*Τμήμα:.*/", $cellValue, $matches)==1))
				{
					if ($Settings->getDebug()) echo "<br>εντοπισμός τμήματος στο $col, $row<br>\n";
					$curClass = new Tmima($grmysqli);
					$curCourse = new Course($grmysqli); //set the new course here
					$curClass->setSchool( $mySchool );

					$v = "";
					$ncol = $col+1;
					do
					{
						$next_cell = $as->getCellByColumnAndRow(
								$ncol,
								$row);
						$next_cellValue = $next_cell->getValue();
						$v = trim($next_cellValue);
						$ncol++;
					}
					while (strcmp($v, "")==0 &&
							$ncol < $highestColumnIndex);

					if ( strcmp($v, "") != 0) 
					{
						$curClass->setTmima( $v );
						$curCourse->setClass( $curClass );
						if ($Settings->getDebug()) echo "<br>Βρήκα το τμήμα! $v<br>\n";
					}
				}

				//
				// verify current class
				//
				if ((preg_match( "/.*Τάξη:.*/", $cellValue, $matches)==1))
				{
					if ($Settings->getDebug()) echo "<br>εντοπισμός τάξης στο $col, $row<br>\n";
					if (! $curClass->isInitialized())
					{
						echo "<b>Warning!\n";
						echo "Found Τάξη without having current class\n";
						echo "at $col, $row</b><br>\n";
					}
					else
					{
						$v = "";
						$ncol = $col+1;
						do
						{
							$next_cell = $as->getCellByColumnAndRow(
									$ncol,
									$row);
							$next_cellValue = $next_cell->getValue();
							$v = trim($next_cellValue);
							$ncol++;
						}
						while (strcmp($v, "")==0 &&
								$ncol < $highestColumnIndex);

						if ( strcmp($v, "") != 0) {
							if (strcmp ($v, $curClass->classLevel)!=0)
							{
								echo "<b>Προσοχή- Λάθος φορμάτ του αρχείου.!\n";
								echo "Mismatch of Τάξη $v and current class $curClass->classLevel\n";
								echo "at $col, $row</b><br>\n";
							}
						}
					}
				}

				//
				// get Lesson
				//
				if ((preg_match( "/^\s*Μάθημα:\s*$/", $cellValue, $matches)==1))
				{
					if ($Settings->getDebug()) echo "<br>εντοπισμός μαθήματος στο $col, $row<br>\n";
					$curLesson = new Lesson($grmysqli);
					$curLesson->setTmima( $curClass );
					
					$v = "";
					$ncol = $col+1;
					do
					{
						$next_cell = $as->getCellByColumnAndRow(
								$ncol,
								$row);
						$next_cellValue = $next_cell->getValue();
						$v = trim($next_cellValue);
						$ncol++;
					}
					while (strcmp($v, "")==0 &&
							$ncol < $highestColumnIndex);

					if ( strcmp($v, "") != 0) 
					{
						$curLesson->setLesson ( $v );
						$curCourse->setLesson( $curLesson );
						if ($Settings->getDebug()) echo "<br>Βρήκα το μάθημα! $v<br>\n";
					}


					//do not store lesson now. Wait for teacher
				}

				//
				// get Teachers
				//
				if ((preg_match( "/.*Διδάσκοντες.*/", $cellValue, $matches)==1))
				{

          /* Βρες το διπλανό κελί που δεν είναι κενό */
					$v = "";
					$ncol = $col+1;
					do
					{
						$next_cell = $as->getCellByColumnAndRow(
								$ncol,
								$row);
						$next_cellValue = $next_cell->getValue();
						$v = trim($next_cellValue);
						$ncol++;
					}
					while (strcmp($v, "")==0 &&
							$ncol < $highestColumnIndex);

          
					if ( strcmp($v, "") != 0) 
					{
            /* Σε αυτό το σημείο έχουμε βρεί το όνομα του καθηγητή. */

            //Το όνομα του καθηγητή μπορεί να είναι μία λίστα απο ονόματα.
            $teachers = explode( ",", $v );
            foreach($teachers as $t) {
              $t = trim($t); 
              $curTeacher = new Teacher($grmysqli);
              $curTeacher->setSchool( $mySchool );
              $curTeacher->setTeacher( $t );
              if ($CREATEUSERS) 
              {
                /* Κράτα τους χρήστες για να δείξουμε μετά τα καρτελάκια */
                $curTeacher->createUser();
                $curTeacher->addUserToUserCake();
                $createdUsers[ $curTeacher->getUsername() ] 
                  = $curTeacher->getPassword();
                $createdUsernames [ $curTeacher->getUsername() ] 
                  = $curTeacher->data;
              }

              $curLesson->addTeacher( $curTeacher );
              $curCourse->addTeacher( $curTeacher );
            }
						if ($Settings->getDebug()) echo "<br>Βρήκα τον καθηγητή! $v<br>\n";
					}
          else
          {
            if ($IGNORENOTEACHER == TRUE) {
              /* Δεν βρήκαμε όνομα καθηγητή, αλλά το αγνοούμε και δίνουμε το
              μάθημα στον ΚΑΝΕΝΑ */
              $curTeacher = new Teacher($grmysqli);
              $curTeacher->setSchool( $mySchool );
              $curTeacher->setTeacher( "ΚΑΝΕΝΑΣ ΜΑΚΑΝΕΝΑΣ" );
              if ($CREATEUSERS) 
              {
                /* Κράτα τους χρήστες για να δείξουμε μετά τα καρτελάκια */
                $curTeacher->createUser();
                $curTeacher->addUserToUserCake();
                $createdUsers[ $curTeacher->getUsername() ] 
                  = $curTeacher->getPassword();
                $createdUsernames [ $curTeacher->getUsername() ] 
                  = $curTeacher->data;
              }

              $curLesson->addTeacher( $curTeacher );
              $curCourse->addTeacher( $curTeacher );
            }

          }
				}

				if ($curLesson->isInitialized()) {
					$curLesson->store();
				}

				//
				// if everything is there, store the course (lesson, class, teacher)
				//
				if ($curCourse->isInitialized())
				{
          /*
            Αποθήκευση της τρέχουσας διδασκαλίας (τμήμα, μάθημα, καθηγητ* )
           */
					$curCourse->store();
				}

				//
				// check columns here
				//
				if ($col==0 && (strcmp($expectedColumnsInOrder[0], $cellValue)==0))
				{
					if (!$expectedColumnsInOrderConfigured)
					{
						/* Την πρώτη φορά που βρίσκω τα ονόματα των στηλών καθορίζω την σειρά
						   τους ώστε να ξέρω που βρίσκεται κάθε τι.
						   Μετά απλά ελέγχω ότι ισχύει η σειρά που εντόπισα εδώ.
						   */
						if ($Settings->getDebug()) echo "Trying to figure out column order\n";
						for ($c=0; $c<$highestColumnIndex; $c++)
						{
							$next_cellValue = $as
								->getCellByColumnAndRow($col+$c, $row)
								->getValue();
							$v = trim($next_cellValue);

							if (strcmp($v, "")!=0) 
							{
								if (isset($expectedColumnsInOrder[$v]))
								{
									if (strcmp($v, MARK)==0)
									{
										/* Μπορεί να έχουμε σε πολλά μέρη το "Αριθμητικώς".
										   Οπότε κάθε φορά που βλέπουμε την στήλη αυτή:
										   1) Κοιτάμε μία γραμμή παραπάνω για να δούμε την περίοδο
										   */
										$gradeColumn = $col+$c;
										$gradeTitleRow = $row-1;
										$gradePeriod = $as->getCellByColumnAndRow($gradeColumn, $gradeTitleRow)
											->getValue();

										$gradePeriod = trim( $gradePeriod );
										if (strcmp($gradePeriod, "")==0)
										{
											echo "<br>Βρέθηκε στήλη ($gradeColumn) με βαθμούς στην γραμμή ($row) αλλά "
												. " δεν βρήκα τον τίτλο της περιόδου</br>";
											exit(1);
										}

										if ($Settings->getDebug()) {
											echo "<br>Βρέθηκε στήλη ($gradeColumn) με βαθμούς στην γραμμή ($row) "
												. " ανήκει στην περίοδο $gradePeriod</br>";
										}


										/* 2) Κρατάμε σε ποιά στήλη το βρήκαμε */
										$gradingPeriodColumnsOrder[$gradeColumn] = $gradePeriod;
										$gradingPeriodColumnsOrder[$gradePeriod] = $gradeColumn;

										continue; 
									}
									$expectedColumnsInOrder[$v] = $c;
									$expectedColumnsInOrder[$c] = $v;
								}
								else
								{
									echo "<br>Βρήκα μία στήλη που δεν την περίμενα ($v) στην γραμμή $row και την αγνοώ<br>\n";
								}
							}
						}//for expectedColumnsInOrder

						//print_r( $expectedColumnsInOrder );
						//exit();

						$expectedColumnsInOrderConfigured=TRUE;
					}

					if ($Settings->getDebug()) echo "row:$row $cellValue $expectedColumnsInOrder[0]<br>\n";
					try 
					{
						$allColumnsOk = TRUE;
						for ($c=0; $c<$highestColumnIndex; $c++)
						{
							$next_cellValue = $as
								->getCellByColumnAndRow($col+$c, $row)
								->getValue();
							$v = trim($next_cellValue);

							if (strcmp($v, "")!=0)  //ignore empty cells
							{
								if (isset($expectedColumnsInOrder[$v]))
								{
									if (strcmp($v, MARK)==0) 
									{
										$gradeColumn = $col+$c;
										$gradeTitleRow = $row-1;
										$gradePeriod = $as->getCellByColumnAndRow($gradeColumn, $gradeTitleRow)
											->getValue();
										$gradePeriod = trim( $gradePeriod );

										if (isset($gradingPeriodColumnsOrder[$gradePeriod])==FALSE
												||
												$gradingPeriodColumnsOrder[$gradePeriod] != $gradeColumn )
										{
											echo "<br> Στην γραμμή $$row βρήκα λάθος σειρά στις στήλες όσο αφορά την στήλη $gradeColumn.<br>";
											$allColumnsOk = FALSE;
											exit(1);
										}

										continue;
									}

									if (strcmp($v, OLOGRAFOS)==0) continue; 

									if ($expectedColumnsInOrder[$v] != $c)
									{
										$allColumnsOk = FALSE;
										echo "<br>Η γραμμή $row δεν ακολουθεί την προκαθορισμένη σειρά στις στήλες!<br>\n";
										$myexpect = $expectedColumnsInOrder[$v];
										echo "<br>Στην στήλη $col συν $c βρήκα το $v ενώ το περίμενα στην στήλη $myexpect<br>";
										exit(1);
											
									}
								}
								else
								{
									echo "<br>Βρήκα μία στήλη που δεν την περίμενα ($v) στην γραμμή $row και την αγνοώ<br>\n";
								}
							}

						}//for expectedColumnsInOrder

						if ($allColumnsOk) 
						{
							//echo "All columns found at row $row\n";
							$parsingStudents = TRUE;

							$allOk = TRUE;
							if ($mySchool->isInitialized() == FALSE) 
							{
								echo "<br>Πρόβλημα! Βρήκα την λίστα των μαθητών χωρίς να έχω βρεί το σχολείο!<br>\n";
								$allOk = FALSE;
							}
							if ($curTeacher->isInitialized() == FALSE) 
							{
								echo "<br>Πρόβλημα! Βρήκα την λίστα των μαθητών χωρίς να έχω βρεί καθηγητή!($row, $col)<br>\n";
								$allOk = FALSE;
							}
							if ($curClass->isInitialized() == FALSE) 
							{
								echo "<br>Πρόβλημα! Βρήκα την λίστα των μαθητών χωρίς να έχω βρεί το τμήμα!($row, $col)<br>\n";
								$allOk = FALSE;
							}
							if ($curLesson->isInitialized() == FALSE) 
							{
								echo "<br>Πρόβλημα! Βρήκα την λίστα των μαθητών χωρίς να έχω βρεί το μάθημα!($row, $col)<br>\n";
								$allOk = FALSE;
							}

							if (!$allOk) 
							{
								echo ("<br>Γραμμή που εντοπίστηκε το πρόβλημα : $row<br>\n");
								exit(1);
							}



							/*
							   BREAK 
							 */

              if ($Settings->getDebug()) {
                echo "<table>";
                echo "<tr>";
                echo "<td>ΑΜ Νέστωρ</td>";
                echo "<td>Επώνυμο</td>";
                echo "<td>Ονομα</td>";
                echo "<td>Πατρώνυμο</td>";
                echo "<td>Μητρώνυμο</td>";
                echo "<td>--Βαθμός--</td>";
                echo "</tr>";
              }


							//go to next line from start
							$col++;

							break;
						}
						else
						{
							echo "<b>Missing columns at row $row</b><br>\n";
							exit();
						}
					}
					catch (Exception $e) 
					{
						echo "<b>Opa!!!!</b><br>\n";
					}
				}


				$col++;
			}
		}//if not parsingStudents
		else //if parsingStudents
		{
			/*
			   STATE 2 
			   getting the students of a specific course with their grades 
			 */
			$curStudMark = new StudentMark( $grmysqli, $DOINSERTGRADES );
			$curStudMark->setCourse( $curCourse );

			for ($c=0; $c<$highestColumnIndex; $c++)
			{
				$next_cellValue = $as
				->getCellByColumnAndRow($c, $row)
				->getValue();
				$v = trim($next_cellValue);

				if ($c==$expectedColumnsInOrder[AA]) {
					if ((preg_match( "/\d+/", $v, $matches)==0))
					{
            //ενώ είμαι στην στήλη του Α/Α, δεν βρήκα αριθμό!
						$parsingStudents = FALSE;

            //κάνε ένα reset
            $curClass = new Tmima($grmysqli);
            $curLesson = new Lesson($grmysqli);
            $curTeacher = new Teacher($grmysqli);
            $curCourse = new Course($grmysqli);

						/*
						   BREAK
						 */
						echo "</table>\n";
						$col++; //go to next line
						break;
					}

					$curStudMark->aa = $v;
				} 
				if ($c==$expectedColumnsInOrder[AM]) {
					$curStudMark->nestorid = $v;
					$curStudMark->school = $mySchool;
				} 
				if ($c==$expectedColumnsInOrder[EPONIMO]) {
					$curStudMark->lastname = $v;
				} 
				if ($c==$expectedColumnsInOrder[ONOMA]) {
					$curStudMark->firstname = $v;
				} 
				if ($c==$expectedColumnsInOrder[FATHER]) {
					$curStudMark->fathername = $v;
				} 
				if ($c==$expectedColumnsInOrder[MOTHER]) {
					$curStudMark->mothername = $v;
				} 
				if (isset($gradingPeriodColumnsOrder[$c])) {
					$curStudMark->addMark( $v, $gradingPeriodColumnsOrder[$c] );
				} 
				if ($c==$highestColumnIndex-1) {
					$curStudMark->store();
				}
			}
		}//if parsingStudents
	}

	echo "<table><tr>Τέλος επεξεργασίας αρχείου εισόδου</tr></table>\n";

	if ($CREATEUSERS)
	{
		echo "<h1>Λογαριασμοί Χρηστών</h1>\n";
		echo "<table border=1>\n";
	foreach ( $createdUsers as $us=>$pas)
	{
		$fullname = $createdUsernames[$us];
		echo "<tr><td><table>";
		echo "<tr>";
		echo "<td><font size=+1>Καθηγητής :</td><td>$fullname<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td><b>Όνομα χρήστη :</b></td><td>$us<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td><b>Κωδικός πρόσβασης: </b></td><td>$pas<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		//XXX for security reason: Να μην είναι στο ίδιο χαρτάκι
		//echo "<td><b>Ιστοσελίδα: </b></td><td>http://<b>koita.me/v</b></font></td>";
		echo "</tr>";
		echo "</td></tr></table>";
	}
	echo "</table>\n";
	//print_r( $createdUsers );
}

?>
