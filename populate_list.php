<?php
require_once("models/config.php");
require ("models/db-queries.php");
//error_reporting(E_ALL);
//ini_set('display_errors', 1);//display errorsi
	
// get the parameters from URL
$list_name=$_REQUEST["name"]; 
$list_value=$_REQUEST["value"];

if ($list_name== 'school'){  //populate teachers

	$teachers = getSchoolTeachers( $grmysqli, $list_value);

	//change array data from rows to columns
	foreach ($teachers as $key => $row) {
		$lastname[$key]  = $row['lastname'];
		$id[$key] = $row['id'];
	}

	// Sort the data with lastname  ascending
	array_multisort($lastname, SORT_ASC, $teachers);	
	
	echo "<select name=\"teacher\" id = 'teacher' onchange=\"populate_lists(this.value,'teacher')\">";// message value& class list
	echo "<option value='-1'>Επιλέξτε καθηγητή </option>";
	foreach($teachers as $t){
		echo '<option value="'.$t['id'].'">'.$t['lastname'].'</option>';
	}
	echo "</select></P>";

}
elseif($list_name== 'teacher'){ //populate lesson
	$lessons = getTeacherLessons($grmysqli,$list_value );

	echo "<select name='schoolcourse' id = 'schoolcourse'>";
	echo "<option value='-1'>Επιλέξτε μάθημα</option>";	
	foreach($lessons as $l){
		$cl = getAClass($grmysqli, $l['schoolclassid']);
		echo($cl);
		
		echo '<option value="'.$l['schoolcourseid'].'">'.$cl['classlevel'].$cl['classnumber'].'  -  '.$l['lessonname'].'</option>';
	}
	
	echo "</select></P>";
}
elseif($list_name== 'school_classnumber'){ //populate classno selection
      
	$sql =  "select DISTINCT classnumber from SCHOOLCLASSES WHERE SCHOOL_ID=? ORDER BY classnumber;";
	$sql = preparedQuery($grmysqli, $sql, $list_value);
	$result = mysqli_query($grmysqli, $sql);
	
	if (!isset($result) || $result==FALSE)  {
		echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
		return null	;
	}
//	echo "<select name='schoolcourse' id = 'schoolcourse'>";
        echo "<option value='-1'>Επιλέξτε τάξη</option>";

	while($row = mysqli_fetch_array($result)) {
            //$schoolclass['classlevel'] = $row['classlevel'];
			$schoolclass['classnumber'] = $row['classnumber'];        
        	echo '<option value="'.$row['classnumber'].'">'.$row['classnumber'].'</option>';        
    }
//	 echo "</select></P>";
}/*elseif($list_name== 'schclassnumber'){ //populate classno selection
      
	$sql =  " select DISTINCT classlevel from SCHOOLCLASSES WHERE SCHOOL_ID=? ORDER BY classlevel;";
	$sql = preparedQuery($grmysqli, $sql, $list_value);
	$result = mysqli_query($grmysqli, $sql);
	
	if (!isset($result) || $result==FALSE)  {
		echo 'Could not run query : ' .mysqli_sqlstate($grmysqli). ' - ' . mysqli_error($grmysqli);
		return null	;
	}
        echo "<option value='-1'>Επιλέξτε τμήμα</option>";

	while($row = mysqli_fetch_array($result)) {
            echo '<option value="'.$row['classlevel'].'">'.$row['classlevel'].'</option>';        
    }
}
elseif($list_name== 'schclassnumber'){ //populate classno selection
      
	$teachers = getSchoolTeachers( $grmysqli, $list_value);

	//change array data from rows to columns
	foreach ($teachers as $key => $row) {
		$lastname[$key]  = $row['lastname'];
		$id[$key] = $row['id'];
	}

	// Sort the data with lastname  ascending
	array_multisort($lastname, SORT_ASC, $teachers);	
	
//	echo "<select name=\"teacher\" id = 'teacher' onchange=\"populate_lists(this.value,'teacher')\">";// message value& class list
	echo "<option value='-1'>Επιλέξτε καθηγητή </option>";
	foreach($teachers as $t){
		echo '<option value="'.$t['id'].'">'.$t['lastname'].'</option>';
	}
//	echo "</select></P>";
}*/
?> 
