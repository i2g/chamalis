	function validate_form_select_course()
        {
                //var li_lesson_id=document.forms['frm_select_lesson']['lesson'].value;
		var li_lesson_id=document.forms['frm_select_lesson']['schoolcourse'].value;
                if (li_lesson_id==null || li_lesson_id=='')
                {
                        alert('Θα πρέπει να επιλέξετε μάθημα!');
                        return false;
                }
                else
                {
                        return true;
                }
        }
	function validate_form_insert_grades()//table grades in range 0-20
	{
		//static $input_grade;
		$("#error_msg").remove(); //remove previous error message
		$('.error_textbox').removeClass("error_textbox");// to remove red border find classes error
		$('#datatable tr').each(function() {
			//alert($(this).find('td.id').text());
			var grade= $(this).find('input.allgrades').val();
			if (isNaN(grade)||grade > 20||grade<0 ){
				//alert("value here must be in range 0-20");
				//$(this).find('input.grade').css("border-color","red");
				$(this).find('input.allgrades').addClass("error_textbox"); //add class so by css above become red border
				$(this).find('input.allgrades').after(' <span id= "error_msg" style="color:red">value here must be in range 0-20!<span>');// add error messagenext to textbox
				$(this).find('input.allgrades').focus(); // set focus to textbox
				return false;					
			}
		});
		return true;	
	}

