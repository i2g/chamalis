/*
UserCake Version: 2.0.2
http://usercake.com
*/
function showHide(div){
	if(document.getElementById(div).style.display = 'block'){
		document.getElementById(div).style.display = 'none';
	}else{
		document.getElementById(div).style.display = 'block'; 
	}
}

function validateGrade(){
	//static $input_grade;
	$("#error_msg").remove(); //remove previous error message
	$('.error_textbox').removeClass("error_textbox");// to remove red border find classes error
	$('#datatable tr').each(function() {
	//alert($(this).find('td.id').text());
	var grade= $(this).find('input.grade').val();
	
	if (isNaN(grade)||grade > 20||grade<0 ){
		//alert("value here must be in range 0-20");
		//$(this).find('input.grade').css("border-color","red");
		$(this).find('input.grade').addClass("error_textbox"); //add class so by css above become red border
		$(this).find('input.grade').after(' <span id= "error_msg" style="color:red">value here must be in range 0-20!<span>');// add error messagenext to textbox
		$(this).find('input.grade').focus(); // set focus to textbox
		return false;					
	}
	else{
		//$(this).find('input.grade').removeClass("error_textbox");// to rempv red border
					
	}
		   
	});
	
	return true;	
}
