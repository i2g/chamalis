-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)

-- Dump completed on 2014-04-13  8:50:24

-- ych 16/Sep/2015 
-- File cleaned from dumping of data
-- All table drops gathered together to be easy to comment them
-- Added absenceTypes and absences

-- mysql code to create database:
--
-- create database  i2g_chamalis_database;
-- MariaDB [(none)]> grant all privileges on i2g_chamalis_database.* to  haritak@localhost identified by '123123' ;

-- if you are installing userCase php remember that you
-- have to create both databases and users before starting.



--
-- Host: localhost    Database: i2g_vathmoi
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `EXCELFILES`
--
/* Table drops. RELAX and BE CAREFULL before uncommenting this code
DROP TABLE IF EXISTS `ABSENCETYPES`;
DROP TABLE IF EXISTS `ABSENCES`;

DROP TABLE IF EXISTS `TEACHERS`;
DROP TABLE IF EXISTS `STUDENTS`;
DROP TABLE IF EXISTS `USERS`;
DROP TABLE IF EXISTS `EXCELFILES`;
DROP TABLE IF EXISTS `GRADES`;
DROP TABLE IF EXISTS `SCHOOLCLASSES`;
DROP TABLE IF EXISTS `SCHOOLCLASSREGISTRATIONS`;
DROP TABLE IF EXISTS `SCHOOLYEARS`;
DROP TABLE IF EXISTS `SCHOOLS`;
DROP TABLE IF EXISTS `SCHOOLREGISTRATIONS`;
DROP TABLE IF EXISTS `SCHOOLLESSONS`;
DROP TABLE IF EXISTS `SCHOOLCOURSES`;
DROP TABLE IF EXISTS `MESSAGES`;
DROP TABLE IF EXISTS `TEACHERSOFSCHOOLCOURSES`;
DROP TABLE IF EXISTS `SETTINGS`;
*/

CREATE TABLE `SETTINGS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `settings` varchar(1000) DEFAULT NULL,
  KEY `SETTINGS_bfbf` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
CREATE TABLE `MESSAGES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `from_user_id` int(11) NOT NULL DEFAULT '0',
  KEY `MESSAGES_bfbf` (`id`),
  CONSTRAINT `MESSAGES_bg` FOREIGN KEY (`from_user_id`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EXCELFILES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) DEFAULT NULL,
  `teacher_id` int(11) NOT NULL,
  `uploaddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`,`uploaddate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GRADES`
--
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GRADES` (
  `student_id` int(11) NOT NULL DEFAULT '0',
  `schoolcourse_id` int(11) NOT NULL DEFAULT '0',
  `term` varchar(100) DEFAULT NULL,
  `grade` double DEFAULT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`student_id`,`schoolcourse_id`, `term`),
  KEY `GRADES_ibfk_2` (`schoolcourse_id`),
  CONSTRAINT `GRADES_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GRADES_ibfk_2` FOREIGN KEY (`schoolcourse_id`) REFERENCES `SCHOOLCOURSES` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `SCHOOLCLASSES`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLCLASSES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classlevel` varchar(100) DEFAULT NULL,
  `classnumber` varchar(100) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `startyear` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `classname` (`classlevel`,`school_id`,`startyear`),
  KEY `school_id` (`school_id`),
  KEY `startyear` (`startyear`),
  CONSTRAINT `SCHOOLCLASSES_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `SCHOOLS` (`id`),
  CONSTRAINT `SCHOOLCLASSES_ibfk_2` FOREIGN KEY (`startyear`) REFERENCES `SCHOOLYEARS` (`startyear`)
) ENGINE=InnoDB AUTO_INCREMENT=1723 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `SCHOOLCLASSREGISTRATIONS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLCLASSREGISTRATIONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `schoolclass_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `class_id` (`schoolclass_id`),
  CONSTRAINT `SCHOOLCLASSREGISTRATIONS_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `STUDENTS` (`id`),
  CONSTRAINT `SCHOOLCLASSREGISTRATIONS_ibfk_2` FOREIGN KEY (`schoolclass_id`) REFERENCES `SCHOOLCLASSES` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13065 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SCHOOLCOURSES`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLCOURSES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolclass_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schoolclass_id` (`schoolclass_id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `SCHOOLCOURSES_ibfk_1` FOREIGN KEY (`schoolclass_id`) REFERENCES `SCHOOLCLASSES` (`id`),
  CONSTRAINT `SCHOOLCOURSES_ibfk_3` FOREIGN KEY (`lesson_id`) REFERENCES `SCHOOLLESSONS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=679 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

CREATE TABLE `TEACHERSOFSCHOOLCOURSES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `schoolcourse_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teacherschool` (`teacher_id`,`schoolcourse_id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `schoolcourse_id` (`schoolcourse_id`),
  CONSTRAINT `COURSESTEACHERS_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `TEACHERS` (`id`),
  CONSTRAINT `COURSESTEACHERS_ibfk_3` FOREIGN KEY (`schoolcourse_id`) REFERENCES `SCHOOLCOURSES` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `SCHOOLLESSONS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLLESSONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lessonname` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SCHOOLREGISTRATIONS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLREGISTRATIONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `startyear` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `student_am` int(11) DEFAULT NULL,

  /* Τα παρακάτω χαρακτηριστικά αφορούν
    τα δεδομένα που δήλωσε ο μαθητής στην αρχή
    της χρονιάς και για αυτό δεν ανήκουν στον πίνακα
    STUDENTS αλλά εδώ. (ych) */
  `phone` varchar(100) DEFAULT NULL,
  `parentphone` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `teritory` varchar(100) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `onlyspec` tinyint(1) DEFAULT '0', /* μόνο μαθήματα ειδικότητας */

  PRIMARY KEY (`id`),
  UNIQUE KEY `student_id` (`student_id`,`school_id`,`startyear`),
  KEY `school_id` (`school_id`),
  KEY `startyear` (`startyear`),
  CONSTRAINT `SCHOOLREGISTRATIONS_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `SCHOOLS` (`id`),
  CONSTRAINT `SCHOOLREGISTRATIONS_ibfk_2` FOREIGN KEY (`startyear`) REFERENCES `SCHOOLYEARS` (`startyear`),
  CONSTRAINT `SCHOOLREGISTRATIONS_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `STUDENTS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SCHOOLS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLS` (
  `id` int(11) NOT NULL,
  `schoolname` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SCHOOLYEARS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHOOLYEARS` (
  `startyear` int(11) NOT NULL,
  PRIMARY KEY (`startyear`),
  UNIQUE KEY `startyear` (`startyear`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STUDENTS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENTS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `fathername` varchar(100) DEFAULT NULL,
  `mothername` varchar(100) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lastname` (`lastname`,`firstname`,`fathername`,`mothername`,`birthdate`)
) ENGINE=InnoDB AUTO_INCREMENT=1072 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TEACHERS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TEACHERS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `fathername` varchar(100) DEFAULT NULL,
  `mothername` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `isadmin` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `USERS`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USERS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `isadmin` smallint(6) DEFAULT '0',
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_schoolid` (`username`,`school_id`),
  KEY `USERS_ibfk_1` (`school_id`),
  CONSTRAINT `USERS_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `SCHOOLS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ABSENCETYPES` (
  `id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ABSENCES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentSchRegId` int(11) NOT NULL,      /* ych : this was named id under apousies */
  `dateOf` date NOT NULL,
  `typeId` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `regDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `studentSchRegId` (`studentSchRegId`,`dateOf`,`period`),
  KEY `typeId` (`typeId`),
  CONSTRAINT `ABSENCESCON123` FOREIGN KEY (`studentSchRegId`) REFERENCES `SCHOOLREGISTRATIONS` (`id`),
  CONSTRAINT `ABSENCESCON1234` FOREIGN KEY (`typeId`) REFERENCES `ABSENCETYPES` (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

