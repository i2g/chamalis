<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
require_once("models/header.php");
require_once("models/db-queries.php");
?>
<body>
	<script src="js/validations.js" type="text/javascript"></script>
	<script src="js/populate_lists.js" type="text/javascript"></script>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript">
		function validate_form()
		{
			var classnumber=document.forms['frm_select_classnumber']['classnumber'].value;
			if (classnumber==null || classnumber==''|| classnumber < 0)
			{
				alert('Θα πρέπει να επιλέξετε τάξη!');
				return false;
			}
			else
			{
				return true;
			}
		}
		function populate_lists(list_value,list_name)
		{
			if (list_value.length==0)
	  		{
	  			return;
	  		}
			var xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
	  		{		
	  			if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    			{
	    				if (list_name == 'school_classnumber')  //populate lists according which list changed
					{
						document.getElementById('classnumber').innerHTML=xmlhttp.responseText;
						//document.getElementById('class').innerHTML=xmlhttp.responseText;
						//document.getElementById('lesson').innerHTML='';
					}
					else if (list_name == 'teacher')  //populate lists according which list changed
					{
						document.getElementById('schoolcourse').innerHTML=xmlhttp.responseText;
						//document.getElementById('class').innerHTML=xmlhttp.responseText;
						//document.getElementById('lesson').innerHTML='';
					}
					 else if (list_name == 'class')
		                        {     
		                                document.getElementById('lesson').innerHTML=xmlhttp.responseText;
		                        }
				}
			}
			xmlhttp.open("GET","populate_list.php?value="+list_value+"&name="+list_name,true);
			xmlhttp.send();
		}
	</script>
	<div id='wrapper'>
	<div id='top'><div id='logo'></div></div>
	<div id='content'>
	<h1> Λήψη απουσιών</h1>
	<h2>Παρακαλώ επιλέξτε σχολείο και τάξη</h2>
	<div id='left-nav'>
	<?php 
	include("left-nav.php");
	?>
	</div>
	<div id='main'>
<?php 
if (securePage($_SERVER['PHP_SELF'])||!isUserLoggedIn()){
?>
	<p>
	<form action='download_apousies.php' name = 'frm_select_classnumber' method='get' onsubmit = "return validate_form()">
		<P>Σχολείο:<br>
		<?php
			$schools = mysqli_query($grmysqli,"SELECT * FROM SCHOOLS");
			// create list schools
		?>
		<select name="school" id = 'school' onchange="populate_lists(this.value,'school_classnumber')">
			<option>Επιλέξτε σχολείο </option>
			<?php
				while($row = mysqli_fetch_array($schools)){
					echo '<option value="'.$row['id'].'">'.$row['schoolname'].'</option>';  
				}
			?>
		</select></P>
		<P>Τάξη:<br>
		<select name='classnumber' id = 'classnumber'>
		</select></P>
		
		<br> <input type='submit' value='Yποβολή'>
	</form>
	</p>
<?php
}else{
echo "<br><br><h1> Θα πρέπει να κάνετε εισαγωγή στο σύστημα για να συνεχίσετε</H1>";
} ?>

	</div>
	<div id='bottom'><center><b>i2g!</center></b></div>
	</div>
</body>
</html>
