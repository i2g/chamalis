<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once("models/config.php");
require_once("models/header.php");
require_once("models/db-queries.php");
if (!securePage($_SERVER['PHP_SELF'])||!isUserLoggedIn()){die();}

?>
<body>
	<script src="js/validations.js" type="text/javascript"></script>
	<script src="js/populate_lists.js" type="text/javascript"></script>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript">
		function validate_form()
		{
			//var li_lesson_id=document.forms['frm_select_lesson']['lesson'].value;
			var li_lesson_id=document.forms['frm_select_lesson']['schoolcourse'].value;
			var li_school_id=document.forms['frm_select_lesson']['school'].value;
			var li_teacher_id=document.forms['frm_select_lesson']['teacher'].value;
			
			if(li_school_id==null || li_school_id<0){
				alert('Θα πρέπει να επιλέξετε σχολείο!');
				return false;
			}else if(li_teacher_id==null || li_teacher_id<0){
				alert('Θα πρέπει να επιλέξετε καθηγητή!');
				return false;
			}else if (li_lesson_id==null || li_lesson_id < 0 )
			{
				alert('Θα πρέπει να επιλέξετε τάξη!');
				return false;
			} 
			else
			{
				return true;
			}
		}
		function populate_lists(list_value,list_name)
		{
			if (list_value.length==0)
	  		{
	  			return;
	  		}
			var xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
	  		{		
	  			if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    			{
	    			if (list_name == 'school')  //populate lists according which list changed
					{
						document.getElementById('teacher').innerHTML=xmlhttp.responseText;
						document.getElementById('schoolcourse').innerHTML="";
						$("schoolcourse").clear();
						//document.getElementById('class').innerHTML=xmlhttp.responseText;
						//document.getElementById('lesson').innerHTML='';
					}
					else if (list_name == 'teacher')  //populate lists according which list changed
					{
						document.getElementById('schoolcourse').innerHTML=xmlhttp.responseText;
						//document.getElementById('class').innerHTML=xmlhttp.responseText;
						//document.getElementById('lesson').innerHTML='';
					}
					 else if (list_name == 'class')
		                        {     
		                                document.getElementById('lesson').innerHTML=xmlhttp.responseText;
		                        }
				}
			}
			xmlhttp.open("GET","populate_list.php?value="+list_value+"&name="+list_name,true);
			xmlhttp.send();
		}
	</script>
	<div id='wrapper'>
	<div id='top'><div id='logo'></div></div>
	<div id='content'>
	<h1>Εισαγωγή βαθμών</h1>
	<h2>Παρακαλώ επιλέξτε σχολείο τμήμα και μάθημα</h2>
	<div id='left-nav'>
	<?php 
	include("left-nav.php");
	?>
	</div>
	<div id='main'>
	<p>
	<form action='gradesinput.php' name = 'frm_select_lesson' method='get' onsubmit = "return validate_form_select_course()">
		<P>Σχολείο:<br>
		<?php
			//$schools = mysqli_query($grmysqli,"SELECT * FROM SCHOOLS");
			$loggedInUsername = $loggedInUser->username;
			$schools = mysqli_query($grmysqli,
					"select school_id as id, schoolname " .
					" from USERS inner join SCHOOLS " .
					" on USERS.school_id = SCHOOLS.id " .
					" where username='$loggedInUsername';" );
			// create list schools
		?>
		<select name="school" id = 'school' onchange="populate_lists(this.value,'school')">
			<option>Επιλέξτε σχολείο </option>
			<?php
				while($row = mysqli_fetch_array($schools)){
					echo '<option value="'.$row['id'].'">'.$row['schoolname'].'</option>';  
				}
			?>
		</select></P>
		<P>Καθηγητής:<br>
		<select name='teacher' id = 'teacher' onchange="populate_lists(this.value,'teacher')">
		</select></P>
		
		<P>Μάθημα:<br>
		<select name='schoolcourse' id = 'schoolcourse'>
		</select></P>
		
<!--		
		<P>Τμήμα:<br>
		<select name='class' id = 'class' onchange="populate_lists(this.value,'class')">
		</select></P>

		<P>Μάθημα:<br>
		<select name='lesson' id = 'lesson'>
		</select></P>
-->
		<br> <input type='submit' value='Yποβολή'>
	</form>
	</p>
	</div>
	<div id='bottom'><center><b>i2g!</center></b></div>
	</div>
</body>
</html>
