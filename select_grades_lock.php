<?php
require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}
if(!isUserLoggedIn()) { header("Location: login.php"); die(); }
require_once("models/header.php");
require_once("models/db-queries.php");
/*error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);*/
?>
<body>
<div id='wrapper'>
<div id='top'>
	<div id='logo'></div>
</div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Διαχείριση Βαθμολογίας Τάξεων</h2>
<div id='left-nav'>

<?php include("left-nav.php"); ?>

</div>
<div id='main'>
<?php
		$u = getAUserByUsername($grmysqli, $loggedInUser->username);
			if($u<>null){
				$user2 = $u[0];
				$schoolid = $user2['school_id'];
				/*
				$schclasses = getSchoolClasses2($grmysqli, $schoolid );
				//change array data from rows to columns
				foreach ($schclasses as $key => $row) {
					$level[$key]  = $row['classlevel'];
			//		$id[$key] = $row['id'];
				}

				// Sort the data with classlevel  ascending
				array_multisort($level, SORT_ASC, $schclasses);
				$counter = 1;
				echo "<br> <ul id='mylist'>";
				
				foreach($schclasses as $c){
					$cid = $c['id'];
					
					echo "<li ";
					if(($counter%2)==1){ 
						echo(" class=\"alt\""); 
					}
					echo "><a href=\"grades_lock.php?schoolid=".$schoolid."&classid=". $cid."\">";
					echo $c['classlevel'];
					echo "</a>";
					echo "</li>";
					$counter++;
				}
				echo "</ul>";
				*/
						
				
				$teachers = getSchoolTeachers( $grmysqli, $schoolid);
				
							
				//change array data from rows to columns
				foreach ($teachers as $key => $row) {
					$lastname[$key]  = $row['lastname'];
				}

				// Sort the data with lastname  ascending
				array_multisort($lastname, SORT_ASC, $teachers);	
				
				$counter = 1;
				echo "<ul id='mylist'>";
				echo "<li><a href=\"grades_lock.php?schoolid=".$schoolid."&teacherid=-50\">Αναφορά βαθμών και κλειδώματος μαθημάτων</a></li>";
				echo "<li><a href=\"grades_lock.php?schoolid=".$schoolid."&teacherid=-100\">Κλείδωμα όλων</a></li>";
				echo "<li><a href=\"grades_lock.php?schoolid=".$schoolid."&teacherid=-200\">Ξεκλείδωμα όλων</a></li><br><br>";
				
				
				foreach($teachers as $t){
					$tid = $t['id'];
					
					echo "<li ";
					if(($counter%2)==1){ 
						echo(" class=\"alt\""); 
					}
					echo "><a href=\"grades_lock.php?schoolid=".$schoolid."&teacherid=". $tid."\">";
					echo $t['lastname'] ." ". $t['firstname'] ;
					echo "</a>";
					echo "</li>";
					$counter++;
				}
				echo "</ul>";
				
			}else{
				echo "Δεν έχετε δικαίωμα να κλειδώσετε βαθμούς.";
			}
?>	
</div>
<div id='bottom'></div>
</div>
</body>
</html>
