<?php 
require_once("models/config.php");
require_once("models/header.php");
require_once ("models/db-queries.php");

if (!securePage($_SERVER['PHP_SELF'])||!isUserLoggedIn()){die();}

error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(1);
?>
<body>
  <div id='wrapper'>
  <div id='top'><div id='logo'></div></div>
  <div id='content'>
  <h1>Ρυθμίσεις</h1>
  <h2>Διάφορες ρυθμίσεις</h2>
  <div id='left-nav'>
<?php 
include("left-nav.php");
?>
  </div>
  <div id='main'>
  <p>
<?php

global $Settings;
$Settings->loadGlobalSettings();

if(isset($_POST['newSettings'])){
  global $Settings;
  if (isset($_POST['debugging'])) {
    if ($_POST['debugging']==3) {
      $Settings->setDebug( false );
    }

    if ($_POST['debugging']==1) {
      $Settings->setDebug( true );
    }
  }
  if (isset($_POST['term'])) {
   $pT = $_POST['term'];
    if ($pT==1 || $pT==2 || $pT==3 || $pT==10 || $pT==11 || $pT==20) {
      $Settings->setTerm( $_POST['term'] );
    } 
  }
  if (isset($_POST['synolo_apousiwn'])) {
    $Settings->setSynoloApousiwn($_POST['synolo_apousiwn']);
  }

  if (isset($_POST['title_AA'])) {
    $Settings->title_AA = ($_POST['title_AA']);
  }
  if (isset($_POST['title_AM'])) {
    $Settings->title_AM = ($_POST['title_AM']);
  }
  if (isset($_POST['title_EPONIMO'])) {
    $Settings->title_EPONIMO = ($_POST['title_EPONIMO']);
  }
  if (isset($_POST['title_ONOMA'])) {
    $Settings->title_ONOMA = ($_POST['title_ONOMA']);
  }
  if (isset($_POST['title_FATHER'])) {
    $Settings->title_FATHER = ($_POST['title_FATHER']);
  }
  if (isset($_POST['title_MOTHER'])) {
    $Settings->title_MOTHER = ($_POST['title_MOTHER']);
  }
  if (isset($_POST['title_MARK'])) {
    $Settings->title_MARK = ($_POST['title_MARK']);
  }
  if (isset($_POST['title_OLOGRAFOS'])) {
    $Settings->title_OLOGRAFOS = ($_POST['title_OLOGRAFOS']);
  }
  $Settings->storeGlobalSettings();
} 


echo "<hr>";
echo "<b>Τρέχουσες Ρυθμίσεις</b>";
$ddebug = $Settings->getDebug();
$term = $Settings->getCurrentTermText();
$synolo_apousiwn = $Settings->getSynoloApousiwn();
$comment = $Settings->getComment();
echo "<li>" . $ddebug . "</li>";
echo "<li>" . $term . "</li>";
echo "<li>" . $synolo_apousiwn  . "</li>";
echo "<li>" . $comment  . "</li>";
echo "<li>" . $Settings->title_AA  . "</li>";
echo "<li>" . $Settings->title_AM  . "</li>";
echo "<li>" . $Settings->title_EPONIMO  . "</li>";
echo "<li>" . $Settings->title_ONOMA  . "</li>";
echo "<li>" . $Settings->title_FATHER  . "</li>";
echo "<li>" . $Settings->title_MOTHER  . "</li>";
echo "<li>" . $Settings->title_MARK  . "</li>";
echo "<li>" . $Settings->title_OLOGRAFOS  . "</li>";
echo "<hr>";

$va = A_TETRAMINO;
$vb = B_TETRAMINO;
$vi = IOYNIOS;
$vs = SEPTEMVRIOS;

echo "
    <form action='settings.php' method='post' id='newSetting'>
          <h2>Αλλαγή Ρυθμίσεων</h2>
         <p>Οι ρυθμίσεις αλλάζουν μαζεμένες. Δηλαδή, κάθε φορά πρέπει να τις δίνουμε όλες.</p>
<table border=1><tr><td>
        <p>Debugging : 
        <select name='debugging'>
          <option value=3>Οχι</option>
          <option value=1>Ναι</option>
        </select>
        </p>
        <p>Τετράμηνο : 
        <select name='term'>
          <option value='$va'>Α Τετράμηνο</option>
          <option value='$vb'>Β Τετράμηνο</option>
          <option value='$vi'>Εξετάσεις Ιουνίου</option>
          <option value='$vs'>Εξετάσεις Σεπτεμβρίου</option>
        </select>
        </p>
        <p>Ονομα μαθήματος για τις συνολικές απουσίες : <input type=text name='synolo_apousiwn' value='" . $synolo_apousiwn . "'/> </p>
        <p>Τίτλος στήλης αύξων αριθμού : <input type=text name='title_AA' value='" . $Settings->title_AA . "' /> </p>
        <p>Τίτλος στήλης αριθμού μητρώου : <input type=text name='title_AM' value='" . $Settings->title_AM . "' /> </p>
        <p>Τίτλος στήλης επώνυμου μαθητή : <input type=text name='title_EPONIMO' value='". $Settings->title_EPONIMO . "' /> </p>
        <p>Τίτλος στήλης ονόματος μαθητή : <input type=text name='title_ONOMA' value='". $Settings->title_ONOMA . "' /> </p>
        <p>Τίτλος στήλης ονόματος πατέρα μαθητή : <input type=text name='title_FATHER' value='". $Settings->title_FATHER. "' /> </p>
        <p>Τίτλος στήλης ονόματος μητέρας μαθητή : <input type=text name='title_MOTHER' value='". $Settings->title_MOTHER. "' /> </p>
        <p>Τίτλος στήλης βαθμού : <input type=text name='title_MARK' value='". $Settings->title_MARK. "' /> </p>
        <p>Τίτλος στήλης ολογράφως : <input type=text name='title_OLOGRAFOS' value='". $Settings->title_OLOGRAFOS . "' /> </p>


        <input type='hidden' name='newSettings'/>
</td></tr></table>
        <input type='submit' value = 'Αλλαγή Ολων'>
   </form>
";

?>
  </p>
  </div>
  <div id='bottom'><center><b>i2g!</center></b></div>
  </div>
</body>
</html>
