<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
/*
 */
   error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once("models/config.php");
require_once("models/db-queries.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$captcha = md5($_POST["captcha"]);
	
	
	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
	//End data validation
	if(count($errors) == 0)
	{	
		require_once("models/header.php");
		echo "
			<body>
			<div id='wrapper'>
			<div id='top'><div id='logo'></div></div>
			<div id='content'>
			<h1>Συλλογή Βαθμών</h1>
			<h2>Αδειασμα όλης της βάσης του σχολείου</h2>

			<div id='left-nav'>";
		include("left-nav.php");
		echo "
			</div>

			<div id='main'>";
		$schoolid = $_POST['school'];


		/* Τσέκαρε οπωσδήποτε τον κωδικό!
		   Αν είναι κενός θα έχουμε πρόβλημα! (θα τα σβήσει όλα!) */
		if ( preg_match ( "/^[0-9]{7}$/", $schoolid) != 1)
		{
			echo "Κωδικός σχολείου ($schoolid) μη αποδεκτός. Τίποτα δεν έγινε...";
			die;
		}


		if (deleteGradesForSchool( $grmysqli, $schoolid ))
		{
			echo "Ολα καλά, κατάφερα και έσβησα τους βαθμούς χωρίς κανένα λάθος.";
			echo "<br>";
			if (deleteCoursesForSchool($grmysqli, $schoolid))
			{
			
				echo "Επίσης έσβησα όλες τις αναθέσεις των καθηγητών.";
				echo "<br>";

        if (deleteStudentsForSchool($grmysqli, $schoolid))
        {
          echo "Επίσης έσβησα όλους τους μαθητές.";
          echo "<br>";

          if (deleteTeachersForSchool($grmysqli, $schoolid))
          {
            echo "Επίσης έσβησα όλους τους καθηγητές.";
            echo "<br>";

            if (deleteSchool($grmysqli, $schoolid))
            {
              echo "Επίσης έσβησα και το σχολείο.";
              echo "<br>";
            }
            else
            {
              echo "Δεν μπόρεσα να σβήσω το σχολείο.<br>";
            }
          }
          else
          {
            echo "Δυστυχώς απέτυχα να σβήσω τους καθηγητές. Και το χειρότερο... Δεν ξέρω τι γίνεται με τη βάση! Καλή τύχη!";
          }
        }
        else
        {
          echo "Δυστυχώς απέτυχα να σβήσω τους μαθητές. Και το χειρότερο... Δεν ξέρω τι γίνεται με τη βάση! Καλή τύχη!";
        }
			}
			else
			{
				echo "Δυστυχώς απέτυχα νσ σβήσω τα courses. Και το χειρότερο... Δεν ξέρω τι γίνεται με τη βάση! Καλή τύχη!";
			}
		}
		else
		{
			echo "Δυστυχώς απέτυχα να σβήσω τους βαθμούς. Και το χειρότερο... Δεν ξέρω τι γίνεται με τη βάση! Καλή τύχη!";
		}


		die;
	}
}

require_once("models/header.php");
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Αδειασμα όλης της βάσης του σχολείου</h2>

<div id='left-nav'>";
include("left-nav.php");
echo "
</div>

<div id='main'>";

echo resultBlock($errors,$successes);
$loggedInUsername = $loggedInUser->username;
$schools = mysqli_query($grmysqli,
		"select id, schoolname from SCHOOLS;" );

echo "
<div id='regbox'>
<form name='newUser' action='".$_SERVER['PHP_SELF']."' method='post'>

<p>
Προσοχή! Αν προχωρήσετε θα σβηστούν ΟΛΑ τα δεδομένα που αφορούν το σχολείο σας.
</p>
<p>
Θα σβηστούν:
<ul>
<li>Βαθμοί</li>
<li>Εγγραφές μαθητών σε τάξεις</li>
<li>Αναθέσεις καθηγητών</li>
<li>Διδασκαλίες μαθημάτων σε τάξεις (και αναθέσεις καθηγητών)</li>
<li>Τμήματα</li>
<li>Μαθήματα</li>
<li>Μαθητές</li>
<li>Καθηγητές (το σβήσιμο τον λογαριασμών στο usercake πρέπει να γίνει χειροκίνητα απο το interface του)</li>
</ul>
</p>
<p>
		<P>Σχολείο:<br>
		<select name=\"school\" id ='school')\">
			<option>Επιλέξτε σχολείο</option>";

			while($row = mysqli_fetch_array($schools)){
				echo '<option value="'.$row['id'].'">'.$row['schoolname'].'</option>';  
			}
echo "
		</select></P>
<label>Κωδικός Ασφαλείας</label>
<img src='models/captcha.php'>
</p>
<label>Παρακαλώ εισάγετε τον κωδικό που βλέπετε:</label>
<input name='captcha' type='text'>
<label>&nbsp;<br>
<input type='submit' value='Delete all'/>

</form>
</div>

</div>
</div>
</body>
</html>";
?>
