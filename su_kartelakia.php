<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/
/*
 */
   error_reporting(E_ALL);
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once("models/config.php");
require_once("models/db-queries.php");
require_once("basic_classes.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}


require_once("models/header.php");
echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Καρτελλάκια χρηστών</h2>

<div id='left-nav'>";
include("left-nav.php");
echo "
</div>

<div id='main'>";

//Forms posted
if(!empty($_POST))
{
	$schoolid = $_POST['school'];
	$users = getUsersBySchoolWithPasswords($grmysqli, $schoolid);
	if ($users == null) die;

	echo "<h1>Λογαριασμοί Χρηστών</h1>\n";
	echo "<table border=1>\n";
	foreach ($users as $user)
	{
		$tmpTeacher = new Teacher($grmysqli);
		$tmpTeacher->setTeacher( $user['firstname'] . $user['lastname'] );
		$fullname = $tmpTeacher->getFullname();
		$us = $tmpTeacher->getUsername();
		$pas = $tmpTeacher->getPassword();

		echo "<tr><td><table>";
		echo "<tr>";
		echo "<td><font size=+1>Καθηγητής :</td><td>$fullname<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td><b>Όνομα χρήστη :</b></td><td>$us<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td><b>Κωδικός πρόσβασης: </b></td><td>$pas<br>\n</td>";
		echo "</tr>";
		echo "<tr>";
		//XXX for security reason: Να μην είναι στο ίδιο χαρτάκι
		//echo "<td><b>Ιστοσελίδα: </b></td><td>http://<b>koita.me/v</b></font></td>";
		echo "</tr>";
		echo "</td></tr></table>";
	}

	die;
}

echo resultBlock($errors,$successes);
$loggedInUsername = $loggedInUser->username;
$schools = mysqli_query($grmysqli,
		"select id, schoolname from SCHOOLS;" );

echo "
<div id='regbox'>
<form name='newUser' action='".$_SERVER['PHP_SELF']."' method='post'>

<p>
		<P>Σχολείο:<br>
		<select name=\"school\" id ='school')\">
			<option>Επιλέξτε σχολείο</option>";

			while($row = mysqli_fetch_array($schools)){
				echo '<option value="'.$row['id'].'">'.$row['schoolname'].'</option>';  
			}
echo "
		</select></P>
</p>
<input type='submit' value='Δείξε μου τα!'/>

</form>
</div>

</div>
</div>
</body>
</html>";
?>
