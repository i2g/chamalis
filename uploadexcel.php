<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/

$DEBUG = FALSE;
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}
require_once("models/header.php");

echo "
<body>
<div id='wrapper'>
<div id='top'><div id='logo'></div></div>
<div id='content'>
<h1>Συλλογή Βαθμών</h1>
<h2>Ανέβασμα Αρχείου Excel</h2>
<div id='left-nav'>";

include("left-nav.php");

echo "
</div>
<div id='main'>";


?>
<center>
<hr/>
	<h2>Παρακαλώ επιλέξτε το αρχείο<br> που περιέχει τα στοιχεία που θέλετε να ανεβάσετε.</h2>
	<FORM method="post" action="uploadexcel.php" enctype="multipart/form-data" >
	<input name="thefile" type="file" VALUE="Επιλογή Αρχείου"/>
	<INPUT TYPE="submit" NAME="uploadfile" VALUE="Ανέβασμα Αρχείου"><br>
	<input type="checkbox" name="createusers" value="Create Users">Μην ξεχάσεις με το ανέβασμα να δημιούργησεις τους λογαριασμούς των καθηγητών.<br>
	<input type="checkbox" name="importgrades" value="Import Grades">Αν βρεις βαθμούς μέσα στο αρχείο, <a href="explain.html">καταχώρησέ τους στην βάση</a>.<br>
	<input type="checkbox" name="ignorenoteacher" value="Ignore No Teacher">Αν βρεις μαθήματα χωρίς καθηγητή/καθηγήτρια μέσα στο αρχείο, <a href="explain2.html">δώσε τα στον ΚΑΝΕΝΑ</a>.<br>
	</FORM>
<hr/>
</center>

<?php
echo "
    <center><h3><a href=\"uploadInstructions.html\">Οδηγίες δημιουργίας αρχείου από το myschool</a></h3></center>
    ";
if(isset($_POST['uploadfile'])){
		
		
		
		try {
			
			// Undefined | Multiple Files | $_FILES Corruption Attack
			// If this request falls under any of them, treat it invalid.
			if (!isset($_FILES['thefile']['error']) ||
				is_array($_FILES['thefile']['error'])
			) {
				throw new RuntimeException('Invalid parameters.');
			}
			
			

			// Check $_FILES['thefile']['error'] value.
			switch ($_FILES['thefile']['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					throw new RuntimeException('No file sent.');
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					throw new RuntimeException('Exceeded filesize limit.');
				default:
					throw new RuntimeException('Unknown errors.');
			}

			// You should also check filesize here.
			if ($_FILES['thefile']['size'] > 4000000) {
				throw new RuntimeException('Exceeded filesize limit.');
			}

			// Check MIME Type to be Excel format.
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			$ext =$finfo->file($_FILES['thefile']['tmp_name']);
		
			switch($ext){
				case "application/x-msexcel" || "application/x-excel" || "application/vnd.ms-excel" || "application/excel":
					if ($DEBUG) echo "Filetype is excel <br />";
					break;				
				default:
					throw new RuntimeException('Invalid file format.<br />');
					break;
			}
		
			$date = new DateTime();
			
			if ($DEBUG) {
				echo $_FILES[ 'thefile' ][ 'name' ] ."<br />";	
				echo $_FILES[ 'thefile' ][ 'tmp_name' ] ."<br />";
				echo $_FILES[ 'thefile' ][ 'size' ]/1024 ." KBytes<br />";
			}
						
			// DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
			$basename = $_FILES['thefile']['name'];
			if (strlen($basename)>200) {
				echo "<b>Το όνομα του αρχείου είναι πολύ μεγάλο!</b>";

				return;
			}
			else
			{
				if ($DEBUG) echo "$basename now<br>";
				if ( (preg_match( "/(\d+)-*(\d*).*\.(xls|XLS)/"
					, $basename, $matches)!=1) ) {
					echo "<b>Λάθος όνομα αρχείου!</b>";

					return;
				}

				$basename = ''.$matches[1].'-'.$matches[2].'.'.'xls';
				if ($DEBUG) echo "$basename is the basename<br>";
			}

			$destination = 'exceldata/'.$basename.'-' .$date->getTimestamp() . sha1_file($_FILES['thefile']['tmp_name']); //$_FILES[ 'thefile' ][ 'name' ]);
//			$destination = $date->getTimestamp() . sha1_file($_FILES['thefile']['tmp_name']); //$_FILES[ 'thefile' ][ 'name' ]);
	
		
			if (move_uploaded_file($_FILES['thefile']['tmp_name'],	$destination)) {
				if ($DEBUG) echo 'File ' .$destination .'  was uploaded successfully.';
			}else{
				throw new RuntimeException('Failed to move uploaded file.');
			}		
		
			echo '<hr />';

			$argv[1] = $destination;
      $argv[2] = FALSE;
      $argv[3] = FALSE;
      $argv[4] = FALSE;
      if (isset($_POST['createusers'])) {
        $argv[2] = $_POST['createusers'];
      }
      if (isset($_POST['importgrades'])) {
        $argv[3] = $_POST['importgrades'];
      }
      if (isset($_POST['ignorenoteacher'])) {
        $argv[4] = $_POST['ignorenoteacher'];
      }
			include "parser.php";
	
		
		} catch (RuntimeException $e) {

			echo $e->getMessage();

		}
		
	}


echo "</div>
<div id='bottom'></div>
</div>
</body>
</html>";

?>
